package com.met.ahvara.weekview.sample

import android.os.Bundle
import android.widget.Toast
import com.met.ahvara.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * An example of how events can be fetched from network and be displayed on the week view.
 */
class AsynchronousActivity : BaseActivity(), Callback<MutableList<com.met.ahvara.weekview.sample.apiclient.WeekViewEvent>> {

    private val events = ArrayList<com.met.ahvara.weekview.library.weekview.WeekViewEvent>()
    private var calledNetwork = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.weekView.setLimitTime(0, 24)
    }

    override fun onMonthChange(newYear: Int, newMonth: Int): MutableList<com.met.ahvara.weekview.library.weekview.WeekViewEvent>? {

        // Download events from network if it hasn't been done already. To understand how events are
        // downloaded using retrofit, visit http://square.github.io/retrofit
        /* if (!calledNetwork) {
             val retrofit = RestAdapter.Builder()
                 .setEndpoint("https://api.myjson.com/bins")
                 .build()
             val service = retrofit.create(MyJsonService::class.java)
             service.listEvents(this)
             calledNetwork = true
         }
 */
        // Return only the events that matches newYear and newMonth.
        val matchedEvents = ArrayList<com.met.ahvara.weekview.library.weekview.WeekViewEvent>()
        for (event in events)
            if (eventMatches(event, newYear, newMonth))
                matchedEvents.add(event)
        return matchedEvents
    }

    /**
     * Checks if an event falls into a specific year and month.
     *
     * @param weekViewEvent The event to check for.
     * @param year  The year.
     * @param month The month.
     * @return True if the event matches the year and month.
     */
    private fun eventMatches(weekViewEvent: com.met.ahvara.weekview.library.weekview.WeekViewEvent, year: Int, month: Int): Boolean {
        return weekViewEvent.startTime.get(Calendar.YEAR) == year && weekViewEvent.startTime.get(Calendar.MONTH) == month - 1 || weekViewEvent.endTime.get(
            Calendar.YEAR
        ) == year && weekViewEvent.endTime.get(Calendar.MONTH) == month - 1
    }


    override fun onResponse(
        call: Call<MutableList<com.met.ahvara.weekview.sample.apiclient.WeekViewEvent>>,
        response: Response<MutableList<com.met.ahvara.weekview.sample.apiclient.WeekViewEvent>>
    ) {
        for (event in response.body()!!)
            this.events.add(event.toWeekViewEvent())
        binding.weekView.notifyDataSetChanged()
    }

    override fun onFailure(call: Call<MutableList<com.met.ahvara.weekview.sample.apiclient.WeekViewEvent>>, t: Throwable) {
        t.printStackTrace()
        Toast.makeText(this, R.string.async_error, Toast.LENGTH_SHORT).show()
    }
}
