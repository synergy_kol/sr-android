package com.met.ahvara.weekview.sample

import android.graphics.RectF
import android.os.Bundle
import android.text.format.DateFormat
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.met.ahvara.R
import com.met.ahvara.weekview.library.weekview.DateTimeInterpreter
import com.met.ahvara.weekview.library.weekview.WeekDaySubtitleInterpreter
import com.met.ahvara.weekview.library.weekview.WeekView
import com.met.ahvara.weekview.library.weekview.WeekViewEvent
import java.text.SimpleDateFormat
import java.util.*

/**
 * Activity to demonstrate snapping of the whole view, for example week-by-week.
 */
class WholeViewSnappingActivity : BasicActivity() {
    val locale = Locale.getDefault()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.weekView.isShowNowLine = true
//        weekView.setAutoLimitTime(true)
        binding.weekView.setLimitTime(0, 24)
        binding.weekView.isUsingCheckersStyle = true
        binding.weekView.columnGap =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, resources.displayMetrics)
                .toInt()
        binding.weekView.hourSeparatorHeight = binding.weekView.columnGap
        binding.weekView.isScrollNumberOfVisibleDays = true
        binding.weekView.dropListener = null
        binding.weekView.allDaySideTitleText = getString(R.string.all_day)
        setDayViewType(TYPE_WEEK_VIEW)
        val cal = Calendar.getInstance()
        val currentHour = cal.get(Calendar.HOUR_OF_DAY) + cal.get(Calendar.MINUTE) / 60.0
        binding.weekView.goToHour(Math.max(currentHour - 1, 0.0))
        cal.set(Calendar.DAY_OF_WEEK, cal.firstDayOfWeek)
        binding.weekView.goToDate(cal)
        binding.weekView.scrollListener = object : WeekView.ScrollListener {
            val monthFormatter = SimpleDateFormat("MMM", locale)
            val yearFormatter = SimpleDateFormat("yyyy", locale)

            override fun onFirstVisibleDayChanged(
                newFirstVisibleDay: Calendar,
                oldFirstVisibleDay: Calendar?
            ) {
                //we show just the month here, so no need to update it every time
                if (oldFirstVisibleDay == null || oldFirstVisibleDay.get(Calendar.MONTH) != newFirstVisibleDay.get(
                        Calendar.MONTH
                    )
                ) {
                    val date = newFirstVisibleDay.time
                    if (cal.get(Calendar.YEAR) == newFirstVisibleDay.get(Calendar.YEAR)) {
                        binding.weekView.sideSubtitleText = ""
                        binding.weekView.sideTitleText = monthFormatter.format(date)
                    } else {
                        binding.weekView.sideTitleText = monthFormatter.format(date)
                        binding.weekView.sideSubtitleText = yearFormatter.format(date)
                    }
                }
            }
        }
        binding.draggableView.visibility = View.GONE
        binding.weekView.weekDaySubtitleInterpreter = object : WeekDaySubtitleInterpreter {
            val dateFormatTitle = SimpleDateFormat("d", locale)

            override fun getFormattedWeekDaySubtitle(date: Calendar): String =
                dateFormatTitle.format(date.time)
        }
        binding.weekView.eventClickListener = object : WeekView.EventClickListener {
            override fun onEventClick(event: WeekViewEvent, eventRect: RectF) {
            }
        }
    }

    override fun setupDateTimeInterpreter(shortDate: Boolean) {
        val calendar = Calendar.getInstance().apply {
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        val timeFormat = DateFormat.getTimeFormat(this)
            ?: SimpleDateFormat("HH:mm", locale)
        val dateFormatTitle = SimpleDateFormat("EEE", locale)
        binding.weekView.dateTimeInterpreter = object : DateTimeInterpreter {
            override fun getFormattedTimeOfDay(hour: Int, minutes: Int): String {
                calendar.set(Calendar.HOUR_OF_DAY, hour)
                calendar.set(Calendar.MINUTE, minutes)
                return timeFormat.format(calendar.time)
            }

            override fun getFormattedWeekDayTitle(date: Calendar): String =
                dateFormatTitle.format(date.time)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_today) {
            val cal = Calendar.getInstance().apply { set(Calendar.DAY_OF_WEEK, firstDayOfWeek) }
            binding.weekView.goToDate(cal)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val result = super.onCreateOptionsMenu(menu)
        menu.findItem(R.id.action_day_view).isChecked = false
        menu.findItem(R.id.action_three_day_view).isChecked = false
        menu.findItem(R.id.action_week_view).isChecked = true
        return result
    }
}