package com.met.ahvara.weekview.library.weekview

import androidx.annotation.ColorInt

interface TextColorPicker {
    @ColorInt
    fun getTextColor(event: WeekViewEvent): Int

}
