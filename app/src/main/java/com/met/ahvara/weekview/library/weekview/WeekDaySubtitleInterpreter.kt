package com.met.ahvara.weekview.library.weekview

import java.util.*

interface WeekDaySubtitleInterpreter {
    fun getFormattedWeekDaySubtitle(date: Calendar): String
}
