package com.met.ahvara.weekview.sample.apiclient

import retrofit2.Callback
import retrofit2.http.GET

interface MyJsonService {

    @GET("/1kpjf")
    fun listEvents(eventsCallback: Callback<MutableList<WeekViewEvent>>)

}
