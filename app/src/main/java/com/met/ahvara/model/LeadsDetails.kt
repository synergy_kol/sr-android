package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class LeadsDetailsRequest(
    @SerializedName("lead_id") var lead_id: String? = null,
)

data class LeadsDetailsResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: LeadsDetailsResult? = LeadsDetailsResult()
)

data class LeadsDetailsResult(
    @SerializedName("data") var data: LeadsDetailsData = LeadsDetailsData()
)

data class LeadsDetailsData(
    @SerializedName("customer_id") var customerId: String? = null,
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("city") var city: String? = null,
    @SerializedName("state") var state: String? = null,
    @SerializedName("zipcode") var zipcode: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("unit") var unit: String? = null,
    @SerializedName("est_date_close") var estDateClose: String? = null,
    @SerializedName("reminder_date") var reminderDate: String? = null,
    @SerializedName("notes") var notes: String? = null
)