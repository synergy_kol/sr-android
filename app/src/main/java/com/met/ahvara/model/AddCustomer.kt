package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class AddCustomerRequest(
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("is_vip") var isVip: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("category") var category: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("city") var city: String? = null,
    @SerializedName("state") var state: String? = null,
    @SerializedName("zip") var zip: String? = null,
    @SerializedName("unit") var unit: Int? = null
)

data class AddCustomerResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: AddCustomerResult? = AddCustomerResult()
)

data class AddCustomerResult(
    @SerializedName("data") var data: AddCustomerData = AddCustomerData()
)

data class AddCustomerData(
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("is_vip") var isVip: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("category") var category: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("city") var city: String? = null,
    @SerializedName("state") var state: String? = null,
    @SerializedName("zip") var zip: String? = null,
    @SerializedName("unit") var unit: Int? = null
)