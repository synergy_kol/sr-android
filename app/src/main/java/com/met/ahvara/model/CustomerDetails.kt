package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class CustomerDetailsRequest(
    //{"customer_id":"1"}
    @SerializedName("customer_id") var customer_id: String? = null,
)

data class CustomerDetailsResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: CustomerDetailsResult? = CustomerDetailsResult()
)

data class CustomerDetailsResult(
    @SerializedName("data") var data: CustomerDetailsData = CustomerDetailsData()
)

data class CustomerDetailsData(
    @SerializedName("customer_id") var customerId: String? = null,
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("is_vip") var isVip: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("city") var city: String? = null,
    @SerializedName("state") var state: String? = null,
    @SerializedName("zipcode") var zipcode: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("unit") var unit: String? = null,
    @SerializedName("category") var category: String? = null,
    @SerializedName("contact_details") var contactDetails: ArrayList<CustomerContactDetails> = arrayListOf()
)

data class CustomerContactDetails(
    @SerializedName("name") var name: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("latitude") var latitude: String? = null,
    @SerializedName("longitude") var longitude: String? = null
)