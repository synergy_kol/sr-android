package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class AppointmentProgressDailyWeeklyResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: ProgressDailyWeeklyResult? = ProgressDailyWeeklyResult()
)

data class ProgressDailyWeeklyData(
    @SerializedName("all_day_listdata") var allDayListdata: Int? = null,
    @SerializedName("all_week_listdata") var allWeekListdata: Int? = null,
    @SerializedName("all_day_complete_listdata") var allDayCompleteListdata: Int? = null,
    @SerializedName("all_week_complete_listdata") var allWeekCompleteListdata: Int? = null
)

data class ProgressDailyWeeklyResult(
    @SerializedName("data") var data: ProgressDailyWeeklyData? = ProgressDailyWeeklyData()
)