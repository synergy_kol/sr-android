package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class WorkOrderListResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: WorkOrderListResult? = WorkOrderListResult()
)

data class WorkOrderListResult(
    @SerializedName("data") var data: ArrayList<WorkOrderListData> = arrayListOf()
)

data class WorkOrderListData(
    @SerializedName("id") var id: String? = null,
    @SerializedName("work_id") var workId: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("product_name") var productName: String? = null,
    @SerializedName("assigned_to") var assignedTo: String? = null,
    @SerializedName("created_by") var createdBy: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("date") var date: String? = null,
    @SerializedName("time") var time: String? = null,
    @SerializedName("expiry_date") var expiryDate: String? = null,
    @SerializedName("expiry_time") var expiryTime: String? = null,
    @SerializedName("description") var description: String? = null,
    @SerializedName("attachement") var attachement: String? = null,
    @SerializedName("priority") var priority: String? = null,
    @SerializedName("parts") var parts: String? = null,
    @SerializedName("total_amount") var total_amount: String? = null
)