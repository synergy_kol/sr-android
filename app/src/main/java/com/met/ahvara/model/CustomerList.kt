package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class CustomerListSearchRequest(
    @SerializedName("search_string") var search_string: String? = null,
)

data class CustomerListResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: CustomerListResult? = CustomerListResult()
)

data class CustomerListResult(
    @SerializedName("data") var data: ArrayList<CustomerListData> = arrayListOf()
)

data class CustomerListData(
    @SerializedName("customer_id") var customerId: String? = null,
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("is_vip") var isVip: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("city") var city: String? = null,
    @SerializedName("state") var state: String? = null,
    @SerializedName("zipcode") var zipcode: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("unit") var unit: String? = null,
    @SerializedName("category") var category: String? = null
)

