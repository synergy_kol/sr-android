package com.met.ahvara.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "mark_as_arrived_table", primaryKeys = ["appointmentId", "status"])
data class StatusChangeRequest(
    @SerializedName("appointment_id") var appointmentId: String = "",
    @SerializedName("status") var status: String = "",
    @SerializedName("latitude") var latitude: String? = null,
    @SerializedName("longitude") var longitude: String? = null
)

data class StatusChangeResponse(
    @SerializedName("status") var status: Status? = Status(),
    //@SerializedName("result") var result: StatusChangeResult? = StatusChangeResult()
)

data class StatusChangeResult(
    @SerializedName("data" ) var data : String? = null
)