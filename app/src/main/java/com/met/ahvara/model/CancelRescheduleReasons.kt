package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class CancelRescheduleRequest(
	//{
	//    "type":"cancel"
	//}
	@SerializedName("type") var type: String? = null,
)

data class CancelRescheduleResponse(
	@SerializedName("status") var status: Status? = Status(),
	@SerializedName("result") var result: CancelRescheduleResult? = CancelRescheduleResult()
)

data class CancelRescheduleResult(
	@SerializedName("data") var data: CancelRescheduleData = CancelRescheduleData()
)

data class CancelRescheduleData(
	@SerializedName("reasonlist" ) var reasonlist : ArrayList<Reasonlist> = arrayListOf()

)
data class Reasonlist (
	@SerializedName("id"     ) var id     : String? = null,
	@SerializedName("type"   ) var type   : String? = null,
	@SerializedName("reason" ) var reason : String? = null
)