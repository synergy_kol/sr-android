package com.met.ahvara.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UpNextAppointmentResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: UpNextAppointmentResult? = UpNextAppointmentResult()
)

data class UpNextAppointmentListData(
    @SerializedName("id") var id: String? = null,
    @SerializedName("start_date_time") var startDateTime: String? = null,
    @SerializedName("end_date_time") var endDateTime: String? = null,
    @SerializedName("duration") var duration: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("customer_id") var customerId: String? = null,
    @SerializedName("customer_site_id") var customerSiteId: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("assigned_to") var assignedTo: String? = null,
    @SerializedName("call_type") var callType: String? = null,
    @SerializedName("created_by") var createdBy: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_by") var updatedBy: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("is_reschedule") var isReschedule: String? = null,
    @SerializedName("reason_id") var reasonId: String? = null,
    @SerializedName("customer_rating") var customerRating: String? = null,
    @SerializedName("customer_site_rating") var customerSiteRating: String? = null,
    @SerializedName("cancel_remark") var cancelRemark: String? = null,
    @SerializedName("reschedule_remark") var rescheduleRemark: String? = null,
    @SerializedName("frequency") var frequency: String? = null,
    @SerializedName("work_order_id") var workOrderId: String? = null,
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("site_latitude") var siteLatitude: String? = null,
    @SerializedName("site_longitude") var siteLongitude: String? = null,
    @SerializedName("site_address") var siteAddress: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(startDateTime)
        parcel.writeString(endDateTime)
        parcel.writeString(duration)
        parcel.writeString(type)
        parcel.writeString(customerId)
        parcel.writeString(customerSiteId)
        parcel.writeString(status)
        parcel.writeString(assignedTo)
        parcel.writeString(callType)
        parcel.writeString(createdBy)
        parcel.writeString(createdAt)
        parcel.writeString(updatedBy)
        parcel.writeString(updatedAt)
        parcel.writeString(isReschedule)
        parcel.writeString(reasonId)
        parcel.writeString(customerRating)
        parcel.writeString(customerSiteRating)
        parcel.writeString(cancelRemark)
        parcel.writeString(rescheduleRemark)
        parcel.writeString(frequency)
        parcel.writeString(workOrderId)
        parcel.writeString(companyName)
        parcel.writeString(contact)
        parcel.writeString(email)
        parcel.writeString(address)
        parcel.writeString(siteLatitude)
        parcel.writeString(siteLongitude)
        parcel.writeString(siteAddress)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UpNextAppointmentListData> {
        override fun createFromParcel(parcel: Parcel): UpNextAppointmentListData {
            return UpNextAppointmentListData(parcel)
        }

        override fun newArray(size: Int): Array<UpNextAppointmentListData?> {
            return arrayOfNulls(size)
        }
    }
}

data class UpNextAppointmentData(
    @SerializedName("listdata") var listdata: UpNextAppointmentListData? = UpNextAppointmentListData()
)

data class UpNextAppointmentResult(
    @SerializedName("data") var data: UpNextAppointmentData? = UpNextAppointmentData()
)