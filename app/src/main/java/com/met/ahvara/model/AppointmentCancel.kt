package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class AppointmentCancelRequest(
    //appointment_id
    //status
    //reason_id
    //remark
    @SerializedName("appointment_id") var appointment_id: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("reason_id") var reason_id: String? = null,
    @SerializedName("remark") var remark: String? = null
)

data class AppointmentCancelResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: AppointmentCancelResult? = AppointmentCancelResult()
)

data class AppointmentCancelResult(
    @SerializedName("data") var data: AppointmentCancelData = AppointmentCancelData()
)

data class AppointmentCancelData(
    @SerializedName("customer_id") var customerId: String? = null,
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("is_vip") var isVip: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("city") var city: String? = null,
    @SerializedName("state") var state: String? = null,
    @SerializedName("zipcode") var zipcode: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("unit") var unit: String? = null,
    @SerializedName("category") var category: String? = null,
    @SerializedName("contact_details") var contactDetails: ArrayList<CustomerContactDetails> = arrayListOf()
)
