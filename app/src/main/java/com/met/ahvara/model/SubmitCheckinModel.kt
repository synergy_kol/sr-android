package com.met.ahvara.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "submit_checkin_table", primaryKeys = ["time", "timestamp"])
data class SubmitCheckinRequest(
    @SerializedName("time")
    var time: String,

    @SerializedName("latitude")
    var latitude: String,

    @SerializedName("longitude")
    var longitude: String,

    @SerializedName("timestamp")
    var timestamp: String
)

data class SubmitCheckinResponse(
    @SerializedName("status") var status: Status? = Status(),
    //   @SerializedName("result") var result: SubmitCheckinResult? = SubmitCheckinResult()
)

data class SubmitCheckinResult(
    @SerializedName("data") var data: SubmitCheckinData? = SubmitCheckinData()
)

data class SubmitCheckinData(
    @SerializedName("time")
    var time: String? = null,
)
