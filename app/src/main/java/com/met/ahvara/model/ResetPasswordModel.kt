package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class ResetPasswordRequest(
    @SerializedName("email") var email: String? = null,
    @SerializedName("password") var password: String? = null,
    @SerializedName("reset_password_otp") var resetPasswordOtp: String? = null,
    @SerializedName("prefix") var prefix: String? = null
)

data class ResetPasswordResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: ResetPasswordResult? = ResetPasswordResult(),
    @SerializedName("execution_time") var executionTime: String? = null
)

data class ResetPasswordUserDetails(
    @SerializedName("user_id") var userId: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("first_name") var firstName: String? = null,
    @SerializedName("last_name") var lastName: String? = null
)

data class ResetPasswordResult(
    @SerializedName("userdetails") var userdetails: ResetPasswordUserDetails? = ResetPasswordUserDetails()
)