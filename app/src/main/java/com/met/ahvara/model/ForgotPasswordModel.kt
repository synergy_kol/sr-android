package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

data class ForgotPasswordResponse(
    @SerializedName("status") var status: Status? = Status(),
    //@SerializedName("result") var result: Result? = Result()
)

data class ForgotPasswordRequest(
    @SerializedName("email") var email: String? = null,
    @SerializedName("prefix") var prefix: String? = null
)


