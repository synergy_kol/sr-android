package com.met.ahvara.model

import com.google.gson.annotations.SerializedName

//@Entity(tableName = "submit_checkin_table", primaryKeys = ["time", "latitude"])
data class SubmitCheckoutRequest(
    @SerializedName("time")
    var time: String,
    @SerializedName("latitude")
    var latitude: String,
    @SerializedName("longitude")
    var longitude: String
)

data class SubmitCheckOutResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: SubmitCheckOutResult? = SubmitCheckOutResult(),
    @SerializedName("execution_time") var executionTime: String? = null
)

data class SubmitCheckOutResult(
    @SerializedName("data") var data: SubmitCheckOutData? = SubmitCheckOutData()
)

data class SubmitCheckOutData(
    @SerializedName("listdata") var listdata: SubmitCheckOutListdata? = SubmitCheckOutListdata()
)

data class SubmitCheckOutListdata(

    @SerializedName("id") var id: String? = null,
    @SerializedName("start_date_time") var startDateTime: String? = null,
    @SerializedName("end_date_time") var endDateTime: String? = null,
    @SerializedName("duration") var duration: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("customer_id") var customerId: String? = null,
    @SerializedName("customer_site_id") var customerSiteId: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("assigned_to") var assignedTo: String? = null,
    @SerializedName("call_type") var callType: String? = null,
    @SerializedName("created_by") var createdBy: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_by") var updatedBy: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("is_reschedule") var isReschedule: String? = null,
    @SerializedName("reason_id") var reasonId: String? = null,
    @SerializedName("customer_rating") var customerRating: String? = null,
    @SerializedName("customer_site_rating") var customerSiteRating: String? = null,
    @SerializedName("cancel_remark") var cancelRemark: String? = null,
    @SerializedName("reschedule_remark") var rescheduleRemark: String? = null,
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("site_latitude") var siteLatitude: String? = null,
    @SerializedName("site_longitude") var siteLongitude: String? = null,
    @SerializedName("site_address") var siteAddress: String? = null

)
