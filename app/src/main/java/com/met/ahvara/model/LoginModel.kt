package com.met.ahvara.model

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class LoginRequest(
    @SerializedName("source") var source: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("password") var password: String? = null,
    @SerializedName("device_type") var deviceType: String? = null,
    @SerializedName("device_token") var deviceToken: String? = null
)

@Entity(tableName = "login_response_table", primaryKeys = ["status", "token"])
data class LoginResponse(

    @SerializedName("status") var status: Status = Status(),
    @SerializedName("token") var token: String = "",
    @SerializedName("result") var result: LoginResponseResult
)

@Parcelize
data class Status(
    @SerializedName("error_code") var errorCode: Int? = null,
    @SerializedName("message") var message: String? = null
) : Parcelable

data class LoginResponseData(
    @SerializedName("user_id") var userId: String,
    @SerializedName("first_name") var firstName: String? = null,
    @SerializedName("last_name") var lastName: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("company_email") var companyEmail: String? = null,
    @SerializedName("phone") var phone: String? = null,
    @SerializedName("company_phone") var companyPhone: String? = null,
    @SerializedName("company") var company: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("role_id") var roleId: String? = null,
    @SerializedName("prefix") var prefix: String? = null,
    @SerializedName("profile_image") var profileImage: String? = null
)

data class LoginResponseResult(
    @SerializedName("data") var data: LoginResponseData
)
