package com.met.ahvara.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class AppointmentListRequest(
    /*{
        "filter_by":"date",
        "date":"2022-05-04"
    }*/
    @SerializedName("filter_by") var filter_by: String? = null,
    @SerializedName("date") var date: String? = null,
)

@Parcelize
data class AppointmentListResponse(
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("result") var result: AppointmentListResult? = AppointmentListResult()
) : Parcelable

@Parcelize
data class AppointmentListResult(
    @SerializedName("data") var data: AppointmentListData? = AppointmentListData()
) : Parcelable

@Parcelize
data class AppointmentListData(
    @SerializedName("listdata")
    var listdata: ArrayList<AppointmentListListdata> = arrayListOf(),

    /* @SerializedName("customerdata")
     var customerdata: ArrayList<AppointmentListCustomerdata> = arrayListOf(),

     @SerializedName("cancelreasonlist")
     var cancelreasonlist: ArrayList<AppointmentListCancelreasonlist> = arrayListOf(),

     @SerializedName("reschedulereasonlist")
     var reschedulereasonlist: ArrayList<AppointmentListReschedulereasonlist> = arrayListOf(),

     @SerializedName("prefix")
     var prefix: String? = null,

     @SerializedName("srlistdata")
     var srlistdata: ArrayList<String> = arrayListOf(),

     @SerializedName("stlistdata")
     var stlistdata: ArrayList<String> = arrayListOf()*/
) : Parcelable

@Parcelize
data class AppointmentListListdata(
    @SerializedName("id") var id: String? = null,
    @SerializedName("record_id") var recordId: String? = null,
    @SerializedName("start_date_time") var startDateTime: String? = null,
    @SerializedName("end_date_time") var endDateTime: String? = null,
    @SerializedName("duration") var duration: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("customer_id") var customerId: String? = null,
    @SerializedName("customer_site_id") var customerSiteId: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("assigned_to") var assignedTo: String? = null,
    @SerializedName("call_type") var callType: String? = null,
    @SerializedName("created_by") var createdBy: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_by") var updatedBy: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("is_reschedule") var isReschedule: String? = null,
    @SerializedName("reason_id") var reasonId: String? = null,
    @SerializedName("customer_rating") var customerRating: String? = null,
    @SerializedName("customer_site_rating") var customerSiteRating: String? = null,
    @SerializedName("cancel_remark") var cancelRemark: String? = null,
    @SerializedName("reschedule_remark") var rescheduleRemark: String? = null,
    @SerializedName("frequency") var frequency: String? = null,
    @SerializedName("work_order_id") var workOrderId: String? = null,
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("site_latitude") var siteLatitude: String? = null,
    @SerializedName("site_longitude") var siteLongitude: String? = null,
    @SerializedName("site_address") var siteAddress: String? = null
) : Parcelable

@Parcelize
data class AppointmentListCustomerdata(
    @SerializedName("id") var id: String? = null,
    @SerializedName("company_name") var companyName: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("is_vip") var isVip: String? = null,
    @SerializedName("contact") var contact: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("category") var category: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("city") var city: String? = null,
    @SerializedName("state") var state: String? = null,
    @SerializedName("country") var country: String? = null,
    @SerializedName("zipcode") var zipcode: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("unit") var unit: String? = null,
    @SerializedName("assigned_to") var assignedTo: String? = null,
    @SerializedName("attachement") var attachement: String? = null,
    @SerializedName("created_by") var createdBy: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null
) : Parcelable

@Parcelize
data class AppointmentListCancelreasonlist(
    @SerializedName("id") var id: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("reason") var reason: String? = null
) : Parcelable

@Parcelize
data class AppointmentListReschedulereasonlist(
    @SerializedName("id") var id: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("reason") var reason: String? = null
) : Parcelable