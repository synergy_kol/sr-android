package com.met.ahvara.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.met.ahvara.converters.ConverterLoginResult
import com.met.ahvara.converters.ConverterStatus
import com.met.ahvara.dao.LoginDao
import com.met.ahvara.dao.MarkAsArrivedDao
import com.met.ahvara.dao.SubmitCheckinDao
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.model.StatusChangeRequest
import com.met.ahvara.model.SubmitCheckinRequest
import com.met.ahvara.utils.AppConstants.Companion.NAME_DATABASE

@Database(
    entities = [LoginResponse::class, SubmitCheckinRequest::class, StatusChangeRequest::class],
    version = 1
)
@TypeConverters(ConverterStatus::class, ConverterLoginResult::class)
abstract class AhvaraDatabase : RoomDatabase() {
    abstract fun loginDao(): LoginDao
    abstract fun submitCheckinDao(): SubmitCheckinDao
    abstract fun markAsArrivedDao(): MarkAsArrivedDao

    companion object {
        private var instance: AhvaraDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): AhvaraDatabase {
            if (instance == null)
                instance = Room.databaseBuilder(
                    ctx.applicationContext, AhvaraDatabase::class.java,
                    NAME_DATABASE
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build()

            return instance!!
        }

        private val roomCallback = object : Callback() {
            /*override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }*/
        }


    }
}