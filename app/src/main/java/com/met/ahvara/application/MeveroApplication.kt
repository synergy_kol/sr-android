package com.met.ahvara.application
/*

import android.app.Application
import androidx.lifecycle.LifecycleObserver
import android.os.StrictMode.VmPolicy
import android.os.StrictMode
import android.util.Log
import com.met.ahvara.application.MeveroApplication
import com.met.ahvara.database.AhvaraDatabase
import androidx.room.Room

class MeveroApplication : Application(), LifecycleObserver {
    private val TAG = "MeveroApplication"
    private val TYPE_BACKGROUND = "BACKGROUND"
    private val TYPE_FOREGROUND = "FOREGROUND"
    override fun onCreate() {
        super.onCreate()
        initApplication()

//To read download file
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
    }

    private fun initApplication() {
        instance = this
    }

    private var meveroAppDatabase: AhvaraDatabase? = null
    private val lock = Any()
    fun initializeUserDB(userId: String) {
        Log.d(TAG, "Acquiring lock - Init")
        synchronized(lock) {
            if (meveroAppDatabase == null) {
                Log.d(TAG, "Creating DB")
                meveroAppDatabase =
                    Room.databaseBuilder(this, AhvaraDatabase::class.java, "USERDB_$userId").build()
                Log.d(TAG, "Created DB")
            }
        }
    }

    fun getMeveroAppDatabase(): AhvaraDatabase? {
        synchronized(lock) { return meveroAppDatabase }
    }

    fun closeMeveroAppDatabase() {
        Log.d(TAG, "Acquiring lock - Close")
        synchronized(lock) {
            if (meveroAppDatabase != null) {
                Log.d(TAG, "Closing DB")
                meveroAppDatabase!!.close()
                meveroAppDatabase = null
                Log.d(TAG, "Closed DB")
            } else {
                Log.d(TAG, "DB is not initialized")
            }
        }
    }

    companion object {
        var instance: MeveroApplication? = null
            private set
    }
}*/
