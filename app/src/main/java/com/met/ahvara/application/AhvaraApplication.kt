package com.met.ahvara.application

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import com.met.ahvara.sharedpreferences.AhvaraPreferences

class AhvaraApplication : Application() {
    private lateinit var context: Context
    private val CHANNEL_ID = "Ahvara"

    companion object {
        lateinit var ahvaraPreference: AhvaraPreferences

        val AHVARA_APPLICATION_INSTANCE: AhvaraApplication? = null

        fun getAppPreference(): AhvaraPreferences {
            return ahvaraPreference
        }

    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        context = applicationContext
        ahvaraPreference = AhvaraPreferences.init(context, "AHVARA")!!

    }

    /*@OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        try {
            PrefUtil.setAppOnBackground(this, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "AHVARA Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(serviceChannel)
        }
    }
}