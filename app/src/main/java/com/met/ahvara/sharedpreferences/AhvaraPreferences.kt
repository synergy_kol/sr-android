package com.met.ahvara.sharedpreferences

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.lifecycle.LifecycleOwner
import com.google.gson.Gson
import com.met.ahvara.database.AhvaraDatabase
import com.met.ahvara.ui.login.SignInActivity
import com.met.ahvara.utils.subscribeOnBackground
import com.met.ahvara.viewmodel.DashboardViewModel
import com.met.ahvara.viewmodel.LoginViewModel

class AhvaraPreferences private constructor(context: Context, DATABASE_NAME: String) {
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(DATABASE_NAME, Context.MODE_PRIVATE)
    var context: Context

    init {
        this.context = context
    }

    companion object {
        private var appPreference: AhvaraPreferences? = null
        fun init(context: Context, DATABASE_NAME: String): AhvaraPreferences? {
            if (appPreference == null) {
                appPreference = AhvaraPreferences(context, DATABASE_NAME)
            }
            return appPreference
        }
    }

    fun saveString(key: String?, value: String?) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getString(key: String?): String? {
        return sharedPreferences.getString(key, "")
    }

    fun logOut(
        mContext: Context, lifecycleOwner: LifecycleOwner, loginViewModel: LoginViewModel,
        dashboardViewModel: DashboardViewModel?
    ) {

        // database clean
        val database = AhvaraDatabase.getInstance(mContext)
        loginViewModel.getAllLogin().removeObservers(lifecycleOwner)
        dashboardViewModel?.observerSubmitCheckinResponse?.removeObservers(lifecycleOwner)

        subscribeOnBackground {
            database.loginDao().deleteLoginData()
            database.submitCheckinDao().deleteSubmitCheckinData()
        }
        // database clean

        sharedPreferences.edit().clear().apply()
        context.getSharedPreferences("Ahvara", 0).edit().clear().apply()
        //isUserLoggedIn = false
        (mContext as Activity).finish()
        mContext.startActivity(Intent(mContext, SignInActivity::class.java))
        mContext.finishAffinity()
    }

    fun saveUserDetails(key: String?, registerModelClass: Any?) {
        val gson = Gson()
        val editor = sharedPreferences.edit()
        editor.putString(key, gson.toJson(registerModelClass))
        editor.apply()
    }

    /*  var isUserLoggedIn: Boolean
          get() = sharedPreferences.getBoolean("isUserLoggedIn", false)
          set(value) {
              val editor = sharedPreferences.edit()
              editor.putBoolean("isUserLoggedIn", value)
              editor.apply()
          }*/

    fun <T> getUserDetails(key: String?, type: Class<T>?): T {
        val gson = Gson()
        return gson.fromJson(sharedPreferences.getString(key, null), type)
    }

    fun setSettings(key: String?, value: Boolean) {
        val editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getSettings(key: String?): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

    val isUserFirstTime: Boolean
        get() = sharedPreferences.getBoolean("isUserFirstTime", true)

    fun setUserLoginTime() {
        val editor = sharedPreferences.edit()
        editor.putBoolean("isUserFirstTime", false)
        editor.apply()
    }


}