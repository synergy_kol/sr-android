package com.met.ahvara.repository

import androidx.lifecycle.LiveData
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.dao.LoginDao
import com.met.ahvara.dao.MarkAsArrivedDao
import com.met.ahvara.dao.SubmitCheckinDao
import com.met.ahvara.database.AhvaraDatabase
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.model.StatusChangeRequest
import com.met.ahvara.model.SubmitCheckinRequest
import com.met.ahvara.utils.subscribeOnBackground
import com.met.ahvara.webservice.RetrofitService
import okhttp3.RequestBody

class MainRepository constructor(
    private val retrofitService: RetrofitService, application: AhvaraApplication
) {
    private val database = AhvaraDatabase.getInstance(application)

    private var loginDao: LoginDao
    private var loginData: LiveData<LoginResponse>

    private var submitCheckinDao: SubmitCheckinDao
    private var submitCheckinRequest: LiveData<SubmitCheckinRequest>

    private var markAsArrivedDao: MarkAsArrivedDao
    private var markAsArrivedRequest: LiveData<StatusChangeRequest>


    init {
        loginDao = database.loginDao()
        loginData = loginDao.getLoginData()

        submitCheckinDao = database.submitCheckinDao()
        submitCheckinRequest = submitCheckinDao.getSubmitCheckinData()

        markAsArrivedDao = database.markAsArrivedDao()
        markAsArrivedRequest = markAsArrivedDao.getMarkedAsArrivedData()
    }

    fun markAsArrivedDataInsert(note: StatusChangeRequest) {
//        Single.just(loginDao.insert(note))
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe()
        subscribeOnBackground {
            markAsArrivedDao.insert(note)
        }
    }

    fun getmarkAsArrivedData(): LiveData<StatusChangeRequest> {
        return markAsArrivedRequest
    }


    fun deleteAllMarkAsArrivedData() {
        markAsArrivedDao.deleteMarkAsArrivedData()
    }

    fun checkinDataInsert(note: SubmitCheckinRequest) {
//        Single.just(loginDao.insert(note))
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe()
        subscribeOnBackground {
            submitCheckinDao.insert(note)
        }
    }

    fun getCheckinData(): LiveData<SubmitCheckinRequest> {
        return submitCheckinRequest
    }


    fun loginDataInsert(note: LoginResponse) {
//        Single.just(loginDao.insert(note))
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe()

        loginDao.insert(note)
    }


    fun loginUpdate(note: LoginResponse) {
        subscribeOnBackground {
            loginDao.update(note)
        }
    }

    fun loginDelete(note: LoginResponse) {
        subscribeOnBackground {
            loginDao.delete(note)
        }
    }

    fun deleteAllLoginData() {
        loginDao.deleteLoginData()
    }

    fun deleteAllCheckin() {
        submitCheckinDao.deleteSubmitCheckinData()
    }

    fun getLoginData(): LiveData<LoginResponse> {
        return loginData
    }

    //login
    fun doLogin(requestBody: RequestBody) = retrofitService.login(requestBody)
    fun doForgotPassword(requestBody: RequestBody) = retrofitService.forgotPassword(requestBody)
    fun doResetPassword(requestBody: RequestBody) = retrofitService.resetPassword(requestBody)
    //login

    //dashboard
    fun doAppointmentList(token: String, requestBody: RequestBody) =
        retrofitService.appointmentList(token, requestBody)

    fun doAppointmentProgressDailyWeekly(token: String) =
        retrofitService.appointmentProgressDailyWeekly(token)

    fun doWorkOrderList(token: String) = retrofitService.workOrderList(token)
    fun doTasksList(token: String) = retrofitService.taskList(token)
    fun doTasksSearchList(token: String, requestBody: RequestBody) =
        retrofitService.taskSearch(token, requestBody)

    fun doUpNextAppointmentList(token: String) = retrofitService.upNextAppointment(token)

    fun doSubmitCheckin(token: String, requestBody: RequestBody) =
        retrofitService.submitCheckin(token, requestBody)

    fun doSubmitCheckout(token: String, requestBody: RequestBody) =
        retrofitService.submitCheckout(token, requestBody)

    fun doStatusChange(token: String, requestBody: RequestBody) =
        retrofitService.changeStatus(token, requestBody)
    //dashboard

    //customer
    fun doCustomerList(token: String) = retrofitService.customerList(token)
    fun doCustomerSearchList(token: String, requestBody: RequestBody) =
        retrofitService.searchCustomer(token, requestBody)

    fun doCustomerDetails(token: String, requestBody: RequestBody) =
        retrofitService.customerDetails(token, requestBody)

    fun doAddCustomer(token: String, requestBody: RequestBody) =
        retrofitService.addCustomer(token, requestBody)
    //customer

    //leads
    fun doLeadsList(token: String) = retrofitService.leadsList(token)

    fun doLeadsSearchList(token: String, requestBody: RequestBody) =
        retrofitService.searchLead(token, requestBody)

    fun doLeadDetails(token: String, requestBody: RequestBody) =
        retrofitService.leadDetails(token, requestBody)

    //leads

    //work orders
    fun doWorkOrderDetails(token: String, requestBody: RequestBody) =
        retrofitService.workorderDetails(token, requestBody)

    //work orders

    //for cancellation/reschedule
    fun doReasonsList(token: String, requestBody: RequestBody) =
        retrofitService.reasonsList(token, requestBody)

    fun doAppointmentCancel(token: String, requestBody: RequestBody) =
        retrofitService.appointmentCancel(token, requestBody)

    fun doAppointmentReschedule(token: String, requestBody: RequestBody) =
        retrofitService.appointmentReschedule(token, requestBody)
    //for cancellation/reschedule

}