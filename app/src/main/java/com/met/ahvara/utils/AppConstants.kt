package com.met.ahvara.utils

class AppConstants {
    companion object {
        const val TEXT_USER_DETAILS = "user_details"
        const val TEXT_DEVICE_TYPE = "Android"
        const val TEXT_SOURCE = "App"

        const val NAME_DATABASE = "ahvara_database"

        const val INTENT_LEAD_ID = "intent_lead_id"
        const val INTENT_CUSTOMER_ID = "intent_customer_id"
        const val INTENT_WORK_ORDER_ID = "intent_work_order_id"
        const val INTENT_APPOINTMENT_ID = "intent_appointment_id"
        const val INTENT_UP_NEXT_APPOINTMENT = "intent_up_next_appointment"
        const val INTENT_APPOINTMENT_LIST = "intent_appointment_list"
        const val INTENT_APPOINTMENT_DETAILS = "intent_appointment_details"
        const val INTENT_EMAIL_ID = "intent_appointment_list"

        const val LOGIN_TYPE_SR = "4"
        const val LOGIN_TYPE_ST = "5"

        const val TIMER_INTERVAL = 1000

        const val APPOINTMENT_STATUS_UNASSIGNED = "0"
        const val APPOINTMENT_STATUS_PENDING = "1"
        const val APPOINTMENT_STATUS_SCHEDULED = "2"
        const val APPOINTMENT_STATUS_HOLD = "3"
        const val APPOINTMENT_STATUS_IN_PROGRESS = "4"
        const val APPOINTMENT_STATUS_REVIEW = "5"
        const val APPOINTMENT_STATUS_COMPLETED = "6"
        const val APPOINTMENT_STATUS_RESCHEDULED = "7"
        const val APPOINTMENT_STATUS_ARRIVED = "8"
        const val APPOINTMENT_STATUS_CANCELLED = "9"

        const val PRIORITY_HIGH = "1"
        const val PRIORITY_NORMAL = "2"
        const val PRIORITY_LOW = "3"

        const val REASON_CANCEL = "cancel"
        const val REASON_RESCHEDULE = "reschedule"


        /*: work order -> priority
(5:45:38 PM) Tanmay Pal: 1-high,2-normal,3-low
(5:45:51 PM) Tanmay Pal: 0=Unassigned,1=Pending,2=Scheduled,3=Hold,4=In Progress,5=Review,6=Complete*/


        var checkedAppointmentIds = ArrayList<String>()

        const val APPOINTMENT_FILTER_BY_DATE = "date"
        const val APPOINTMENT_FILTER_BY_MONTH = "month"
        const val APPOINTMENT_FILTER_BY_WEEK = "week"

    }
}