package com.met.ahvara.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LifecycleOwner
import com.github.thunder413.datetimeutils.DateTimeUnits
import com.github.thunder413.datetimeutils.DateTimeUtils
import com.google.gson.Gson
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.DialogSpinkitBinding
import com.met.ahvara.ui.account.AccountActivity
import com.met.ahvara.ui.appointments.AppointmentsActivity
import com.met.ahvara.ui.customer.CustomerListActivity
import com.met.ahvara.ui.dashboard.DashboardActivity
import com.met.ahvara.ui.documents.DocumentsActivity
import com.met.ahvara.ui.leads.LeadsListActivity
import com.met.ahvara.ui.notifications.NotificationsActivity
import com.met.ahvara.ui.reports.ReportsActivity
import com.met.ahvara.ui.schedule.ScheduleActivity
import com.met.ahvara.ui.task.TaskListActivity
import com.met.ahvara.ui.units.UnitsActivity
import com.met.ahvara.ui.workorder.WorkOrdersActivity
import com.met.ahvara.viewmodel.DashboardViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.math.min


object CommonUtils {
    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target ?: "").matches()
    }

    fun getJsonFromObject(`object`: Any?, classType: Class<*>?): String {
        var jsonText = ""
        if (`object` != null) {
            val gson = Gson()
            jsonText = gson.toJson(`object`, classType)
        }
        return jsonText
    }

    fun getObjectFromJson(jsonText: String?, classType: Class<*>?): Any? {
        var `object`: Any? = null
        if (jsonText != null) {
            val gson = Gson()
            `object` = gson.fromJson(jsonText, classType)
        }
        return `object`
    }

    fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    fun hideSoftKeyboard(activity: Activity, view: View) {
        val inputMethodManager = activity
            .getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun getTimeFromYYYYMMDD(userTime: String?): String {
        var newFormatTime = ""
        try {                               //"2022-04-01 04:00:00"
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            val output = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
            //val output = SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH)
            val d: Date? = sdf.parse(userTime!!)
            newFormatTime = output.format(d!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return newFormatTime
    }

    fun getMonthDayYearFromYYYYMMDDHHmmss(userTime: String?): String {
        var newFormatTime = ""
        try {                               //"2022-04-01 04:00:00"
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            val output = SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH)
            //val output = SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH)
            var d: Date? = null
            d = sdf.parse(userTime!!)
            newFormatTime = output.format(d!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return newFormatTime
    }

    fun getMonthDayYearFromYYYYMMDD(userTime: String?): String {
        var newFormatTime = ""
        try {                               //"2022-04-01"
            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            val output = SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH)
            //val output = SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH)
            var d: Date? = null
            d = sdf.parse(userTime.toString())
            newFormatTime = output.format(d!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return newFormatTime
    }

    fun getYYYYMMDDHHMMSSFromYYYYMMDDHHMMA(userTime: String?): String {
        var newFormatTime = ""
        try {                               //"2022-04-01 10:10 AM"
            val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH)
            //2022-04-19 04:14:49
            val output = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            //val output = SimpleDateFormat("dd/MM/yyyy hh:mm ", Locale.ENGLISH)
            var d: Date? = null
            d = sdf.parse(userTime!!)
            newFormatTime = output.format(d!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return newFormatTime
    }

    fun getDateUsingPattern(userTime: String, fromPattern: String, toPattern: String): String {
        var newFormatTime = ""
        try {                               //"2022-04-01 10:10 AM"
            val sdf = SimpleDateFormat(fromPattern, Locale.ENGLISH)
            //2022-04-19 04:14:49
            val output = SimpleDateFormat(toPattern, Locale.ENGLISH)
            //val output = SimpleDateFormat("dd/MM/yyyy hh:mm ", Locale.ENGLISH)
            var d: Date? = null
            d = sdf.parse(userTime)
            newFormatTime = output.format(d)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return newFormatTime
    }


    fun hhmmsstoMilis(myDate: String?): Long {
        return try {
            //    2019-10-31 03:30 AM;
            val sdf =
                SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault())
            val date = sdf.parse(myDate)
            date!!.time
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            0
        }
    }

    fun getDateDifference(mContext: Context, date2: String?): String {
        // Dates can be date object or date string
        val date = Date()
        //val date2 = "2022-04-19 04:14:49"
        // Get difference in milliseconds
        val diff1 = DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MILLISECONDS)
        // Get difference in seconds
        val diff2 = DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.SECONDS)
        // Get difference in minutes
        val diffMinutes =
            if (DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MINUTES) > 0) {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MINUTES)
            } else {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MINUTES) * (-1)
            }
        // Get difference in hours
        val diffHours =
            if (DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.HOURS) > 0) {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.HOURS)
            } else {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.HOURS) * (-1)
            }

        // Get difference in days
        val diffDays =
            if (DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.DAYS) > 0) {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.DAYS)
            } else {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.DAYS) * (-1)
            }

        /*  Log.e(
              "difference",
              date.toString()
          )
          Log.e(
              "difference",
              date2.toString()
          )
          Log.e(
              "difference111",
              "$diffDays::$diffHours::$diffMinutes"
          )*/
        return when {
            diffDays > 0 -> {
                mContext.resources.getQuantityString(R.plurals.day_left, diffDays, diffDays)
            }
            diffHours > 0 -> {
                mContext.resources.getQuantityString(R.plurals.hour_left, diffHours, diffHours)
            }
            else -> {
                mContext.resources.getQuantityString(
                    R.plurals.minute_left,
                    diffMinutes,
                    diffMinutes
                )
            }
        }
    }
    fun getDateDifferenceDays(mContext: Context, date2: String?): String {
        // Dates can be date object or date string
        val date = Date()
        //val date2 = "2022-04-19 04:14:49"
        // Get difference in milliseconds
        val diff1 = DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MILLISECONDS)
        // Get difference in seconds
        val diff2 = DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.SECONDS)
        // Get difference in minutes
        val diffMinutes =
            if (DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MINUTES) > 0) {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MINUTES)
            } else {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MINUTES) * (-1)
            }
        // Get difference in hours
        val diffHours =
            if (DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.HOURS) > 0) {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.HOURS)
            } else {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.HOURS) * (-1)
            }

        // Get difference in days
        val diffDays =
            if (DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.DAYS) > 0) {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.DAYS)
            } else {
                DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.DAYS) * (-1)
            }

        /*  Log.e(
              "difference",
              date.toString()
          )
          Log.e(
              "difference",
              date2.toString()
          )
          Log.e(
              "difference111",
              "$diffDays::$diffHours::$diffMinutes"
          )*/
        return when {
            diffDays > 0 -> {
                mContext.resources.getQuantityString(R.plurals.day_to_go, diffDays, diffDays)
            }
            diffHours > 0 -> {
                mContext.resources.getQuantityString(R.plurals.hour_to_go, diffHours, diffHours)
            }
            else -> {
                mContext.resources.getQuantityString(
                    R.plurals.minute_to_go,
                    diffMinutes,
                    diffMinutes
                )
            }
        }
    }

    fun showErrorLog(tag: String, text: String) {
        try {
            val maxLogSize = 2000
            for (i in 0..text.length / maxLogSize) {
                val start = i * maxLogSize
                var end = (i + 1) * maxLogSize
                end = min(end, text.length)
                Log.e("" + tag, "" + text)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun showLogoutDialog(
        mContext: Context, lifecycleOwner: LifecycleOwner,
        loginViewModel: LoginViewModel,
        dashboardViewModel: DashboardViewModel?
    ) {
        val builder = AlertDialog.Builder(mContext)
        builder.setTitle(mContext.getString(R.string.logout))
        builder.setMessage(mContext.getString(R.string.are_you_sure))

        builder.setPositiveButton(mContext.getString(R.string.yes)) { dialog, which ->
            AhvaraApplication.ahvaraPreference.logOut(
                mContext,
                lifecycleOwner,
                loginViewModel,
                dashboardViewModel
            )
            dialog.dismiss()
        }

        builder.setNegativeButton(mContext.getString(R.string.no)) { dialog, which ->
            // Do nothing
            dialog.dismiss()
        }
        val alert = builder.create()
        alert.show()
    }

    fun getCurrentTimeHHmm(): String {
        val dateTime: String

        val calendar: Calendar = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        dateTime = simpleDateFormat.format(calendar.time).toString()
        //format1.text = dateTime
        return dateTime
    }

    fun getYYYYMMDDHHMMSSFromHHMM(userTime: String?): String {
        var newFormatTime = ""
        try {                               //"2022-04-01 10:10 AM"
            val sdf = SimpleDateFormat("HH:mm", Locale.ENGLISH)
            //2022-04-19 04:14:49
            val output = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            //val output = SimpleDateFormat("dd/MM/yyyy hh:mm ", Locale.ENGLISH)
            var d: Date? = null
            d = sdf.parse(userTime!!)
            newFormatTime = output.format(d!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return newFormatTime
    }


    fun getTimeDifference(date2: String?): Long {
        //Specifying the pattern of input date and time
        //Specifying the pattern of input date and time
        //.toDateYMDInDigits()            // 2019-08-13
        val sdf = SimpleDateFormat("yyyy-mm-dd HH:mm")
//        val dateString = "22-03-2017 11:18:32"

        //formatting the dateString to convert it into a Date
        val date = sdf.parse(date2)
        println("Given Time in milliseconds : " + date.time)
        val calendar = Calendar.getInstance()
        //Setting the Calendar date and time to the given date and time
        calendar.time = date
        println("Given Time in milliseconds : " + calendar.timeInMillis)

        return calendar.timeInMillis
    }

    fun getProgressDialog(mContext: Context): AlertDialog {
        lateinit var popupClockIn: AlertDialog
        val dialogBinding = DialogSpinkitBinding.inflate(LayoutInflater.from(mContext))
        val builder = AlertDialog.Builder(mContext, R.style.popupDialog)
        builder.setCancelable(false)
        builder.setView(dialogBinding.root)

        popupClockIn = builder.create()

        return popupClockIn
    }

    fun getDatefromString(time: String): Date {
        // val dtStart = "2010-10-15T09:27:37Z"
        //        val sdf = SimpleDateFormat("yyyy-mm-dd HH:mm")

        val format = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH)

        val date = format.parse(time)
        println(date)

        return date!!
    }

    fun getTimeDateDifference(date2: Date?): String {
        // Dates can be date object or date string
        val date = Date()
        //val date2 = "2022-04-19 04:14:49"
        // Get difference in milliseconds
        val diff1 = DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MILLISECONDS)
        // Get difference in seconds
        val diffSeconds = DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.SECONDS)
        // Get difference in minutes
        val diffMinutes = DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.MINUTES)
        // Get difference in hours
        val diffHours = DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.HOURS)
        // Get difference in days
        val diffDays = DateTimeUtils.getDateDiff(date, date2, DateTimeUnits.DAYS)

        return "$diffHours:$diffMinutes:$diffDays"
        /* showErrorLog(
             "timeDiff",
             "miliseconds--" + diff1 +
                     "\nseconds--" + diff2 +
                     "\nminutes--" + diffMinutes +
                     "\nhours--" + diffHours +
                     "\ndays--" + diffDays
         )
 */
/*
        when {
            diffDays > 0 -> {
                return if (diffDays > 1) {
                    mContext.getString(R.string.days_left, diffDays)
                } else {
                    mContext.getString(R.string.day_left, diffDays)
                }
            }
            diffHours > 0 -> {
                return if (diffHours > 1) {
                    mContext.getString(R.string.hours_left, diffHours)
                } else {
                    mContext.getString(R.string.hour_left, diffHours)
                }
            }
            else -> {
                return if (diffMinutes > 1) {
                    mContext.getString(R.string.minutes_left, diffMinutes)
                } else {
                    mContext.getString(R.string.minute_left, diffMinutes)
                }
            }
        }
*/
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    fun convertPixelsToDp(px: Float, context: Context): Float {
        return px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    fun getFormattedStopWatch(ms: Long): String {
        var milliseconds = ms
        val hours = TimeUnit.MILLISECONDS.toHours(milliseconds)
        milliseconds -= TimeUnit.HOURS.toMillis(hours)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
        milliseconds -= TimeUnit.MINUTES.toMillis(minutes)
        val seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds)

        return "${if (hours < 10) "0" else ""}$hours:" +
                "${if (minutes < 10) "0" else ""}$minutes:" +
                "${if (seconds < 10) "0" else ""}$seconds"
    }


    fun hhmmssToMillis(time: String): Long {
        val hhmmss = time.split(":").toTypedArray()
        var hours = 0
        hours = hhmmss[0].toInt()
        val minutes: Int = hhmmss[1].toInt()
        val seconds: Int = hhmmss[2].toInt()
        // } else {
        //   minutes = hhmmss[0].toInt()
        //  seconds = hhmmss[1].toInt()
        //}
        return ((hours * 60 + minutes * 60 + seconds) * 1000).toLong()
    }

    fun navigateNavigationDrawer(
        mContext: Context,
        item: MenuItem,
        lifecycleOwner: LifecycleOwner,
        loginViewModel: LoginViewModel,
        dashboardViewModel: DashboardViewModel?
    ) {
        when (item.itemId) {
            R.id.drawer_dashboard -> {
                if ((mContext as Activity) !is DashboardActivity) {
                    val intent = Intent(mContext, DashboardActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_schedule -> {
                if ((mContext as Activity) !is ScheduleActivity) {
                    val intent = Intent(mContext, ScheduleActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_appointments -> {
                if ((mContext as Activity) !is AppointmentsActivity) {
                    val intent = Intent(mContext, AppointmentsActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_tasks -> {
                if ((mContext as Activity) !is TaskListActivity) {
                    val intent = Intent(mContext, TaskListActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_leads -> {
                if ((mContext as Activity) !is LeadsListActivity) {
                    val intent = Intent(mContext, LeadsListActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_customers -> {
                if ((mContext as Activity) !is CustomerListActivity) {
                    val intent = Intent(mContext, CustomerListActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_units -> {
                if ((mContext as Activity) !is UnitsActivity) {
                    val intent = Intent(mContext, UnitsActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_orders -> {
                if ((mContext as Activity) !is WorkOrdersActivity) {
                    val intent = Intent(mContext, WorkOrdersActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_documents -> {
                if ((mContext as Activity) !is DocumentsActivity) {
                    val intent = Intent(mContext, DocumentsActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_reports -> {
                if ((mContext as Activity) !is ReportsActivity) {
                    val intent = Intent(mContext, ReportsActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_account -> {
                if ((mContext as Activity) !is AccountActivity) {
                    val intent = Intent(mContext, AccountActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_notifications -> {
                if ((mContext as Activity) !is NotificationsActivity) {
                    val intent = Intent(mContext, NotificationsActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
            R.id.drawer_logout -> {
                showLogoutDialog(mContext, lifecycleOwner, loginViewModel, dashboardViewModel)
            }

        }
    }

/*
    fun getMarkerBitmapFromView(@DrawableRes resId: Int, mContext: Context): Bitmap? {
        val customMarkerView: View =
            (mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?).inflate(
                R.layout.view_custom_marker,
                null
            )
        val markerImageView: ImageView =
            customMarkerView.findViewById<View>(R.id.profile_image) as ImageView
        markerImageView.setImageResource(resId)
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        customMarkerView.layout(
            0,
            0,
            customMarkerView.measuredWidth,
            customMarkerView.measuredHeight
        )
        customMarkerView.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(
            customMarkerView.measuredWidth, customMarkerView.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = customMarkerView.background
        drawable?.draw(canvas)
        customMarkerView.draw(canvas)
        return returnedBitmap
    }
*/

    fun isValidPassword(password: String?): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = password?.let { pattern.matcher(it) }!!
        return matcher.matches()
    }
}