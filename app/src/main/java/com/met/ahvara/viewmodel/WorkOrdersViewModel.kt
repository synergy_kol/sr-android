package com.met.ahvara.viewmodel

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.model.WorkOrderDetailsResponse
import com.met.ahvara.model.WorkOrderListResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WorkOrdersViewModel(
    private val repository: MainRepository,
    ahvaraApplication: AhvaraApplication
) : AndroidViewModel(ahvaraApplication) {

    val workOrderDetailsResponse = MutableLiveData<WorkOrderDetailsResponse>()
    val errorMessage = MutableLiveData<String>()

    fun getWorkOrdersDetailsResponse(token: String, requestBody: RequestBody) {
        val response = repository.doWorkOrderDetails(token, requestBody)
        response.enqueue(object : Callback<WorkOrderDetailsResponse> {
            override fun onResponse(
                call: Call<WorkOrderDetailsResponse>,
                response: Response<WorkOrderDetailsResponse>
            ) {
                workOrderDetailsResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<WorkOrderDetailsResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    val workOrderListResponse = MutableLiveData<WorkOrderListResponse>()
    fun getWorkOrdersListResponse(token: String) {
        val response = repository.doWorkOrderList(token)
        response.enqueue(object : Callback<WorkOrderListResponse> {
            override fun onResponse(
                call: Call<WorkOrderListResponse>,
                response: Response<WorkOrderListResponse>
            ) {
                workOrderListResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<WorkOrderListResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }


}