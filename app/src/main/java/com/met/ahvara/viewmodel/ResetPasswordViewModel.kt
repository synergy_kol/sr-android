package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.ResetPasswordResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResetPasswordViewModel(private val repository: MainRepository) : ViewModel() {

    val resetPasswordResponse = MutableLiveData<ResetPasswordResponse>()
    val errorMessage = MutableLiveData<String>()

    fun getResetPasswordResponse(requestBody: RequestBody) {
        val response = repository.doResetPassword(requestBody)
        response.enqueue(object : Callback<ResetPasswordResponse> {
            override fun onResponse(
                call: Call<ResetPasswordResponse>,
                response: Response<ResetPasswordResponse>
            ) {
                resetPasswordResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<ResetPasswordResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}