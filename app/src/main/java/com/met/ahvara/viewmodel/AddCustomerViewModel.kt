package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.AddCustomerResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddCustomerViewModel(private val repository: MainRepository) : ViewModel() {

    val addCustomerResponse = MutableLiveData<AddCustomerResponse>()
    val errorMessage = MutableLiveData<String>()

    fun getAddCustomerResponse(token:String,requestBody: RequestBody) {
        val response = repository.doAddCustomer(token,requestBody)
        response.enqueue(object : Callback<AddCustomerResponse> {
            override fun onResponse(
                call: Call<AddCustomerResponse>,
                response: Response<AddCustomerResponse>
            ) {
                addCustomerResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<AddCustomerResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}