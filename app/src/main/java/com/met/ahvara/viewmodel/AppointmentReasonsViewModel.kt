package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.CancelRescheduleResponse
import com.met.ahvara.model.CustomerDetailsResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AppointmentReasonsViewModel(private val repository: MainRepository) : ViewModel() {
    val errorMessage = MutableLiveData<String>()
    val reasonsResponse = MutableLiveData<CancelRescheduleResponse>()

    fun getReasonsResponse(token: String,requestBody: RequestBody) {
        val response = repository.doReasonsList(token,requestBody)
        response.enqueue(object : Callback<CancelRescheduleResponse> {
            override fun onResponse(
                call: Call<CancelRescheduleResponse>,
                response: Response<CancelRescheduleResponse>
            ) {
                reasonsResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<CancelRescheduleResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

 }