package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.CustomerListResponse
import com.met.ahvara.model.LeadsListResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LeadsListingViewModel(private val repository: MainRepository) : ViewModel() {
    val errorMessage = MutableLiveData<String>()
    val leadsListResponse = MutableLiveData<LeadsListResponse>()

    fun getLeadsListResponse(token: String) {
        val response = repository.doLeadsList(token)
        response.enqueue(object : Callback<LeadsListResponse> {
            override fun onResponse(
                call: Call<LeadsListResponse>,
                response: Response<LeadsListResponse>
            ) {
                leadsListResponse.postValue(response.body())
            }
            override fun onFailure(call: Call<LeadsListResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    fun getLeadsSearchListResponse(token: String, requestBody: RequestBody) {
        val response = repository.doLeadsSearchList(token, requestBody)
        response.enqueue(object : Callback<LeadsListResponse> {
            override fun onResponse(
                call: Call<LeadsListResponse>,
                response: Response<LeadsListResponse>
            ) {
                if (response.isSuccessful) {
                    leadsListResponse.postValue(response.body())
                }
            }
            override fun onFailure(call: Call<LeadsListResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}