package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.LeadsDetailsResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LeadsDetailsViewModel(private val repository: MainRepository) : ViewModel() {
    val errorMessage = MutableLiveData<String>()
    val leadsDetailsResponse = MutableLiveData<LeadsDetailsResponse>()

    fun getLeadsDetailsResponse(token: String, requestBody: RequestBody) {
        val response = repository.doLeadDetails(token, requestBody)
        response.enqueue(object : Callback<LeadsDetailsResponse> {
            override fun onResponse(
                call: Call<LeadsDetailsResponse>,
                response: Response<LeadsDetailsResponse>
            ) {
                leadsDetailsResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<LeadsDetailsResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}