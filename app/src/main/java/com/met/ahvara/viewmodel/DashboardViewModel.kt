package com.met.ahvara.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.*
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.AppConstants
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashboardViewModel(private val repository: MainRepository) : ViewModel() {
    val observerErrorMessage = MutableLiveData<String>()
    val observerAppointmentListResponse = MutableLiveData<AppointmentListResponse>()

    fun getAppointmentListResponse(
        token: String,
        requestBody: RequestBody
    ) {
        val response = repository.doAppointmentList(token, requestBody)
        response.enqueue(object : Callback<AppointmentListResponse> {
            override fun onResponse(
                call: Call<AppointmentListResponse>,
                response: Response<AppointmentListResponse>
            ) {
                observerAppointmentListResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<AppointmentListResponse>, t: Throwable) {
                observerErrorMessage.postValue(t.message)
            }
        })
    }

    val observerProgressDailyWeeklyResponse =
        MutableLiveData<AppointmentProgressDailyWeeklyResponse>()

    fun getProgressDailyWeeklyResponse(token: String) {
        val response = repository.doAppointmentProgressDailyWeekly(token)
        response.enqueue(object : Callback<AppointmentProgressDailyWeeklyResponse> {
            override fun onResponse(
                call: Call<AppointmentProgressDailyWeeklyResponse>,
                response: Response<AppointmentProgressDailyWeeklyResponse>
            ) {
                observerProgressDailyWeeklyResponse.postValue(response.body())
            }

            override fun onFailure(
                call: Call<AppointmentProgressDailyWeeklyResponse>,
                t: Throwable
            ) {
                observerErrorMessage.postValue(t.message)
            }
        })
    }

    val observerWorkOrderListResponse = MutableLiveData<WorkOrderListResponse>()
    fun getWorkOrderListResponse(token: String) {
        val response = repository.doWorkOrderList(token)
        response.enqueue(object : Callback<WorkOrderListResponse> {
            override fun onResponse(
                call: Call<WorkOrderListResponse>,
                response: Response<WorkOrderListResponse>
            ) {
                observerWorkOrderListResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<WorkOrderListResponse>, t: Throwable) {
                observerErrorMessage.postValue(t.message)
            }
        })
    }

    val observerTasksListResponse = MutableLiveData<TaskListResponse>()
    fun getTaskListResponse(token: String) {
        val response = repository.doTasksList(token)
        response.enqueue(object : Callback<TaskListResponse> {
            override fun onResponse(
                call: Call<TaskListResponse>,
                response: Response<TaskListResponse>
            ) {
                observerTasksListResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<TaskListResponse>, t: Throwable) {
                observerErrorMessage.postValue(t.message)
            }
        })
    }

    // val observerTasksListResponse = MutableLiveData<TaskListResponse>()
    fun getTaskSearchListResponse(token: String, requestBody: RequestBody) {
        val response = repository.doTasksSearchList(token, requestBody)
        response.enqueue(object : Callback<TaskListResponse> {
            override fun onResponse(
                call: Call<TaskListResponse>,
                response: Response<TaskListResponse>
            ) {
                observerTasksListResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<TaskListResponse>, t: Throwable) {
                observerErrorMessage.postValue(t.message)
            }
        })
    }

    val observerUpNextAppointment = MutableLiveData<UpNextAppointmentResponse>()
    fun getUpNextAppointmentResponse(token: String) {
        val response = repository.doUpNextAppointmentList(token)
        response.enqueue(object : Callback<UpNextAppointmentResponse> {
            override fun onResponse(
                call: Call<UpNextAppointmentResponse>,
                response: Response<UpNextAppointmentResponse>
            ) {
                observerUpNextAppointment.postValue(response.body())
            }

            override fun onFailure(call: Call<UpNextAppointmentResponse>, t: Throwable) {
                observerErrorMessage.postValue(t.message)
            }
        })
    }

    val observerSubmitCheckinResponse = MutableLiveData<SubmitCheckinResponse>()
    fun getSubmitCheckinResponse(token: String, requestBody: RequestBody) {
        val response = repository.doSubmitCheckin(token, requestBody)
        response.enqueue(object : Callback<SubmitCheckinResponse> {
            override fun onResponse(
                call: Call<SubmitCheckinResponse>,
                response: Response<SubmitCheckinResponse>
            ) {
                observerSubmitCheckinResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<SubmitCheckinResponse>, t: Throwable) {
                observerErrorMessage.postValue(t.message)
            }
        })
    }


    fun insertCheckin(note: SubmitCheckinRequest) {
        repository.checkinDataInsert(note)
    }

    /*  fun updateCheckin(note: SubmitCheckinRequest) {
          repository.cUpdate(note)
      }

      fun deleteCheckin(note: SubmitCheckinRequest) {
          repository.loginDelete(note)
      }*/

    fun deleteAllCheckin() {
        repository.deleteAllCheckin()
    }

    val submitCheckinRequest = MutableLiveData<SubmitCheckinRequest>()


    fun getAllCheckin(): LiveData<SubmitCheckinRequest> {
        // submitCheckinRequest.postValue(repository.getCheckinData())
        return repository.getCheckinData()
    }

    val observerSubmitCheckOutResponse = MutableLiveData<SubmitCheckOutResponse>()
    fun getSubmitCheckOutResponse(token: String, requestBody: RequestBody) {
        val response = repository.doSubmitCheckout(token, requestBody)
        response.enqueue(object : Callback<SubmitCheckOutResponse> {
            override fun onResponse(
                call: Call<SubmitCheckOutResponse>,
                response: Response<SubmitCheckOutResponse>
            ) {
                observerSubmitCheckOutResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<SubmitCheckOutResponse>, t: Throwable) {
                observerErrorMessage.postValue(t.message)
            }
        })
    }

    val observerMarkAsArrivedStatusChangeResponse = MutableLiveData<StatusChangeResponse>()

    fun getStatusChangeResponse(token: String, requestBody: RequestBody, type: String) {
        val response = repository.doStatusChange(token, requestBody)
        response.enqueue(object : Callback<StatusChangeResponse> {
            override fun onResponse(
                call: Call<StatusChangeResponse>,
                response: Response<StatusChangeResponse>
            ) {
                when (type) {
                    AppConstants.APPOINTMENT_STATUS_ARRIVED -> {
                        observerMarkAsArrivedStatusChangeResponse.postValue(response.body())
                    }
                }
            }

            override fun onFailure(call: Call<StatusChangeResponse>, t: Throwable) {
                observerErrorMessage.postValue(t.message)
            }
        })
    }

    fun insertMarkAsArrived(note: StatusChangeRequest) {
        repository.markAsArrivedDataInsert(note)
    }


    fun deleteAllMarkAsArrived() {
        repository.deleteAllMarkAsArrivedData()
    }
    fun getAllMarkAsArrived(): LiveData<StatusChangeRequest> {
        return repository.getmarkAsArrivedData()
    }

}