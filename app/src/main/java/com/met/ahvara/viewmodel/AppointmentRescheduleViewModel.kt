package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.AppointmentRescheduleResponse
import com.met.ahvara.model.CustomerDetailsResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AppointmentRescheduleViewModel(private val repository: MainRepository) : ViewModel() {
    val errorMessage = MutableLiveData<String>()
    val appointmentRescheduleResponse = MutableLiveData<AppointmentRescheduleResponse>()

    fun getAppointmentRescheduleResponse(token: String,requestBody: RequestBody) {
        val response = repository.doAppointmentReschedule(token,requestBody)
        response.enqueue(object : Callback<AppointmentRescheduleResponse> {
            override fun onResponse(
                call: Call<AppointmentRescheduleResponse>,
                response: Response<AppointmentRescheduleResponse>
            ) {
                appointmentRescheduleResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<AppointmentRescheduleResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
 }