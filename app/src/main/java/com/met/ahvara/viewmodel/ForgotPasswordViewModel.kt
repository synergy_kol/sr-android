package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.ForgotPasswordResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordViewModel(private val repository: MainRepository) : ViewModel() {

    val forgotPasswordResponse = MutableLiveData<ForgotPasswordResponse>()
    val errorMessage = MutableLiveData<String>()

    fun getForgotPasswordResponse(requestBody: RequestBody) {
        val response = repository.doForgotPassword(requestBody)
        response.enqueue(object : Callback<ForgotPasswordResponse> {
            override fun onResponse(
                call: Call<ForgotPasswordResponse>,
                response: Response<ForgotPasswordResponse>
            ) {
                forgotPasswordResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<ForgotPasswordResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}