package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.AppointmentCancelResponse
import com.met.ahvara.model.CustomerDetailsResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AppointmentCancellationViewModel(private val repository: MainRepository) : ViewModel() {
    val errorMessage = MutableLiveData<String>()
    val appointmentCancelResponse = MutableLiveData<AppointmentCancelResponse>()

    fun getAppointmentCancelResponse(token: String,requestBody: RequestBody) {
        val response = repository.doAppointmentCancel(token,requestBody)
        response.enqueue(object : Callback<AppointmentCancelResponse> {
            override fun onResponse(
                call: Call<AppointmentCancelResponse>,
                response: Response<AppointmentCancelResponse>
            ) {
                appointmentCancelResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<AppointmentCancelResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

 }