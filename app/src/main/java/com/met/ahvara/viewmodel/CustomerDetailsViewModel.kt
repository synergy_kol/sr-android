package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.CustomerDetailsResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CustomerDetailsViewModel(private val repository: MainRepository) : ViewModel() {
    val errorMessage = MutableLiveData<String>()
    val customerDetailsResponse = MutableLiveData<CustomerDetailsResponse>()

    fun getCustomerDetailsResponse(token: String,requestBody: RequestBody) {
        val response = repository.doCustomerDetails(token,requestBody)
        response.enqueue(object : Callback<CustomerDetailsResponse> {
            override fun onResponse(
                call: Call<CustomerDetailsResponse>,
                response: Response<CustomerDetailsResponse>
            ) {
                customerDetailsResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<CustomerDetailsResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

 }