package com.met.ahvara.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.met.ahvara.model.CustomerListResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CustomerListingViewModel(private val repository: MainRepository) : ViewModel() {
    val errorMessage = MutableLiveData<String>()
    val customerListResponse = MutableLiveData<CustomerListResponse>()

    fun getCustomerListResponse(token: String) {
        val response = repository.doCustomerList(token)
        response.enqueue(object : Callback<CustomerListResponse> {
            override fun onResponse(
                call: Call<CustomerListResponse>,
                response: Response<CustomerListResponse>
            ) {
                customerListResponse.postValue(response.body())
            }
            override fun onFailure(call: Call<CustomerListResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    fun getCustomerSearchListResponse(token: String, requestBody: RequestBody) {
        val response = repository.doCustomerSearchList(token, requestBody)
        response.enqueue(object : Callback<CustomerListResponse> {
            override fun onResponse(
                call: Call<CustomerListResponse>,
                response: Response<CustomerListResponse>
            ) {
                if (response.isSuccessful) {
                    customerListResponse.postValue(response.body())
                }
            }
            override fun onFailure(call: Call<CustomerListResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}