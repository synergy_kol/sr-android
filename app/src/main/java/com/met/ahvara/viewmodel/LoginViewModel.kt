package com.met.ahvara.viewmodel

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel(
    private val repository: MainRepository,
    ahvaraApplication: AhvaraApplication
) : AndroidViewModel(ahvaraApplication) {
    private val allLoginData = repository.getLoginData()
    val loginResponse = MutableLiveData<LoginResponse>()
    val errorMessage = MutableLiveData<String>()

    fun getLoginResponse(requestBody: RequestBody) {
        val response = repository.doLogin(requestBody)
        response.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                loginResponse.postValue(response.body())
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    fun insert(loginResponse: LoginResponse) {
        repository.loginDataInsert(loginResponse)
    }

    fun update(loginResponse: LoginResponse) {
        repository.loginUpdate(loginResponse)
    }

    fun delete(loginResponse: LoginResponse) {
        repository.loginDelete(loginResponse)
    }

    fun deleteAllLogin() {
        repository.deleteAllLoginData()
    }

    fun getAllLogin(): LiveData<LoginResponse> {
        return allLoginData
    }
}