package com.met.ahvara.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.met.ahvara.model.StatusChangeRequest
import com.met.ahvara.model.SubmitCheckinRequest

@Dao
interface MarkAsArrivedDao {
    @Insert
    fun insert(request: StatusChangeRequest)

    @Update
    fun update(request: StatusChangeRequest)

    @Delete
    fun delete(request: StatusChangeRequest)

    @Query("delete from mark_as_arrived_table")
    fun deleteMarkAsArrivedData()

    //@Query("select * from login_table order by priority desc")
    @Query("select * from mark_as_arrived_table")
    fun getMarkedAsArrivedData(): LiveData<StatusChangeRequest>
}