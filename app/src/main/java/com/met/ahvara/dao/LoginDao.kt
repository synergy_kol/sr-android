package com.met.ahvara.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.met.ahvara.model.LoginResponse

@Dao
interface LoginDao {
    @Insert
    fun insert(note: LoginResponse)

    @Update
    fun update(note: LoginResponse)

    @Delete
    fun delete(note: LoginResponse)

    @Query("delete from login_response_table")
    fun deleteLoginData()

    @Query("select * from login_response_table")
    fun getLoginData(): LiveData<LoginResponse>
}