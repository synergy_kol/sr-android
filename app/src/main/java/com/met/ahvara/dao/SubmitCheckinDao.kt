package com.met.ahvara.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.met.ahvara.model.SubmitCheckinRequest

@Dao
interface SubmitCheckinDao {
    @Insert
    fun insert(submitCheckinRequest: SubmitCheckinRequest)

    @Update
    fun update(submitCheckinRequest: SubmitCheckinRequest)

    @Delete
    fun delete(submitCheckinRequest: SubmitCheckinRequest)

    @Query("delete from submit_checkin_table")
    fun deleteSubmitCheckinData()

    //@Query("select * from login_table order by priority desc")
    @Query("select * from submit_checkin_table")
    fun getSubmitCheckinData(): LiveData<SubmitCheckinRequest>
}