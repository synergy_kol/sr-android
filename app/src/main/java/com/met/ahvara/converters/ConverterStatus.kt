package com.met.ahvara.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.met.ahvara.model.Status
import java.lang.reflect.Type

class ConverterStatus {
    @TypeConverter // note this annotation
    fun fromStatusValue(optionValues: Status?): String? {
        if (optionValues == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Status?>() {}.type
        return gson.toJson(optionValues, type)
    }

    @TypeConverter // note this annotation
    fun toStatusValue(optionValuesString: String?): Status? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Status>() {}.type
        return gson.fromJson<Status>(optionValuesString, type)
    }
}