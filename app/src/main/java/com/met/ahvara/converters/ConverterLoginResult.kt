package com.met.ahvara.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.met.ahvara.model.LoginResponseResult
import java.lang.reflect.Type

class ConverterLoginResult {
    @TypeConverter // note this annotation
    fun fromStatusValue(optionValues: LoginResponseResult?): String? {
        if (optionValues == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<LoginResponseResult?>() {}.type
        return gson.toJson(optionValues, type)
    }

    @TypeConverter // note this annotation
    fun toStatusValue(optionValuesString: String?): LoginResponseResult? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<LoginResponseResult>() {}.type
        return gson.fromJson<LoginResponseResult>(optionValuesString, type)
    }
}