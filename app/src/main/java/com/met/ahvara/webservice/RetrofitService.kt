package com.met.ahvara.webservice

import com.met.ahvara.model.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface RetrofitService {
    @POST("login")
    fun login(@Body requestBody: RequestBody): Call<LoginResponse>
    //https://devfitser.com/Ahvara/api/v1/login

    @POST("forgot-password")
    fun forgotPassword(@Body requestBody: RequestBody): Call<ForgotPasswordResponse>
    //https://devfitser.com/Ahvara/api/v1/forgot-password

    @POST("reset-password")
    fun resetPassword(@Body requestBody: RequestBody): Call<ResetPasswordResponse>
    //https://devfitser.com/Ahvara/api/v1/reset-password

    @POST("appointment-list")
    fun appointmentList(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<AppointmentListResponse>
    //https://devfitser.com/Ahvara/api/v1/appointment-list

    @POST("submitCheckin")
    fun submitCheckin(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<SubmitCheckinResponse>
    //https://devfitser.com/Ahvara/api/v1/submitCheckin

    @POST("submitCheckout")
    fun submitCheckout(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<SubmitCheckOutResponse>
    //https://devfitser.com/Ahvara/api/v1/submitCheckout

    @POST("appointment-status-change")
    fun changeStatus(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<StatusChangeResponse>
    //https://devfitser.com/Ahvara/api/v1/appointment-status-change

    @POST("up-next-appointment")
    fun upNextAppointment(@Header("Token") token: String): Call<UpNextAppointmentResponse>
    //https://devfitser.com/Ahvara/api/v1/up-next-appointment

    @POST("appointment-progress-daily-weekly")
    fun appointmentProgressDailyWeekly(@Header("Token") token: String): Call<AppointmentProgressDailyWeeklyResponse>
    //https://devfitser.com/Ahvara/api/v1/appointment-progress-daily-weekly

    @POST("workorder_list")
    fun workOrderList(@Header("Token") token: String): Call<WorkOrderListResponse>
    //https://devfitser.com/Ahvara/api/v1/workorder_list

    @POST("task-list")
    fun taskList(@Header("Token") token: String): Call<TaskListResponse>
    //https://devfitser.com/Ahvara/api/v1/task-list

    @POST("task-search")
    fun taskSearch(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<TaskListResponse>
    //https://devfitser.com/Ahvara/api/v1/task-search

    @POST("customer_list")
    fun customerList(@Header("Token") token: String): Call<CustomerListResponse>
    //https://devfitser.com/Ahvara/api/v1/customer_list

    @POST("search_customer")
    fun searchCustomer(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<CustomerListResponse>
    //https://devfitser.com/Ahvara/api/v1/search_customer

    @POST("customer_details")
    fun customerDetails(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<CustomerDetailsResponse>
    //https://devfitser.com/Ahvara/api/v1/customer_details

    @POST("add_customer")
    fun addCustomer(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<AddCustomerResponse>
    //https://devfitser.com/Ahvara/api/v1/add_customer

    @POST("workorder_details")
    fun workorderDetails(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<WorkOrderDetailsResponse>
    //https://devfitser.com/Ahvara/api/v1/workorder_details

    @POST("appointment-reason-list")
    fun reasonsList(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<CancelRescheduleResponse>
    //https://devfitser.com/Ahvara/api/v1/appointment-reason-list

    @POST("appointment-cancel")
    fun appointmentCancel(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<AppointmentCancelResponse>
    //https://devfitser.com/Ahvara/api/v1/appointment-cancel

    @POST("appointment-cancel")
    fun appointmentReschedule(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<AppointmentRescheduleResponse>
    //https://devfitser.com/Ahvara/api/v1/appointment-cancel

    @POST("lead_list")
    fun leadsList(@Header("Token") token: String): Call<LeadsListResponse>
    //https://devfitser.com/Ahvara/api/v1/lead_list


    @POST("search_lead")
    fun searchLead(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<LeadsListResponse>
    //https://devfitser.com/Ahvara/api/v1/search_lead

    @POST("lead_details")
    fun leadDetails(
        @Header("Token") token: String,
        @Body requestBody: RequestBody
    ): Call<LeadsDetailsResponse>
    //https://devfitser.com/Ahvara/api/v1/lead_details
}