package com.met.ahvara.ui.customer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.CustomerAddBinding
import com.met.ahvara.model.AddCustomerRequest
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.notifications.NotificationsActivity
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.AddCustomerViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class CustomerAddActivity : AppCompatActivity() {
    private lateinit var binding: CustomerAddBinding
    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private val tag = "CustomerAddActivity"
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var addCustomerViewModel: AddCustomerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)

        binding = CustomerAddBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViewModel()
        initViews()
        initClickListener()
    }

    private fun initViews() {

        binding.toolbar.tvTitle.text = "Add Customer"
    }

    private fun initViewModel() {
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it

            fetchDataFromWeb()
        }

        addCustomerViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[AddCustomerViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        addCustomerViewModel.addCustomerResponse.observe(this) {
            if (it.status?.errorCode ?: 1000 == 0) {
                // setCustomerData(it.result!!.data)
                Snackbar.make(
                    mContext,
                    binding.cvSubmit,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
                onBackPressed()
            } else {
                Snackbar.make(
                    mContext,
                    binding.cvSubmit,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        addCustomerViewModel.errorMessage.observe(this) {
            Log.d(tag, "errorMessage: $it")
        }
    }

    private fun fetchDataFromWeb() {

    }

    private fun initClickListener() {
        binding.toolbar.ivMenu.setOnClickListener {
            onBackPressed()
        }

        binding.toolbar.ivNotification.setOnClickListener {
            val intent = Intent(mContext, NotificationsActivity::class.java)
            mContext.startActivity(intent)
        }

        binding.cvSubmit.setOnClickListener {
            validateData()
        }
    }

    private fun validateData() {

        if (binding.etCompanyName.text.toString() == "") {
            binding.etCompanyName.error = "Please enter company name"
        } else if (binding.etName.text.toString() == "") {
            binding.etName.error = "Please enter name"
        } else if (binding.etAddress.text.toString() == "") {
            binding.etAddress.error = "Please enter address"
        } else if (binding.etCity.text.toString() == "") {
            binding.etCity.error = "Please enter city"
        } else if (binding.etState.text.toString() == "") {
            binding.etState.error = "Please enter state"
        } else if (binding.etZipCode.text.toString() == "") {
            binding.etZipCode.error = "Please enter ZipCode"
        } else {
            if (CommonUtils.isInternetAvailable(mContext)) {
                requestAddCustomer()
            } else {
                Snackbar.make(
                    mContext,
                    binding.cvSubmit,
                    getString(R.string.internet_connection),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun requestAddCustomer() {
        val addCustomerRequest = AddCustomerRequest()
        addCustomerRequest.companyName = binding.etCompanyName.text.toString()
        addCustomerRequest.customerName = binding.etName.text.toString()
        if (binding.cbVip.isChecked) {
            addCustomerRequest.isVip = "yes"
        } else {
            addCustomerRequest.isVip = "no"
        }
        //     addCustomerRequest.contact = binding.etName.text.toString()
        //   addCustomerRequest.email = binding.etEmail.text.toString()

        addCustomerRequest.category = binding.etCategory.text.toString()
        addCustomerRequest.address = binding.etAddress.text.toString()
        addCustomerRequest.city = binding.etCity.text.toString()
        addCustomerRequest.state = binding.etState.text.toString()
        addCustomerRequest.zip = binding.etZipCode.text.toString()

        //addCustomerRequest.unit =  binding.etZipCode.text.toString()

        val requestString: String =
            CommonUtils.getJsonFromObject(addCustomerRequest, AddCustomerRequest::class.java)

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = requestString.toRequestBody("application/json".toMediaTypeOrNull())

        addCustomerViewModel.getAddCustomerResponse(loginResponse.token, requestBody)
    }
}