package com.met.ahvara.ui.documents

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.StContractDetailsBinding

class ContractDetailsActivity : AppCompatActivity() {

    private lateinit var binding: StContractDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = StContractDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}