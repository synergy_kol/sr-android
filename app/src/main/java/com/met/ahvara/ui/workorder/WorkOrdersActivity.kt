package com.met.ahvara.ui.workorder

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.adapter.AdapterWorkOrderList
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.ContentMenuHeaderBinding
import com.met.ahvara.databinding.WorkOrdersActivityBinding
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.model.WorkOrderListData
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodel.WorkOrdersViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.squareup.picasso.Picasso

class WorkOrdersActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    View.OnClickListener {
    private lateinit var binding: WorkOrdersActivityBinding

    private lateinit var adapterWorkOrderList: AdapterWorkOrderList
    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private val tag = "WorkOrdersActivity"
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var workOrdersViewModel: WorkOrdersViewModel
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        binding = WorkOrdersActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        progressDialog = CommonUtils.getProgressDialog(mContext)

        initViewModel()
        initViews()
        initClickListener()
    }

    private fun initViews() {
        binding.workOrdersContent.includeTb.tvTitle.text = getString(R.string.work_orders)
        binding.workOrdersNavigationView.itemIconTintList = null
        binding.workOrdersNavigationView.setNavigationItemSelectedListener(this)

        binding.workOrdersContent.svWorkOrder.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.isNotEmpty()) {
                    //      searchCustomer(query)
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                if (query!!.isEmpty()) {
                    fetchDataFromWeb()
                }
                return false
            }
        })

        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)

        binding.workOrdersContent.rvWorkOrders.addItemDecoration(dividerItemDecoration)
        binding.workOrdersContent.rvWorkOrders.layoutManager = linearLayoutManager
        adapterWorkOrderList = AdapterWorkOrderList(mContext, this)
        binding.workOrdersContent.rvWorkOrders.adapter = adapterWorkOrderList
    }

    private fun initClickListener() {
        binding.workOrdersContent.includeTb.ivMenu.setOnClickListener {
            if (!binding.workOrdersDrawer.isDrawerOpen(GravityCompat.START)) {
                binding.workOrdersDrawer.openDrawer(GravityCompat.START)
            }
        }

        binding.workOrdersContent.workAdd.setOnClickListener {
            val intent = Intent(this, WorkOrderAddActivity::class.java)
            startActivity(intent)
        }
        if (binding.workOrdersDrawer.isDrawerOpen(GravityCompat.START)) {
            binding.workOrdersDrawer.closeDrawer(GravityCompat.START)
        }
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this, {
            loginResponse = it
            setDataFromLogin()
            fetchDataFromWeb()
        })

        workOrdersViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[WorkOrdersViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        workOrdersViewModel.workOrderListResponse.observe(this, {
            if (it.status?.errorCode ?: 1000 == 0) {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                setWorkOrderListData(it.result!!.data)
                binding.workOrdersContent.tvNoData.visibility = View.GONE

            } else {
                Snackbar.make(
                    mContext,
                    binding.workOrdersNavigationView,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        })

        //invoked when a network exception occurred
        workOrdersViewModel.errorMessage.observe(this, {
            Log.d(tag, "errorMessage: $it")

            adapterWorkOrderList.clearList()
            //binding.workOrdersContent.tvNoData.visibility = View.VISIBLE
        })
    }

    private fun setWorkOrderListData(data: ArrayList<WorkOrderListData>) {
        adapterWorkOrderList.putData(data)

    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()
            workOrdersViewModel.getWorkOrdersListResponse(loginResponse.token)

        } else {
            Snackbar.make(
                mContext,
                binding.workOrdersContent.cv01,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        CommonUtils.navigateNavigationDrawer(mContext, item, this,
            loginViewModel,
            null)
        return true
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.ll_work_orders_parent) {
            val intent = Intent(mContext, WorkOrderDetailsActivity::class.java)
            intent.putExtra(AppConstants.INTENT_WORK_ORDER_ID, view.tag.toString())
            startActivity(intent)
        }
    }

    private fun setDataFromLogin() {
        val loginResponseData = loginResponse.result.data
        // navView is NavigationView
        val viewHeader = binding.workOrdersNavigationView.getHeaderView(0)
        // nav_header.xml is headerLayout
        val navViewHeaderBinding: ContentMenuHeaderBinding =
            ContentMenuHeaderBinding.bind(viewHeader)
        // title is Children of nav_header
        navViewHeaderBinding.profileName.text =
            getString(
                R.string.dashboard_nav_name,
                loginResponseData.firstName,
                loginResponseData.lastName
            )

        Picasso.get()
            .load(loginResponseData.profileImage)
            .placeholder(R.drawable.ic_baseline_person_24)
            .error(R.drawable.ic_baseline_person_24)
            .into(navViewHeaderBinding.menuDp)

        binding.workOrdersContent.includeTb.tvTitle.text = getString(
            R.string.work_orders
        )

        //for st sr
        val navigationMenu = binding.workOrdersNavigationView.menu
        if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_ST) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = false
        } else if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_SR) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = true
        }
        //for st sr
    }

}