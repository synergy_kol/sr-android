package com.met.ahvara.ui.leads

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.adapter.AdapterLeadsList
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.ContentMenuHeaderBinding
import com.met.ahvara.databinding.LeadsActivityBinding
import com.met.ahvara.model.CustomerListSearchRequest
import com.met.ahvara.model.LeadsListData
import com.met.ahvara.model.LeadsListSearchRequest
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.notifications.NotificationsActivity
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.LeadsListingViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.squareup.picasso.Picasso
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class LeadsListActivity : AppCompatActivity(), View.OnClickListener,
    NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: LeadsActivityBinding

    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private lateinit var adapterLeadsList: AdapterLeadsList
    private val tag = "LeadsListActivity"
    lateinit var viewModel: LeadsListingViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)

        binding = LeadsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        progressDialog = CommonUtils.getProgressDialog(mContext)

        initViewModel()
        initViews()
        initClickListener()
    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()

            viewModel.getLeadsListResponse(loginResponse.token)

        } else {
            Snackbar.make(
                mContext,
                binding.leadsDrawer,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            setDataFromLogin()
            fetchDataFromWeb()
        }

        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LeadsListingViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        viewModel.leadsListResponse.observe(this) {
            if (it.status?.errorCode ?: 1000 == 0) {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                if (it.result!!.data.size > 0) {
                    setLeadsListData(it.result!!.data)
                    binding.leadsContent.tvNoData.visibility = View.GONE
                } else {
                    binding.leadsContent.tvNoData.visibility = View.VISIBLE
                }
            } else {
                binding.leadsContent.tvNoData.visibility = View.VISIBLE
                Snackbar.make(
                    mContext,
                    binding.leadsNavigationView,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        viewModel.errorMessage.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")

            adapterLeadsList.clearList()
            binding.leadsContent.tvNoData.visibility = View.VISIBLE
        }
    }

    private fun setLeadsListData(list: ArrayList<LeadsListData>) {
        adapterLeadsList.putData(list)
    }

    private fun initClickListener() {
        binding.leadsContent.includeTb.ivMenu.setOnClickListener {
            if (!binding.leadsDrawer.isDrawerOpen(GravityCompat.START)) {
                binding.leadsDrawer.openDrawer(GravityCompat.START)
            }
        }
        /*      binding.leadsContent.customerAdd.setOnClickListener {
                  val intent = Intent(mContext, CustomerAddActivity::class.java)
                  startActivity(intent)
              }*/
        binding.leadsContent.includeTb.ivNotification.setOnClickListener {
            val intent = Intent(mContext, NotificationsActivity::class.java)
            mContext.startActivity(intent)
        }
    }

    private fun setDataFromLogin() {
        val loginResponseData = loginResponse.result.data
        // navView is NavigationView
        val viewHeader = binding.leadsNavigationView.getHeaderView(0)
        // nav_header.xml is headerLayout
        val navViewHeaderBinding: ContentMenuHeaderBinding =
            ContentMenuHeaderBinding.bind(viewHeader)
        // title is Children of nav_header
        navViewHeaderBinding.profileName.text =
            getString(
                R.string.dashboard_nav_name,
                loginResponseData.firstName,
                loginResponseData.lastName
            )

        Picasso.get()
            .load(loginResponseData.profileImage)
            .placeholder(R.drawable.ic_baseline_person_24)
            .error(R.drawable.ic_baseline_person_24)
            .into(navViewHeaderBinding.menuDp)

        binding.leadsContent.includeTb.tvTitle.text = getString(R.string.menu_leads)

        //for st sr
        val navigationMenu = binding.leadsNavigationView.menu
        if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_ST) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = false
        } else if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_SR) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = true
        }
        //for st sr
    }

    private fun initViews() {
        binding.leadsNavigationView.itemIconTintList = null
        binding.leadsNavigationView.setNavigationItemSelectedListener(this)

        binding.leadsContent.svLeads.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.isNotEmpty()) {
                    searchCustomer(query)
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                if (query!!.isEmpty()) {
                    fetchDataFromWeb()
                }
                return false
            }
        })

        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)

        binding.leadsContent.rvLeads.addItemDecoration(dividerItemDecoration)
        binding.leadsContent.rvLeads.layoutManager = linearLayoutManager
        adapterLeadsList = AdapterLeadsList(mContext, this)
        binding.leadsContent.rvLeads.adapter = adapterLeadsList
    }

    private fun searchCustomer(query: String) {
        if (CommonUtils.isInternetAvailable(mContext)) {
            val searchRequest = LeadsListSearchRequest()
            searchRequest.search_string = query
            val requestString: String =
                CommonUtils.getJsonFromObject(searchRequest, LeadsListSearchRequest::class.java)

            viewModel.getLeadsSearchListResponse(
                loginResponse.token,
                requestString.toRequestBody("application/json".toMediaTypeOrNull())
            )

        } else {
            Snackbar.make(
                mContext,
                binding.leadsDrawer,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        CommonUtils.navigateNavigationDrawer(
            mContext, item, this,
            loginViewModel,
            null
        )
        binding.leadsDrawer.closeDrawer(GravityCompat.START)

        return true
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.llTop -> {
                val intent = Intent(mContext, LeadDetailsActivity::class.java)
                intent.putExtra(AppConstants.INTENT_LEAD_ID, view.tag.toString())
                startActivity(intent)
            }
            R.id.btCancelLost -> {
                Log.e("btCancelLost", "tasks--" + Throwable().stackTrace[0].lineNumber)
            }
            R.id.btConvert -> {
                Log.e("btConvert", "tasks--" + Throwable().stackTrace[0].lineNumber)
            }
        }
    }
}