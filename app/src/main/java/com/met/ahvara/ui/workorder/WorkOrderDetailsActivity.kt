package com.met.ahvara.ui.workorder

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.WorkOrdersDetailsBinding
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.model.WorkOrderDetailsData
import com.met.ahvara.model.WorkOrderDetailsRequest
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodel.WorkOrdersViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.squareup.picasso.Picasso
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class WorkOrderDetailsActivity : AppCompatActivity() {
    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private val tag = "WorkOrderDetailsActivity"
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var binding: WorkOrdersDetailsBinding
    private lateinit var workOrdersViewModel: WorkOrdersViewModel
    private lateinit var stWorkOrderID: String

    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        binding = WorkOrdersDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        progressDialog = CommonUtils.getProgressDialog(mContext)

        stWorkOrderID = intent.getStringExtra(AppConstants.INTENT_WORK_ORDER_ID) ?: "0"

        initViewModel()
        initClickListener()
    }

    private fun initClickListener() {
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initViewModel() {
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this, {
            loginResponse = it
            fetchDataFromWeb()
        })

        workOrdersViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[WorkOrdersViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        workOrdersViewModel.workOrderDetailsResponse.observe(this) {
            if (it.status?.errorCode ?: 1000 == 0) {
                setWorkOrdersData(it.result!!.data)
                if (progressDialog.isShowing)
                    progressDialog.dismiss()

            } else {
                Snackbar.make(
                    mContext,
                    binding.holderTitle,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        workOrdersViewModel.errorMessage.observe(this) {
            Log.d(tag, "errorMessage: $it")
        }
    }

    private fun setWorkOrdersData(data: WorkOrderDetailsData) {
        binding.tvCompanyName.text = data.companyName
        binding.tvContactNumber.text = data.contact
        binding.tvEmail.text = data.email
        binding.tvAddress.text = data.address
        binding.tvDueDate.text =
            CommonUtils.getDateUsingPattern(data.expiryDate!!, "YYYY-MM-DD", "MMM dd, YYYY")
        //"1970-01-01"
        //"Nov 08, 1970"

        binding.tvDaysLeft.text = CommonUtils.getDateDifference(
            mContext,
            CommonUtils.getYYYYMMDDHHMMSSFromYYYYMMDDHHMMA(
                mContext.getString(
                    R.string.add_space,
                    data.expiryDate,
                    data.expiryTime
                )
            )
        )
        binding.tvPriority.text = data.priority
        when {
            data.priority.equals("High", ignoreCase = true) -> {
                binding.tvPriority.setTextColor(mContext.getColor(R.color.priority_high))
            }
            data.priority.equals("Low", ignoreCase = true) -> {
                binding.tvPriority.setTextColor(mContext.getColor(R.color.priority_medium))
            }
            data.priority.equals("Normal", ignoreCase = true) -> {
                binding.tvPriority.setTextColor(mContext.getColor(R.color.priority_low))
            }
        }

        binding.tvAmount.text = "$$$$$$$$$"
        binding.tvType.text = "############"
        binding.tvContactNumber.text = data.contact
        binding.tvUnit.text = "unit unit"
        binding.tvParts.text = "Parts parts"
        binding.tvDetails.text = data.description

        if (data.attachement != "")
            Picasso.get()
                .load(data.attachement)
                .placeholder(R.drawable.no_image_available)
                .error(R.drawable.no_image_available)
                .into(binding.ivAttachment)

        binding.tvCreatedBy.text = data.customerName
        binding.tvStatus.text = data.status
    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()

            val request = WorkOrderDetailsRequest()
            request.id = stWorkOrderID

            val requestString: String =
                CommonUtils.getJsonFromObject(
                    request,
                    WorkOrderDetailsRequest::class.java
                )

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody = requestString.toRequestBody("application/json".toMediaTypeOrNull())

            workOrdersViewModel.getWorkOrdersDetailsResponse(loginResponse.token, requestBody)

        } else {
            Snackbar.make(
                mContext,
                binding.holderTitle,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }
}