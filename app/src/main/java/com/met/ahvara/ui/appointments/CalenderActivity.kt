package com.met.ahvara.ui.appointments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.navigation.NavigationView
import com.met.ahvara.adapter.AdapterCustomerList
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.AppointmentCalenderActivityBinding
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.CustomerListingViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService

class CalenderActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: AppointmentCalenderActivityBinding

    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private lateinit var CalenderActivity: AdapterCustomerList
    private val tag = "CalenderActivity"
    lateinit var viewModel: CustomerListingViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)

        binding =
            com.met.ahvara.databinding.AppointmentCalenderActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.calenderNavigationView.itemIconTintList = null
        binding.calenderNavigationView.setNavigationItemSelectedListener(this)

        if (binding.calenderDrawer.isDrawerOpen(GravityCompat.START)) {
            binding.calenderDrawer.closeDrawer(GravityCompat.START)
        }

        binding.calenderContent.includeTb.ivMenu.setOnClickListener {
            if (!binding.calenderDrawer.isDrawerOpen(GravityCompat.START)) {
                binding.calenderDrawer.openDrawer(GravityCompat.START)
            }
        }

        initViewModel()

    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            //    setDataFromLogin()
            //   fetchDataFromWeb()
        }

        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[CustomerListingViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes

        //invoked when a network exception occurred
        viewModel.errorMessage.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")

            /*  adapterCustomerList.clearList()
              binding.customerContent.tvNoData.visibility = View.VISIBLE*/
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        CommonUtils.navigateNavigationDrawer(
            mContext, item, this,
            loginViewModel,
            null
        )

        return true
    }
}