package com.met.ahvara.ui.documents

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.StInvoiceDetailsBinding

class InvoiceDetailsActivity : AppCompatActivity() {

    private lateinit var binding: StInvoiceDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = StInvoiceDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}