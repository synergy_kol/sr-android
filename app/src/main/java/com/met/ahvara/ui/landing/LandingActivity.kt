package com.met.ahvara.ui.landing

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.LandingBinding
import com.met.ahvara.ui.login.SignInActivity

class LandingActivity : AppCompatActivity() {

    private lateinit var binding: LandingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = LandingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.cvNext.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }
    }
}