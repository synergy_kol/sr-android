package com.met.ahvara.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.ResetPasswordActivityBinding
import com.met.ahvara.model.ResetPasswordRequest
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.ResetPasswordViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class ResetPasswordActivity : AppCompatActivity() {
    private lateinit var binding: ResetPasswordActivityBinding

    private val tag = "ResetPasswordActivity"
    lateinit var viewModel: ResetPasswordViewModel
    lateinit var retrofitService: RetrofitService
    private lateinit var mContext: Context
    private lateinit var progressDialog: AlertDialog
    private lateinit var stEmail: String


    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        binding = ResetPasswordActivityBinding.inflate(layoutInflater)

        setContentView(binding.root)
        retrofitService = ApiClient.getInstance()
        progressDialog = CommonUtils.getProgressDialog(mContext)
        stEmail = intent.getStringExtra(AppConstants.INTENT_EMAIL_ID) ?: ""

        initClickListener()
        initViewModel()
    }

    private fun initClickListener() {
        binding.cvUpdate.setOnClickListener {
            val intent = Intent(this, PasswordUpdatedActivity::class.java)
            startActivity(intent)
        }

        binding.cvUpdate.setOnClickListener {
            validateFields()
        }
    }

    private fun validateFields() {
        if (stEmail == "") {
            onBackPressed()
        } else if (binding.etOtp.text.toString() == "") {
            binding.editOtp.error = "Please enter the OTP"
        } else if (binding.etNewPassword.text.toString() == "") {
            binding.editNewPassword.error = "Please enter New Password"
        } else if (binding.etNewPassword.text.toString().length < 8) {
            binding.editNewPassword.error = "New password must be at least 8 characters long"
        } else if (!CommonUtils.isValidPassword(binding.etNewPassword.text.toString())) {
            binding.editNewPassword.error =
                "Passwords must contain at least Eight characters, 1 uppercase letter, 1 lowercase letter and 1 special character."
        } else if (binding.etConfirmPassword.text.toString() == "") {
            binding.editConfirmPassword.error = "Please enter Confirm Password"
        } else if (binding.etNewPassword.text.toString() != binding.etConfirmPassword.text.toString()) {
            binding.editConfirmPassword.error = "Passwords do not match"
        } else {
            if (CommonUtils.isInternetAvailable(mContext)) {
                requestResetPassword()
            } else {
                Snackbar.make(
                    mContext,
                    binding.cvUpdate,
                    getString(R.string.internet_connection),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun requestResetPassword() {
        val request = ResetPasswordRequest()
        request.prefix = getString(R.string.request_prefix)
        request.email = stEmail
        request.resetPasswordOtp = binding.etOtp.text.toString()
        request.password = binding.etNewPassword.text.toString()

        val requestString: String =
            CommonUtils.getJsonFromObject(request, ResetPasswordRequest::class.java)

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody =
            requestString.toRequestBody("application/json".toMediaTypeOrNull())

        viewModel.getResetPasswordResponse(requestBody)
    }

    private fun initViewModel() {
        //get viewmodel instance using ViewModelProvider.Factory
        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[ResetPasswordViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        viewModel.resetPasswordResponse.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            if (it.status?.errorCode ?: 1 == 0) {
                val intent = Intent(mContext, SignInActivity::class.java)
                startActivity(intent)
                finishAffinity()

            } else {
                Snackbar.make(
                    mContext,
                    binding.cvUpdate,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        viewModel.errorMessage.observe(this) {
            Log.d(tag, "errorMessage: $it")
        }
    }
}