package com.met.ahvara.ui.map

import android.Manifest

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.util.TypedValue
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ActivityCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.libraries.places.api.Places
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.RoutePlannerMapViewBinding
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.model.MapData
import com.met.ahvara.model.UpNextAppointmentListData
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.notifications.NotificationsActivity
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.CustomerListingViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.vmadalin.easypermissions.EasyPermissions
import com.vmadalin.easypermissions.dialogs.DEFAULT_SETTINGS_REQ_CODE
import com.vmadalin.easypermissions.dialogs.SettingsDialog
import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext
import kotlin.math.roundToInt
import kotlin.properties.Delegates

class RoutePlannerActivity : AppCompatActivity(), OnMapReadyCallback, CoroutineScope,
    EasyPermissions.PermissionCallbacks,
    EasyPermissions.RationaleCallbacks {
    private lateinit var binding: RoutePlannerMapViewBinding

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private val tag = "RoutePlannerActivity"
    lateinit var viewModel: CustomerListingViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog

    //location
    private var locationFlag: Boolean = false

    private var latitude by Delegates.notNull<Double>()
    private var longitude by Delegates.notNull<Double>()

    // FusedLocationProviderClient - Main class for receiving location updates.
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    // LocationRequest - Requirements for the location updates, i.e.,
    // how often you should receive updates, the priority, etc.
    private lateinit var locationRequest: LocationRequest

    // LocationCallback - Called when FusedLocationProviderClient
    // has a new Location
    private lateinit var locationCallback: LocationCallback

    // This will store current location info
    private lateinit var currentLocation: Location
    private val RC_LOCATION_PERM = 124
    private lateinit var googleMap: GoogleMap

    //location
    private val INITIAL_STROKE_WIDTH_PX = 5


    // Coordinates of a park nearby
    private var destinationLatitude by Delegates.notNull<Double>()
    private var destinationLongitude by Delegates.notNull<Double>()

    private lateinit var upNextAppointmentData: UpNextAppointmentListData

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        binding = RoutePlannerMapViewBinding.inflate(layoutInflater)
        try {
            setContentView(binding.root)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            upNextAppointmentData =
                intent.getParcelableExtra(AppConstants.INTENT_UP_NEXT_APPOINTMENT)!!
        } catch (e: Exception) {
            Snackbar.make(binding.fabZoomOut, getString(R.string.api_call_error), LENGTH_SHORT)
                .show()
            onBackPressed()
        }
        destinationLatitude = upNextAppointmentData.siteLatitude?.toDouble() ?: 00.00
        destinationLongitude = upNextAppointmentData.siteLongitude?.toDouble() ?: 00.00

        progressDialog = CommonUtils.getProgressDialog(mContext)
        job = Job() // create the Job
        val supportMapFragment =
            supportFragmentManager.findFragmentById(R.id.map)
                    as SupportMapFragment

        supportMapFragment.getMapAsync(this)

        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, getString(R.string.maps_api_key))
        }

        locationTask()
        initViewModel()
        initViews()
        initClickListener()
    }

    private fun initClickListener() {
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
        binding.ivNotification.setOnClickListener {
            val intent = Intent(mContext, NotificationsActivity::class.java)
            startActivity(intent)
        }
        binding.fabZoomIn.setOnClickListener {
            googleMap.animateCamera(CameraUpdateFactory.zoomIn())
        }
        binding.fabZoomOut.setOnClickListener {
            googleMap.animateCamera(CameraUpdateFactory.zoomOut())
        }
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            setDataFromLogin()
            fetchDataFromWeb()
        }

        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[CustomerListingViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes

        //invoked when a network exception occurred
        viewModel.errorMessage.observe(this, Observer {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")
        })
    }

    private fun setDataFromLogin() {

    }

    private fun fetchDataFromWeb() {

    }

    private fun initViews() {

    }

    private fun locationTask() {
        // Have permission, do the thing!
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create().apply {
            // Sets the desired interval for
            // active location updates.
            // This interval is inexact.
            interval = TimeUnit.SECONDS.toMillis(3)

            // Sets the fastest rate for active location updates.
            // This interval is exact, and your application will never
            // receive updates more frequently than this value
            fastestInterval = TimeUnit.SECONDS.toMillis(5)

            // Sets the maximum time when batched location
            // updates are delivered. Updates may be
            // delivered sooner than this interval
            maxWaitTime = TimeUnit.SECONDS.toMillis(10)

            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                locationResult.lastLocation.let {
                    locationFlag = true
                    currentLocation = it
                    latitude = currentLocation.latitude
                    longitude = currentLocation.longitude

                    updateMap()
                    // use latitude and longitude as per your need
                }
            }
        }

        if (hasLocationPermissions()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.rationale_location),
                    RC_LOCATION_PERM,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                return
            }
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()!!
            )

            fusedLocationProviderClient.lastLocation.addOnSuccessListener { location: Location? ->
                // Got last known location. In some rare situations this can be null.
                locationFlag = true
                currentLocation = location!!
                latitude = currentLocation.latitude
                longitude = currentLocation.longitude

                setUpMap()
            }
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_location),
                RC_LOCATION_PERM,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    private fun updateMap() {
        val latLng = LatLng(currentLocation.latitude, currentLocation.longitude)
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))

        val urll = getDirectionURL(
            LatLng(latitude, longitude),
            LatLng(destinationLatitude, destinationLongitude),
            getString(R.string.maps_api_key)
        )

        launch(Dispatchers.Main) {
            fetchPolyLineAndShow(urll)
        }
    }

    private fun setUpMap() {
        val dip = 25f
        val r: Resources = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dip,
            r.displayMetrics
        ).roundToInt()

        val latLng = LatLng(currentLocation.latitude, currentLocation.longitude)
        val markerOptions =
            MarkerOptions()
                .icon(AppCompatResources.getDrawable(mContext, R.drawable.ic_arrow_up)?.let {
                    BitmapDescriptorFactory.fromBitmap(
                        it.toBitmap(px, px)
                    )
                })
                .position(latLng)
                .title("I am here!")
        // googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f))
        googleMap.addMarker(markerOptions)

/*
        with(googleMap) {
            // Override the default content description on the view, for accessibility mode.
            //setContentDescription(getString(R.string.polyline_demo_description))

            // A geodesic polyline that goes around the world.
            addPolyline(PolylineOptions().apply {
                add(LatLng(latitude,longitude), LatLng(destinationLatitude,destinationLongitude))
                width(INITIAL_STROKE_WIDTH_PX.toFloat())
                color(Color.BLUE)
                geodesic(true)
//                clickable(clickabilityCheckbox.isChecked)
                clickable(true)

            })

            // Move the googleMap so that it is centered on the mutable polyline.
            moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude,longitude), 18f))

            // Add a listener for polyline clicks that changes the clicked polyline's color.
            setOnPolylineClickListener { polyline ->
                // Flip the values of the red, green and blue components of the polyline's color.
                polyline.color = polyline.color xor 0x00ffffff
            }
        }
*/

        val urll = getDirectionURL(
            LatLng(latitude, longitude),
            LatLng(destinationLatitude, destinationLongitude),
            getString(R.string.maps_api_key)
        )

        launch(Dispatchers.Main) {
            fetchPolyLineAndShow(urll)
        }

        /* googleMap.animateCamera(
             CameraUpdateFactory.newLatLngZoom(
                 LatLng(
                     latitude,
                     longitude
                 ), 14F
             )
         )*/
    }

    private fun hasLocationPermissions(): Boolean {
        return EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        Log.d(tag, "onPermissionsGranted:" + requestCode + ":" + perms.size)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest, locationCallback,
            Looper.myLooper()!!
        )
    }

    override fun onRationaleAccepted(requestCode: Int) {
        Log.d(tag, "onRationaleAccepted:$requestCode")
    }

    override fun onRationaleDenied(requestCode: Int) {
        Log.d(tag, "onRationaleDenied:$requestCode")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Log.d(tag, "onPermissionsDenied:" + requestCode + ":" + perms.size)

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            SettingsDialog.Builder(this).build().show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DEFAULT_SETTINGS_REQ_CODE) {
            val yes = getString(R.string.yes)
            val no = getString(R.string.no)
            // Do something after user returned from app settings screen, like showing a Toast.
            Toast.makeText(
                this,
                getString(
                    R.string.returned_from_app_settings_to_activity,
                    if (hasLocationPermissions()) yes else no,
                ),
                Toast.LENGTH_LONG
            )
                .show()
        }
    }

    override fun onDestroy() {
        job.cancel() // cancel the Job
        super.onDestroy()

        val removeTask = fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        removeTask.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.d(tag, "Location Callback removed.")
            } else {
                Log.d(tag, "Failed to remove Location Callback.")
            }
        }
    }

    override fun onMapReady(p0: GoogleMap) {
        googleMap = p0

        // Doing Nothing
    }

    private fun getDirectionURL(origin: LatLng, dest: LatLng, secret: String): String {
        return "https://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude},${origin.longitude}" +
                "&destination=${dest.latitude},${dest.longitude}" +
                "&sensor=false" +
                "&mode=driving" +
                "&key=$secret"
    }

    private fun decodePolyline(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val latLng = LatLng((lat.toDouble() / 1E5), (lng.toDouble() / 1E5))
            poly.add(latLng)
        }
        return poly
    }


    private suspend fun fetchPolyLineAndShow(url: String) {
        val polyline = withContext(Dispatchers.IO) { fetchPolyline(url) }// fetch on IO thread
        showPolyline(polyline) // back on UI thread
    }

    private fun fetchPolyline(url: String): List<List<LatLng>> {

        val client = OkHttpClient()
        val request = Request.Builder().url(url).build()
        val response = client.newCall(request).execute()
        val data = response.body!!.string()

        val result = ArrayList<List<LatLng>>()
        try {
            val respObj = Gson().fromJson(data, MapData::class.java)
            val path = ArrayList<LatLng>()
            for (i in 0 until respObj.routes[0].legs[0].steps.size) {
                path.addAll(decodePolyline(respObj.routes[0].legs[0].steps[i].polyline.points))
            }
            result.add(path)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return result
    }

    private fun showPolyline(result: List<List<LatLng>>) {
        val lineoption = PolylineOptions()
        for (i in result.indices) {
            lineoption.addAll(result[i])
            lineoption.width(10f)
            lineoption.color(Color.GREEN)
            lineoption.geodesic(true)
        }
        googleMap.addPolyline(lineoption)
    }

}