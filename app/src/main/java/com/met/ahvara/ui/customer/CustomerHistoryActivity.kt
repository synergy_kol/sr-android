package com.met.ahvara.ui.customer

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.CustomerHistoryBinding

class CustomerHistoryActivity : AppCompatActivity() {

    private lateinit var binding: CustomerHistoryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = CustomerHistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvCallHistory.setOnClickListener {

            val intent = Intent(this, CustomerCallHistoryActivity::class.java)
            startActivity(intent)
        }
    }
}