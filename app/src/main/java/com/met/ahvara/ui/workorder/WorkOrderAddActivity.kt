package com.met.ahvara.ui.workorder

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.WorkOrdersAddBinding

class WorkOrderAddActivity : AppCompatActivity() {

    private lateinit var binding: WorkOrdersAddBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = WorkOrdersAddBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}