package com.met.ahvara.ui.notifications

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.adapter.AdapterNotificationsList
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.ContentMenuHeaderBinding
import com.met.ahvara.databinding.NotificationsActivityBinding
import com.met.ahvara.model.CustomerListData
import com.met.ahvara.model.CustomerListSearchRequest
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.customer.CustomerDetailsActivity
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.CustomerListingViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.squareup.picasso.Picasso
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class NotificationsActivity : AppCompatActivity(), View.OnClickListener,
    NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: NotificationsActivityBinding

    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private lateinit var adapterNotificationsList: AdapterNotificationsList
    private val tag = "NotificationsActivity"
    lateinit var viewModel: CustomerListingViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)

        binding = NotificationsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        progressDialog = CommonUtils.getProgressDialog(mContext)

        initViewModel()
        initViews()
        initClickListener()
    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()

            viewModel.getCustomerListResponse(loginResponse.token)

        } else {
            Snackbar.make(
                mContext,
                binding.notificationsDrawer,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            setDataFromLogin()
            fetchDataFromWeb()
        }

        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[CustomerListingViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        viewModel.customerListResponse.observe(this) {
            if (it.status?.errorCode ?: 1000 == 0) {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                if (it.result!!.data.size > 0) {
                    setListData(it.result!!.data)
                    binding.notificationsContent.tvNoData.visibility = View.GONE
                } else {
                    binding.notificationsContent.tvNoData.visibility = View.VISIBLE
                }
            } else {
                binding.notificationsContent.tvNoData.visibility = View.VISIBLE
                Snackbar.make(
                    mContext,
                    binding.notificationsDrawer,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        viewModel.errorMessage.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")

            adapterNotificationsList.clearList()
            binding.notificationsContent.tvNoData.visibility = View.VISIBLE
        }
    }

    private fun setListData(list: ArrayList<CustomerListData>) {
   //     adapterNotificationsList.putData(list)
    }

    private fun initClickListener() {

        binding.notificationsContent.includeTb.ivNotification.setOnClickListener {
            val intent = Intent(mContext, NotificationsActivity::class.java)
            mContext.startActivity(intent)
        }
    }

    private fun setDataFromLogin() {
        val loginResponseData = loginResponse.result.data
        // navView is NavigationView
        val viewHeader = binding.notificationsNavigationView.getHeaderView(0)
        // nav_header.xml is headerLayout
        val navViewHeaderBinding: ContentMenuHeaderBinding =
            ContentMenuHeaderBinding.bind(viewHeader)
        // title is Children of nav_header
        navViewHeaderBinding.profileName.text =
            getString(
                R.string.dashboard_nav_name,
                loginResponseData.firstName,
                loginResponseData.lastName
            )

        Picasso.get()
            .load(loginResponseData.profileImage)
            .placeholder(R.drawable.ic_baseline_person_24)
            .error(R.drawable.ic_baseline_person_24)
            .into(navViewHeaderBinding.menuDp)

        binding.notificationsContent.includeTb.tvTitle.text = getString(R.string.notifications)

        //for st sr
        val navigationMenu = binding.notificationsNavigationView.menu
        if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_ST) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = false
        } else if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_SR) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = true
        }
        //for st sr
    }

    private fun initViews() {
        binding.notificationsNavigationView.itemIconTintList = null
        binding.notificationsNavigationView.setNavigationItemSelectedListener(this)

/*
        binding.notificationsContent.svCustomer.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.isNotEmpty()) {
                    searchCustomer(query)
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                if (query!!.isEmpty()) {
                    fetchDataFromWeb()
                }
                return false
            }
        })
*/

        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)

        binding.notificationsContent.rvNotifications.addItemDecoration(dividerItemDecoration)
        binding.notificationsContent.rvNotifications.layoutManager = linearLayoutManager
        adapterNotificationsList = AdapterNotificationsList(mContext, this)
        binding.notificationsContent.rvNotifications.adapter = adapterNotificationsList
    }

    private fun searchNotifications(query: String) {
        if (CommonUtils.isInternetAvailable(mContext)) {
            val searchRequest = CustomerListSearchRequest()
            searchRequest.search_string = query
            val requestString: String =
                CommonUtils.getJsonFromObject(searchRequest, CustomerListSearchRequest::class.java)

            viewModel.getCustomerSearchListResponse(
                loginResponse.token,
                requestString.toRequestBody("application/json".toMediaTypeOrNull())
            )

        } else {
            Snackbar.make(
                mContext,
                binding.notificationsDrawer,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        CommonUtils.navigateNavigationDrawer(
            mContext, item, this,
            loginViewModel,
            null
        )
        binding.notificationsDrawer.closeDrawer(GravityCompat.START)

        return true
    }

    override fun onClick(view: View?) {
        if (view!!.id == R.id.ll_demo) {
            val intent = Intent(mContext, CustomerDetailsActivity::class.java)
            intent.putExtra(AppConstants.INTENT_CUSTOMER_ID, view.tag.toString())
            startActivity(intent)
        }
    }
}