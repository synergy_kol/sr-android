package com.met.ahvara.ui.task

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.TaskAddBinding

class TaskAddActivity : AppCompatActivity() {

    private lateinit var binding: TaskAddBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = TaskAddBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}