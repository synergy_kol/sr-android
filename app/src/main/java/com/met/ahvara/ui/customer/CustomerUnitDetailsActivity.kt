package com.met.ahvara.ui.customer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.CustomerUnitDetailsBinding

class CustomerUnitDetailsActivity : AppCompatActivity() {

    private lateinit var binding: CustomerUnitDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = CustomerUnitDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}