package com.met.ahvara.ui.account

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.MyAccountBinding

class AccountActivity : AppCompatActivity() {

    private lateinit var binding: MyAccountBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = MyAccountBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}