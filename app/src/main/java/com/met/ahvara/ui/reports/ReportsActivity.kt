package com.met.ahvara.ui.reports

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.ReportsActivityBinding

class ReportsActivity : AppCompatActivity() {

    private lateinit var binding: ReportsActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ReportsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}