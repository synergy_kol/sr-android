package com.met.ahvara.ui.units

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.UnitsActivityBinding

class UnitsActivity : AppCompatActivity() {

    private lateinit var binding: UnitsActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = UnitsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.unitsContent.cv01.setOnClickListener {

            val intent = Intent(this, UnitDetailsActivity::class.java)
            startActivity(intent)
        }
    }
}