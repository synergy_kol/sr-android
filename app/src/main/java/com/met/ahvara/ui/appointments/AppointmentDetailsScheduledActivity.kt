package com.met.ahvara.ui.appointments

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ActivityCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.AppointmentDetailsScheduledBinding
import com.met.ahvara.model.AppointmentListListdata
import com.met.ahvara.model.AppointmentListRequest
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.workorder.WorkOrderDetailsActivity
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.DashboardViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.vmadalin.easypermissions.EasyPermissions
import com.vmadalin.easypermissions.dialogs.DEFAULT_SETTINGS_REQ_CODE
import com.vmadalin.easypermissions.dialogs.SettingsDialog
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt
import kotlin.properties.Delegates

class AppointmentDetailsScheduledActivity : AppCompatActivity(), View.OnClickListener,
    OnMapReadyCallback, EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks {
    private lateinit var binding: AppointmentDetailsScheduledBinding

    private lateinit var stCurrentDate: String
    private lateinit var appointmentListListdata: AppointmentListListdata

    private lateinit var popupStatus: AlertDialog
    private lateinit var popupNote: AlertDialog
    private lateinit var popupRate: AlertDialog

    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse

    private val tag = "AppointmentDetailsScheduledActivity"
    lateinit var dashboardViewModel: DashboardViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog

    //location
    private var locationFlag: Boolean = false

    private var latitude by Delegates.notNull<Double>()
    private var longitude by Delegates.notNull<Double>()

    // FusedLocationProviderClient - Main class for receiving location updates.
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    // LocationRequest - Requirements for the location updates, i.e.,
    // how often you should receive updates, the priority, etc.
    private lateinit var locationRequest: LocationRequest

    // LocationCallback - Called when FusedLocationProviderClient
    // has a new Location
    private lateinit var locationCallback: LocationCallback

    // This will store current location info
    private lateinit var currentLocation: Location
    private val RC_LOCATION_PERM = 124
    private lateinit var googleMap: GoogleMap
    //location

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        binding = AppointmentDetailsScheduledBinding.inflate(layoutInflater)
        setContentView(binding.root)

        appointmentListListdata =
            intent.getParcelableExtra(AppConstants.INTENT_APPOINTMENT_DETAILS)!!
        val date = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        stCurrentDate = date.format(formatter)

        progressDialog = CommonUtils.getProgressDialog(mContext)
        val supportMapFragment =
            supportFragmentManager.findFragmentById(R.id.map)
                    as SupportMapFragment

        supportMapFragment.getMapAsync(this)


        /* binding.fabMap.setOnClickListener {
             val intent = Intent(this, RoutePlannerActivity::class.java)
             startActivity(intent)
         }

         binding.cvStatus.setOnClickListener {
             showStatus()
         }

         binding.cvNote.setOnClickListener {
             showAddNote()
         }

         binding.cvRate.setOnClickListener {
             showRate()
         }

         val supportMapFragment =
             supportFragmentManager.findFragmentById(R.id.map)
                     as SupportMapFragment

         supportMapFragment.getMapAsync(this)*/
        setDataFromIntent()
        initViewModel()
        locationTask()
        initViews()
        initClickListener()
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            setDataFromLogin()
            fetchDataFromWeb()
        }

        dashboardViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[DashboardViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        dashboardViewModel.observerAppointmentListResponse.observe(this) {
            if (it.status?.errorCode ?: 1000 == 0) {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                if (it.result?.data!!.listdata.size > 0) {
                }

            } else {
                Snackbar.make(
                    mContext,
                    binding.content.cvWorkOrdersViewAll,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        dashboardViewModel.observerErrorMessage.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")

        }
    }

    private fun initViews() {
        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)

        /* binding.appointmentsContent.rvAppointments.addItemDecoration(dividerItemDecoration)
         binding.appointmentsContent.rvAppointments.layoutManager = linearLayoutManager
         adapterAppointmentList = AdapterAppointments(mContext, this)
         binding.appointmentsContent.rvAppointments.adapter = adapterAppointmentList*/
    }

    private fun searchAppointments(query: String) {
        /* if (CommonUtils.isInternetAvailable(mContext)) {
             val searchRequest = CustomerListSearchRequest()
             searchRequest.search_string = query
             val requestString: String =
                 CommonUtils.getJsonFromObject(searchRequest, CustomerListSearchRequest::class.java)

             dashboardViewModel.getCustomerSearchListResponse(
                 loginResponse.token,
                 requestString.toRequestBody("application/json".toMediaTypeOrNull())
             )

         } else {
             Snackbar.make(
                 mContext,
                 binding.appointmentsDrawer,
                 getString(R.string.internet_connection),
                 BaseTransientBottomBar.LENGTH_SHORT
             ).show()
         }*/
    }

    private fun initClickListener() {
        binding.ivMenu.setOnClickListener {
            onBackPressed()
        }
        binding.content.fabCall.setOnClickListener {
            Log.e(tag, "call--" + Throwable().stackTrace[0].lineNumber)
        }

        binding.content.fabMap.setOnClickListener {
            Log.e(tag, "map--" + Throwable().stackTrace[0].lineNumber)
        }

        binding.content.btNotesViewAll.setOnClickListener {
            Log.e(tag, "btNotesViewAll--" + Throwable().stackTrace[0].lineNumber)
        }
        binding.content.btLeadsViewAll.setOnClickListener {
            Log.e(tag, "btLeadsViewAll--" + Throwable().stackTrace[0].lineNumber)
        }
    }

    private fun showStatus() {

        val inflater = layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_appointment_status, null)

        val cancel = view.findViewById<RelativeLayout>(R.id.status_cancel)
        val proceed = view.findViewById<RelativeLayout>(R.id.status_proceed)

        val builder = AlertDialog.Builder(this, R.style.popupDialog)
        builder.setCancelable(false)
        builder.setView(view)

        cancel.setOnClickListener {
            if (popupStatus.isShowing) {
                popupStatus.dismiss()
            }
        }

        proceed.setOnClickListener {
            if (popupStatus.isShowing) {
                popupStatus.dismiss()
            }
        }

        popupStatus = builder.create()
        popupStatus.show()
    }

    private fun showAddNote() {

        val inflater = layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_add_note, null)

        val cancel = view.findViewById<RelativeLayout>(R.id.note_cancel)
        val proceed = view.findViewById<RelativeLayout>(R.id.note_proceed)

        val builder = AlertDialog.Builder(this, R.style.popupDialog)
        builder.setCancelable(false)
        builder.setView(view)

        cancel.setOnClickListener {
            if (popupNote.isShowing) {
                popupNote.dismiss()
            }
        }

        proceed.setOnClickListener {
            if (popupNote.isShowing) {
                popupNote.dismiss()
            }
        }

        popupNote = builder.create()
        popupNote.show()
    }

    private fun showRate() {

        val inflater = layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_rate, null)

        val cancel = view.findViewById<RelativeLayout>(R.id.rate_cancel)
        val proceed = view.findViewById<RelativeLayout>(R.id.rate_proceed)

        val builder = AlertDialog.Builder(this, R.style.popupDialog)
        builder.setCancelable(false)
        builder.setView(view)

        cancel.setOnClickListener {
            if (popupRate.isShowing) {
                popupRate.dismiss()
            }
        }

        proceed.setOnClickListener {
            if (popupRate.isShowing) {
                popupRate.dismiss()
            }
        }

        popupRate = builder.create()
        popupRate.show()
    }

    private fun setDataFromLogin() {
        val loginResponseData = loginResponse.result.data
        // navView is NavigationView

        //for st sr
        if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_ST) {
            //  navigationMenu.findItem(R.id.drawer_leads).isVisible = false
        } else if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_SR) {
            // navigationMenu.findItem(R.id.drawer_leads).isVisible = true

        }
        //for st sr
    }
    private fun setDataFromIntent() {

        binding.content.tvId.text = getString(R.string.hash_tag, appointmentListListdata.recordId)
        binding.content.tvVisitType.text = appointmentListListdata.type
        binding.content.tvCustomerName.text = appointmentListListdata.companyName
        binding.content.tvCustomerPhoneNumber.text = appointmentListListdata.companyName
        binding.content.tvAddress.text = appointmentListListdata.address

        binding.content.tvDate.text =
            CommonUtils.getMonthDayYearFromYYYYMMDDHHmmss(appointmentListListdata.endDateTime)
        binding.content.tvDay.text = CommonUtils.getDateUsingPattern(
            appointmentListListdata.endDateTime!!,
            "yyyy-MM-dd HH:mm:ss",
            "dddd"
        )

        binding.content.tvTime.text = CommonUtils.getDateUsingPattern(
            appointmentListListdata.endDateTime!!,
            "yyyy-MM-dd HH:mm:ss",
            "HH:mm a"
        )

        Log.e("dateeee", appointmentListListdata.endDateTime!!)
        binding.content.tvTimeRemaining.text = CommonUtils.getDateDifference(
            mContext,
            appointmentListListdata.endDateTime
        )

        binding.content.tvNotesText.text = "appointmentListListdata"
        binding.content.tvNotesLastUpdatedOn.text = "appointmentListListdata"

        binding.content.tvOwnerRating.text = appointmentListListdata.customerRating
        //binding.content.ivOwnerRatingUpDown.setImageResource()

        binding.content.tvManagerRating.text = appointmentListListdata.customerSiteRating
        //binding.content.ivManagerRatingUpDown.setImageResource()

        binding.content.tvRatingLastUpdate.text = CommonUtils.getDateUsingPattern(
            appointmentListListdata.updatedAt!!,
            "yyyy-MM-dd HH:mm:ss",
            "MMM dd, yyyy"
        )

        //binding.content.rvLeads.text = appointmentListListdata.customerSiteRating

        binding.content.tvLeadsOpen.text = appointmentListListdata.customerSiteRating

        //binding.content.rvWorkOrders
        binding.content.tvWorkOrdersText.visibility = View.VISIBLE

        binding.content.tvCreatedBy.text = appointmentListListdata.reasonId
        binding.content.tvCreatedByLocation.text = appointmentListListdata.reasonId
    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()

            val request = AppointmentListRequest()

            request.date = stCurrentDate
            request.filter_by = AppConstants.APPOINTMENT_FILTER_BY_DATE

            val requestString: String =
                CommonUtils.getJsonFromObject(request, AppointmentListRequest::class.java)

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody = requestString.toRequestBody("application/json".toMediaTypeOrNull())

            dashboardViewModel.getAppointmentListResponse(loginResponse.token, requestBody)

        } else {
            Snackbar.make(
                mContext,
                binding.content.cvWorkOrdersViewAll,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.ll_work_orders_parent) {
            val intent = Intent(mContext, WorkOrderDetailsActivity::class.java)
            intent.putExtra(AppConstants.INTENT_APPOINTMENT_ID, view.tag.toString())
            startActivity(intent)
        }
    }

    private fun locationTask() {

        // Have permission, do the thing!
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create().apply {
            // Sets the desired interval for
            // active location updates.
            // This interval is inexact.
            interval = TimeUnit.SECONDS.toMillis(3)

            // Sets the fastest rate for active location updates.
            // This interval is exact, and your application will never
            // receive updates more frequently than this value
            fastestInterval = TimeUnit.SECONDS.toMillis(5)

            // Sets the maximum time when batched location
            // updates are delivered. Updates may be
            // delivered sooner than this interval
            maxWaitTime = TimeUnit.SECONDS.toMillis(10)

            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                locationResult.lastLocation.let {
                    locationFlag = true
                    currentLocation = it
                    latitude = currentLocation.latitude
                    longitude = currentLocation.longitude

                    setUpMap()
                    // use latitude and longitude as per your need
                }
            }
        }

        if (hasLocationPermissions()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.rationale_location),
                    RC_LOCATION_PERM,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                return
            }
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()!!
            )
            fusedLocationProviderClient.lastLocation.addOnSuccessListener { location: Location? ->
                // Got last known location. In some rare situations this can be null.

                locationFlag = true
                currentLocation = location!!
                latitude = currentLocation.latitude
                longitude = currentLocation.longitude
                /*Log.e(
                    "location",
                    "new processlastLocation" + location?.latitude + "---" + location?.longitude
                )*/
                setUpMap()
            }
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_location),
                RC_LOCATION_PERM,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    private fun setUpMap() {
        val dip = 25f
        val r: Resources = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dip,
            r.displayMetrics
        ).roundToInt()

        val latLng = LatLng(currentLocation.latitude, currentLocation.longitude)
        val markerOptions =
            MarkerOptions()
                .icon(AppCompatResources.getDrawable(mContext, R.drawable.ic_arrow_up)?.let {
                    BitmapDescriptorFactory.fromBitmap(
                        it.toBitmap(px, px)
                    )
                })
                .position(latLng)
                .title("I am here!")
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))

        googleMap.addMarker(markerOptions)
    }

    private fun hasLocationPermissions(): Boolean {
        return EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        Log.d(tag, "onPermissionsGranted:" + requestCode + ":" + perms.size)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest, locationCallback,
            Looper.myLooper()!!
        )

    }

    override fun onRationaleAccepted(requestCode: Int) {
        Log.d(tag, "onRationaleAccepted:$requestCode")
    }

    override fun onRationaleDenied(requestCode: Int) {
        Log.d(tag, "onRationaleDenied:$requestCode")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Log.d(tag, "onPermissionsDenied:" + requestCode + ":" + perms.size)

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            SettingsDialog.Builder(this).build().show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DEFAULT_SETTINGS_REQ_CODE) {
            val yes = getString(R.string.yes)
            val no = getString(R.string.no)
            // Do something after user returned from app settings screen, like showing a Toast.
            Toast.makeText(
                this,
                getString(
                    R.string.returned_from_app_settings_to_activity,
                    if (hasLocationPermissions()) yes else no,
                ),
                Toast.LENGTH_LONG
            )
                .show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        val removeTask = fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        removeTask.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.d(tag, "Location Callback removed.")
            } else {
                Log.d(tag, "Failed to remove Location Callback.")
            }
        }
    }

    override fun onMapReady(p0: GoogleMap) {
        googleMap = p0
        //googleMap.uiSettings.isZoomControlsEnabled = true

        //  googleMap.animateCamera()
        // Doing Nothing
    }
}