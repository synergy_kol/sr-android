package com.met.ahvara.ui.appointments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.CompoundButton
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.adapter.AdapterAppointmentsCancellations
import com.met.ahvara.adapter.AdapterSpinnerCancelRescheduleReasons
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.RescheduleActivityBinding
import com.met.ahvara.datetimepicker.view.datePicker.DatePicker
import com.met.ahvara.model.AppointmentListResponse
import com.met.ahvara.model.CancelRescheduleData
import com.met.ahvara.model.CancelRescheduleRequest
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.AppointmentReasonsViewModel
import com.met.ahvara.viewmodel.AppointmentRescheduleViewModel
import com.met.ahvara.viewmodel.DashboardViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody


class RescheduleActivity : AppCompatActivity(), View.OnClickListener,
    CompoundButton.OnCheckedChangeListener {
    private lateinit var binding: RescheduleActivityBinding

    private lateinit var adapterAppointmentsCancellations: AdapterAppointmentsCancellations
    private lateinit var appointmentListResponse: AppointmentListResponse
    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private lateinit var cancelRescheduleData: CancelRescheduleData
    private var selectedReason: String = ""

    private val tag = "RescheduleActivity"
    lateinit var dashboardViewModel: DashboardViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog
    lateinit var appointmentReasonsViewModel: AppointmentReasonsViewModel
    lateinit var appointmentRescheduleViewModel: AppointmentRescheduleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)

        binding = RescheduleActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        progressDialog = CommonUtils.getProgressDialog(mContext)

        appointmentListResponse =
            intent.getParcelableExtra<AppointmentListResponse>(AppConstants.INTENT_APPOINTMENT_LIST) as AppointmentListResponse


        initViewModel()
        initViews()
        initClickListener()

        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initClickListener() {
        binding.datepicker.setDataSelectListener { date, day, month, year ->

        }
        binding.timepicker.setTimeSelectListener { hour, minute ->


        }
        binding.spReasons.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedReason = cancelRescheduleData.reasonlist[position].id.toString()
            }
        }

        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
        binding.rlCancel.setOnClickListener {
            onBackPressed()
        }
        binding.rlConfirm.setOnClickListener {
            // onBackPressed()
        }
    }

    private fun initViews() {
        binding.datepicker.setPickerMode(DatePicker.MONTH_ON_FIRST)

        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)
        binding.rvAppointments.addItemDecoration(dividerItemDecoration)
        binding.rvAppointments.layoutManager = linearLayoutManager
        adapterAppointmentsCancellations = AdapterAppointmentsCancellations(mContext, this, this)
        binding.rvAppointments.adapter = adapterAppointmentsCancellations

        adapterAppointmentsCancellations.putData(appointmentListResponse.result?.data!!.listdata)
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            // setDataFromLogin()
            fetchDataFromWeb()
        }

        appointmentReasonsViewModel = ViewModelProvider(
            this, MyViewModelFactory(
                MainRepository(retrofitService, application as AhvaraApplication),
                application as AhvaraApplication
            )
        )[AppointmentReasonsViewModel::class.java]

        appointmentReasonsViewModel.reasonsResponse.observe(this) {

            if (progressDialog.isShowing)
                progressDialog.dismiss()
            cancelRescheduleData = it.result?.data!!
            val customAdapter =
                AdapterSpinnerCancelRescheduleReasons(
                    mContext,
                    cancelRescheduleData.reasonlist
                )

            binding.spReasons.adapter = customAdapter
        }
    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()
            val request = CancelRescheduleRequest()
            request.type = AppConstants.REASON_CANCEL

            val requestString: String =
                CommonUtils.getJsonFromObject(request, CancelRescheduleRequest::class.java)

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody = requestString.toRequestBody("application/json".toMediaTypeOrNull())

            appointmentReasonsViewModel.getReasonsResponse(loginResponse.token, requestBody)
            // dashboardViewModel.getAppointmentListResponse(loginResponse.token)

        } else {
            Snackbar.make(
                mContext,
                binding.cvBottom,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }


    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when (buttonView?.id) {
            R.id.cb_selection -> {
                if (isChecked) {
                    AppConstants.checkedAppointmentIds.add(buttonView.tag.toString())
                } else {
                    AppConstants.checkedAppointmentIds.remove(buttonView.tag.toString())
                }
            }
        }
    }
}