package com.met.ahvara.ui.customer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.adapter.AdapterCustomerList
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.ContentMenuHeaderBinding
import com.met.ahvara.databinding.CustomerActivityBinding
import com.met.ahvara.model.CustomerListData
import com.met.ahvara.model.CustomerListSearchRequest
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.notifications.NotificationsActivity
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.AppConstants.Companion.INTENT_CUSTOMER_ID
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.CustomerListingViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.squareup.picasso.Picasso
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class CustomerListActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    View.OnClickListener {
    private lateinit var binding: CustomerActivityBinding

    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private lateinit var adapterCustomerList: AdapterCustomerList
    private val tag = "CustomerListActivity"
    lateinit var viewModel: CustomerListingViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        binding = CustomerActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        progressDialog = CommonUtils.getProgressDialog(mContext)

        initViewModel()
        initViews()
        initClickListener()
    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()

            viewModel.getCustomerListResponse(loginResponse.token)

        } else {
            Snackbar.make(
                mContext,
                binding.customerDrawer,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            setDataFromLogin()
            fetchDataFromWeb()
        }

        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[CustomerListingViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        viewModel.customerListResponse.observe(this) {
            if (it.status?.errorCode ?: 1000 == 0) {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                if (it.result!!.data.size > 0) {
                    setCustomerListData(it.result!!.data)
                    binding.customerContent.tvNoData.visibility = View.GONE
                } else {
                    binding.customerContent.tvNoData.visibility = View.VISIBLE
                }
            } else {
                binding.customerContent.tvNoData.visibility = View.VISIBLE
                Snackbar.make(
                    mContext,
                    binding.customerDrawer,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        viewModel.errorMessage.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")

            adapterCustomerList.clearList()
            binding.customerContent.tvNoData.visibility = View.VISIBLE
        }
    }

    private fun setCustomerListData(list: ArrayList<CustomerListData>) {
        adapterCustomerList.putData(list)
    }

    private fun initClickListener() {
        if (binding.customerDrawer.isDrawerOpen(GravityCompat.START)) {
            binding.customerDrawer.closeDrawer(GravityCompat.START)
        }
        binding.customerContent.includeTb.ivMenu.setOnClickListener {
            if (!binding.customerDrawer.isDrawerOpen(GravityCompat.START)) {
                binding.customerDrawer.openDrawer(GravityCompat.START)
            }
        }
        binding.customerContent.customerAdd.setOnClickListener {
            val intent = Intent(mContext, CustomerAddActivity::class.java)
            startActivity(intent)
        }
        binding.customerContent.includeTb.ivNotification.setOnClickListener {
            val intent = Intent(mContext, NotificationsActivity::class.java)
            mContext.startActivity(intent)
        }
    }

    private fun setDataFromLogin() {
        val loginResponseData = loginResponse.result.data
        // navView is NavigationView
        val viewHeader = binding.customerNavigationView.getHeaderView(0)
        // nav_header.xml is headerLayout
        val navViewHeaderBinding: ContentMenuHeaderBinding =
            ContentMenuHeaderBinding.bind(viewHeader)
        // title is Children of nav_header
        navViewHeaderBinding.profileName.text =
            getString(
                R.string.dashboard_nav_name,
                loginResponseData.firstName,
                loginResponseData.lastName
            )

        Picasso.get()
            .load(loginResponseData.profileImage)
            .placeholder(R.drawable.ic_baseline_person_24)
            .error(R.drawable.ic_baseline_person_24)
            .into(navViewHeaderBinding.menuDp)

        binding.customerContent.includeTb.tvTitle.text = getString(R.string.customer)

        //for st sr
        val navigationMenu = binding.customerNavigationView.menu
        if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_ST) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = false
        } else if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_SR) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = true
        }
        //for st sr
    }

    private fun initViews() {
        binding.customerNavigationView.itemIconTintList = null
        binding.customerNavigationView.setNavigationItemSelectedListener(this)

        binding.customerContent.svCustomer.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.isNotEmpty()) {
                    searchCustomer(query)
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                if (query!!.isEmpty()) {
                    fetchDataFromWeb()
                }
                return false
            }
        })

        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)

        binding.customerContent.rvCustomers.addItemDecoration(dividerItemDecoration)
        binding.customerContent.rvCustomers.layoutManager = linearLayoutManager
        adapterCustomerList = AdapterCustomerList(mContext, this)
        binding.customerContent.rvCustomers.adapter = adapterCustomerList
    }

    private fun searchCustomer(query: String) {
        if (CommonUtils.isInternetAvailable(mContext)) {
            val searchRequest = CustomerListSearchRequest()
            searchRequest.search_string = query
            val requestString: String =
                CommonUtils.getJsonFromObject(searchRequest, CustomerListSearchRequest::class.java)

            viewModel.getCustomerSearchListResponse(
                loginResponse.token,
                requestString.toRequestBody("application/json".toMediaTypeOrNull())
            )

        } else {
            Snackbar.make(
                mContext,
                binding.customerDrawer,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        CommonUtils.navigateNavigationDrawer(
            mContext, item, this,
            loginViewModel,
            null
        )
        binding.customerDrawer.closeDrawer(GravityCompat.START)

        return true
    }

    override fun onClick(view: View?) {
        if (view!!.id == R.id.ll_demo) {
            val intent = Intent(mContext, CustomerDetailsActivity::class.java)
            intent.putExtra(INTENT_CUSTOMER_ID, view.tag.toString())
            startActivity(intent)
        }
    }
}