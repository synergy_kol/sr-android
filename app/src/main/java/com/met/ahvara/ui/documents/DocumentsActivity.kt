package com.met.ahvara.ui.documents

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.StDocumentsActivityBinding

class DocumentsActivity : AppCompatActivity() {

    private lateinit var binding: StDocumentsActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = StDocumentsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.workOrdersContent.cvInvoice.setOnClickListener {
            val intent = Intent(this, InvoiceDetailsActivity::class.java)
            startActivity(intent)
        }

        binding.workOrdersContent.cvContract.setOnClickListener {
            val intent = Intent(this, ContractDetailsActivity::class.java)
            startActivity(intent)
        }
    }
}