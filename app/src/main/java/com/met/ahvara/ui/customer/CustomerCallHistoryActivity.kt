package com.met.ahvara.ui.customer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.CustomerCallHistoryBinding

class CustomerCallHistoryActivity : AppCompatActivity() {

    private lateinit var binding: CustomerCallHistoryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = CustomerCallHistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}