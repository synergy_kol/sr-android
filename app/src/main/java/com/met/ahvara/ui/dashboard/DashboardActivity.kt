package com.met.ahvara.ui.dashboard

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.CompoundButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.thunder413.datetimeutils.DateTimeUtils
import com.google.android.gms.location.*
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.kizitonwose.calendarview.utils.Size
import com.met.ahvara.R
import com.met.ahvara.adapter.AdapterDashboardAppointments
import com.met.ahvara.adapter.AdapterDashboardTasks
import com.met.ahvara.adapter.AdapterDashboardWorkOrders
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.*
import com.met.ahvara.model.*
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.appointments.*
import com.met.ahvara.ui.map.RoutePlannerActivity
import com.met.ahvara.ui.notifications.NotificationsActivity
import com.met.ahvara.ui.task.TaskListActivity
import com.met.ahvara.ui.workorder.WorkOrderDetailsActivity
import com.met.ahvara.ui.workorder.WorkOrdersActivity
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.AppConstants.Companion.INTENT_WORK_ORDER_ID
import com.met.ahvara.utils.AppConstants.Companion.TIMER_INTERVAL
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.utils.subscribeOnBackground
import com.met.ahvara.viewmodel.DashboardViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.met.ahvara.weekview.sample.MainActivity
import com.squareup.picasso.Picasso
import com.vmadalin.easypermissions.EasyPermissions
import com.vmadalin.easypermissions.dialogs.DEFAULT_SETTINGS_REQ_CODE
import com.vmadalin.easypermissions.dialogs.SettingsDialog
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

class DashboardActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    CompoundButton.OnCheckedChangeListener, View.OnClickListener,
    EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks {
    private lateinit var binding: StDashboardActivityBinding

    private lateinit var markAsArrivedAppointmentId: String
    private lateinit var markAsArrivedRequest: StatusChangeRequest
    private lateinit var appointmentListResponse: AppointmentListResponse
    private lateinit var formattedTime: String
    private var timeToAdd: Long = 0
    private lateinit var adapterDashboardAppointments: AdapterDashboardAppointments
    private lateinit var stCurrentDate: String

    //weekview
    private var selectedDate = LocalDate.now()
    private val dateFormatter = DateTimeFormatter.ofPattern("dd")
    private val dayFormatter = DateTimeFormatter.ofPattern("EEE")
    private val monthFormatter = DateTimeFormatter.ofPattern("MMM")
    //weekview

    private lateinit var submitCheckinRequest: SubmitCheckinRequest

    //location
    private var locationFlag: Boolean = false

    private var latitude by Delegates.notNull<Double>()
    private var longitude by Delegates.notNull<Double>()

    // FusedLocationProviderClient - Main class for receiving location updates.
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    // LocationRequest - Requirements for the location updates, i.e.,
    // how often you should receive updates, the priority, etc.
    private lateinit var locationRequest: LocationRequest

    // LocationCallback - Called when FusedLocationProviderClient
    // has a new Location
    private lateinit var locationCallback: LocationCallback

    // This will store current location info
    private lateinit var currentLocation: Location
    private val RC_LOCATION_PERM = 124
    //location

    private var progressPadding = 0
    private var progressDailyClick = true

    private val tag = "DashboardActivity"
    lateinit var dashboardViewModel: DashboardViewModel
    lateinit var loginViewModel: LoginViewModel

    lateinit var retrofitService: RetrofitService
    lateinit var loginResponse: LoginResponse

    private lateinit var progressDailyWeeklyData: ProgressDailyWeeklyData
    private lateinit var workOrderListData: ArrayList<WorkOrderListData>
    private lateinit var taskListData: ArrayList<TaskListData>
    private lateinit var upNextAppointmentData: UpNextAppointmentListData

    private lateinit var mContext: Context
    private var doubleBackToExitPressedOnce = false

    private lateinit var popupClockIn: AlertDialog
    private lateinit var popupClockOut: AlertDialog
    private lateinit var progressDialog: AlertDialog

    ///stopwatch
    private val mInterval = TIMER_INTERVAL // 1 second in this case
    private var mHandler: Handler? = null
    private var timeInSeconds = 0L

    private var mStatusChecker: Runnable = object : Runnable {
        override fun run() {
            try {
                timeInSeconds += 1
                updateStopWatchView(timeInSeconds)
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler!!.postDelayed(this, mInterval.toLong())
            }
        }
    }    ///stopwatch

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        progressDialog = CommonUtils.getProgressDialog(mContext)

        retrofitService = ApiClient.getInstance()
        binding = StDashboardActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (!progressDialog.isShowing)
            progressDialog.show()

        val date = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        stCurrentDate = date.format(formatter)

        locationTask()
        initViewModel()
        initViews()
        initClickListener()
    }

    override fun onResume() {
        super.onResume()

        if ((this::dashboardViewModel.isInitialized) && (this::loginResponse.isInitialized)) {
            if (CommonUtils.isInternetAvailable(mContext)) {
                val request = AppointmentListRequest()
                request.date = stCurrentDate
                request.filter_by = AppConstants.APPOINTMENT_FILTER_BY_DATE

                val requestString: String =
                    CommonUtils.getJsonFromObject(request, AppointmentListRequest::class.java)

                // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
                val requestBody =
                    requestString.toRequestBody("application/json".toMediaTypeOrNull())

                dashboardViewModel.getAppointmentListResponse(loginResponse.token, requestBody)
                dashboardViewModel.getUpNextAppointmentResponse(loginResponse.token)
                dashboardViewModel.getProgressDailyWeeklyResponse(loginResponse.token)
                dashboardViewModel.getWorkOrderListResponse(loginResponse.token)
                dashboardViewModel.getTaskListResponse(loginResponse.token)
            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardNavigationView,
                    getString(R.string.internet_connection),
                    LENGTH_LONG
                ).show()
            }
        }
    }

    private fun setDataFromLogin() {
        // navView is NavigationView
        val viewHeader = binding.dashboardNavigationView.getHeaderView(0)
        // nav_header.xml is headerLayout
        val navViewHeaderBinding: ContentMenuHeaderBinding =
            ContentMenuHeaderBinding.bind(viewHeader)
        // title is Children of nav_header
        navViewHeaderBinding.profileName.text =
            getString(
                R.string.dashboard_nav_name,
                loginResponse.result.data.firstName,
                loginResponse.result.data.lastName
            )

        Picasso.get()
            .load(loginResponse.result.data.profileImage)
            .placeholder(R.drawable.ic_baseline_person_24)
            .error(R.drawable.ic_baseline_person_24)
            .into(navViewHeaderBinding.menuDp)

        binding.dashboardContent.includeTb.tvTitle.text = getString(
            R.string.dashboard_name, loginResponse.result.data.firstName ?: ""
        )

        fetchDataFromWeb(false)

        //for st/sr
        val navigationMenu = binding.dashboardNavigationView.menu
        if (loginResponse.result.data.roleId!! == AppConstants.LOGIN_TYPE_ST) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = false
        } else if (loginResponse.result.data.roleId!! == AppConstants.LOGIN_TYPE_SR) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = true
        }
        //for st/sr
    }

    private fun fetchDataFromWeb(dateChange: Boolean) {
        if (CommonUtils.isInternetAvailable(mContext)) {
            if (!progressDialog.isShowing)
                progressDialog.show()
            val request = AppointmentListRequest()

            request.date = stCurrentDate
            request.filter_by = AppConstants.APPOINTMENT_FILTER_BY_DATE

            val requestString: String =
                CommonUtils.getJsonFromObject(request, AppointmentListRequest::class.java)

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody =
                requestString.toRequestBody("application/json".toMediaTypeOrNull())

            if (dateChange) {
                dashboardViewModel.getAppointmentListResponse(loginResponse.token, requestBody)
                /* dashboardViewModel.getUpNextAppointmentResponse(loginResponse.token)
                 dashboardViewModel.getProgressDailyWeeklyResponse(loginResponse.token)
                 dashboardViewModel.getWorkOrderListResponse(loginResponse.token)
                 dashboardViewModel.getTaskListResponse(loginResponse.token)*/
            } else {
                dashboardViewModel.getAppointmentListResponse(loginResponse.token, requestBody)
                dashboardViewModel.getUpNextAppointmentResponse(loginResponse.token)
                dashboardViewModel.getProgressDailyWeeklyResponse(loginResponse.token)
                dashboardViewModel.getWorkOrderListResponse(loginResponse.token)
                dashboardViewModel.getTaskListResponse(loginResponse.token)
            }
        } else {
            Snackbar.make(
                mContext,
                binding.dashboardNavigationView,
                getString(R.string.internet_connection),
                LENGTH_LONG
            ).show()
        }
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        val loginLivaData = loginViewModel.getAllLogin()
        loginLivaData.observe(this) {
            loginResponse = it
            setDataFromLogin()
        }

        dashboardViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[DashboardViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        dashboardViewModel.observerAppointmentListResponse.observe(this) {
            if (it?.status?.errorCode ?: 1 == 0) {
                appointmentListResponse = it
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                if (appointmentListResponse.result!!.data!!.listdata.size > 0) {
                    adapterDashboardAppointments.putData(appointmentListResponse.result!!.data!!.listdata)
                } else {
                    adapterDashboardAppointments.clearList()
                }
            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardDrawer,
                    it?.status?.message ?: getString(R.string.api_call_error),
                    LENGTH_SHORT
                ).show()
            }
        }
        dashboardViewModel.observerProgressDailyWeeklyResponse.observe(this) {
            if (it?.status?.errorCode ?: 1 == 0) {
                progressDailyWeeklyData = it?.result!!.data!!
                setAppointmentProgressData()
            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardDrawer,
                    it?.status?.message ?: getString(R.string.api_call_error),
                    LENGTH_SHORT
                ).show()
            }
        }
        dashboardViewModel.observerWorkOrderListResponse.observe(this) {
            if (it?.status?.errorCode ?: 1 == 0) {
                workOrderListData = it.result!!.data
                setWorkOrdersListData()
            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardDrawer,
                    it?.status?.message ?: getString(R.string.api_call_error),
                    LENGTH_SHORT
                ).show()
            }
        }

        dashboardViewModel.observerTasksListResponse.observe(this) {
            if (it?.status?.errorCode ?: 1 == 0) {
                taskListData = it.result!!.data
                setTaskListData()
            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardDrawer,
                    it?.status?.message ?: getString(R.string.api_call_error),
                    LENGTH_SHORT
                ).show()
            }
        }

        dashboardViewModel.observerUpNextAppointment.observe(this) {
            if (it?.status?.errorCode ?: 1 == 0) {
                if (it?.result?.data?.listdata != null) {
                    upNextAppointmentData = it.result!!.data!!.listdata!!
                    setUpNextAppointmentData()
                    binding.dashboardContent.inclueUpNext.cvTop.visibility = View.VISIBLE
                } else {
                    binding.dashboardContent.inclueUpNext.cvTop.visibility = View.GONE
                }
            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardDrawer,
                    it?.status?.message ?: getString(R.string.api_call_error),
                    LENGTH_SHORT
                ).show()
            }
        }

        dashboardViewModel.observerSubmitCheckinResponse.observe(this) {
            if (it?.status?.errorCode ?: 1 == 0) {
                binding.dashboardContent.btClockText.text = getString(R.string.clock_out)
                binding.dashboardContent.tvShiftStarted.visibility = View.VISIBLE

                subscribeOnBackground {
                    dashboardViewModel.deleteAllCheckin()
                    dashboardViewModel.insertCheckin(submitCheckinRequest)
                }
                if (popupClockIn.isShowing) {
                    popupClockIn.dismiss()
                }
                stopTimer()
            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardDrawer,
                    it?.status?.message ?: getString(R.string.api_call_error),
                    LENGTH_SHORT
                ).show()
            }
        }
        dashboardViewModel.observerSubmitCheckOutResponse.observe(this) {
            if (it?.status?.errorCode ?: 1 == 0) {
                binding.dashboardContent.btClockText.text = getString(R.string.clock_in)

                binding.dashboardContent.tvShiftStarted.visibility = View.INVISIBLE

                timeInSeconds = 0
                timeToAdd = 0
                binding.dashboardContent.tvStartTimeValue.text = "00:00"
                binding.dashboardContent.tvDurationValue.text = "00:00:00"

                subscribeOnBackground {
                    dashboardViewModel.deleteAllCheckin()
                }
                if (popupClockOut.isShowing) {
                    popupClockOut.dismiss()
                }
                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }

                stopTimer()
                mHandler?.removeCallbacks(mStatusChecker)

            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardDrawer,
                    it?.status?.message ?: getString(R.string.api_call_error),
                    LENGTH_SHORT
                ).show()
            }
        }

        dashboardViewModel.getAllCheckin().observe(this)
        {
            if (it?.time != null) {
                binding.dashboardContent.btClockText.text = getString(R.string.clock_out)
                binding.dashboardContent.tvStartTimeValue.text = it.time
                binding.dashboardContent.tvShiftStarted.visibility = View.VISIBLE

                timeToAdd = System.currentTimeMillis() - it.timestamp.toLong()
                val time = DateTimeUtils.millisToTime(timeToAdd)
                binding.dashboardContent.tvDurationValue.text = time
                startTimer()
            }
        }

        dashboardViewModel.observerMarkAsArrivedStatusChangeResponse.observe(this)
        {
            if (this::markAsArrivedRequest.isInitialized) {
                subscribeOnBackground {
                    dashboardViewModel.deleteAllMarkAsArrived()
                    dashboardViewModel.insertMarkAsArrived(markAsArrivedRequest)
                }
                fetchDataFromWeb(false)
            }
        }

        dashboardViewModel.getAllMarkAsArrived().observe(this)
        {
            if (it?.appointmentId != null && (this::loginResponse.isInitialized)) {
                markAsArrivedAppointmentId = it.appointmentId
                dashboardViewModel.getUpNextAppointmentResponse(loginResponse.token)
            }
        }

        //invoked when a network exception occurred
        dashboardViewModel.observerErrorMessage.observe(this)
        {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")
        }
    }

    private fun updateStopWatchView(timeInSeconds: Long) {
        formattedTime = CommonUtils.getFormattedStopWatch(timeToAdd + (timeInSeconds * 1000))
        binding.dashboardContent.tvDurationValue.text = formattedTime
    }

    private fun startTimer() {
        mHandler = Handler(Looper.getMainLooper())
        mStatusChecker.run()
    }

    private fun stopTimer() {
        mHandler?.removeCallbacks(mStatusChecker)
    }

    private fun setUpNextAppointmentData() {
        if (this::markAsArrivedAppointmentId.isInitialized && upNextAppointmentData.id == markAsArrivedAppointmentId) {
            binding.dashboardContent.inclueUpNext.tvTitle.text =
                getString(R.string.upnext_title_in_progress)
            binding.dashboardContent.inclueUpNext.tvNextGetDirection.text =
                getString(R.string.upnext_order_parts)
            binding.dashboardContent.inclueUpNext.tvNextGetDirection.setCompoundDrawablesWithIntrinsicBounds(
                0,
                R.drawable.ic_baseline_shopping_cart_24,
                0,
                0
            )

            binding.dashboardContent.inclueUpNext.tvNextMarkArrived.text =
                getString(R.string.upnext_complete)
            binding.dashboardContent.inclueUpNext.tvNextMarkArrived.setCompoundDrawablesWithIntrinsicBounds(
                0,
                R.drawable.ic_baseline_check_24,
                0,
                0
            )

            // sdfsdf
        } else {
            binding.dashboardContent.inclueUpNext.tvTitle.text =
                getString(R.string.upnext_title_upnext)
            binding.dashboardContent.inclueUpNext.tvNextGetDirection.text =
                getString(R.string.upnext_get_direction)
            binding.dashboardContent.inclueUpNext.tvNextGetDirection.setCompoundDrawablesWithIntrinsicBounds(
                0,
                R.drawable.ic_directions_white,
                0,
                0
            )

            binding.dashboardContent.inclueUpNext.tvNextMarkArrived.text =
                getString(R.string.upnext_mark_arrived)
            binding.dashboardContent.inclueUpNext.tvNextMarkArrived.setCompoundDrawablesWithIntrinsicBounds(
                0,
                R.drawable.ic_baseline_outlined_flag_24,
                0,
                0
            )
        }

        if (upNextAppointmentData.startDateTime?.isEmpty() == false) {
            binding.dashboardContent.inclueUpNext.tvNextTime.text = CommonUtils.getDateUsingPattern(
                upNextAppointmentData.startDateTime!!,
                "yyyy-MM-dd HH:mm:ss",
                "hh:mm aa"
            )
            //  "2022-04-01 04:00:00"

            binding.dashboardContent.inclueUpNext.tvNextName.text =
                upNextAppointmentData.companyName
            binding.dashboardContent.inclueUpNext.tvNextAddress.text = upNextAppointmentData.address
            //    binding.dashboardContent.inclueUpNext.tvNextAddress.text = upNextAppointmentData.siteAddress


        } else {
            binding.dashboardContent.inclueUpNext.cvTop.visibility = View.GONE
        }
    }

    private fun setTaskListData() {
        var noOfOpenTasks = 0
        for (noOfOpen in taskListData) {
            if (noOfOpen.status.equals("Open", true))
                noOfOpenTasks++
        }
        binding.dashboardContent.tvTasksOpenNo.text =
            getString(R.string.no_of_open, noOfOpenTasks)
        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)

        binding.dashboardContent.rvTasks.addItemDecoration(dividerItemDecoration)
        binding.dashboardContent.rvTasks.layoutManager = linearLayoutManager
        binding.dashboardContent.rvTasks.adapter =
            AdapterDashboardTasks(mContext, taskListData, this)
    }

    private fun setWorkOrdersListData() {
        var noOfOpenWorkOrders = 0

        for (noOfOpen in workOrderListData) {
            if (noOfOpen.status.equals("Open", true))
                noOfOpenWorkOrders++
        }
        //listing
        binding.dashboardContent.tvWorkOrdersOpen.text =
            getString(R.string.no_of_open, noOfOpenWorkOrders)
        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)

        binding.dashboardContent.rvWorkOrders.addItemDecoration(dividerItemDecoration)
        binding.dashboardContent.rvWorkOrders.layoutManager = linearLayoutManager
        binding.dashboardContent.rvWorkOrders.adapter =
            AdapterDashboardWorkOrders(mContext, workOrderListData, this)
        //listing
    }

    private fun setAppointmentProgressData() {
        binding.dashboardContent.tvProgress.text = getString(
            R.string.dashboard_progress_out_of,
            progressDailyWeeklyData.allDayCompleteListdata, progressDailyWeeklyData.allDayListdata
        )
        binding.dashboardContent.pbAppointments.max = progressDailyWeeklyData.allDayListdata ?: 0
        binding.dashboardContent.pbAppointments.progress =
            progressDailyWeeklyData.allDayCompleteListdata ?: 0
    }

    private fun setAppointmentListDataView() {
        adapterDashboardAppointments = AdapterDashboardAppointments(mContext, this, this)
        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)
        binding.dashboardContent.rvAppointments.addItemDecoration(dividerItemDecoration)
        binding.dashboardContent.rvAppointments.layoutManager = linearLayoutManager
        binding.dashboardContent.rvAppointments.adapter = adapterDashboardAppointments
    }


    private fun markAsArrived(appointmentId: String) {
        val request =
            StatusChangeRequest(
                appointmentId,
                AppConstants.APPOINTMENT_STATUS_ARRIVED,
                latitude.toString(),
                longitude.toString()
            )

        markAsArrivedRequest = request

        val requestString: String =
            CommonUtils.getJsonFromObject(
                request, StatusChangeRequest::class.java
            )

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody =
            requestString.toRequestBody("application/json".toMediaTypeOrNull())

        dashboardViewModel.getStatusChangeResponse(
            loginResponse.token,
            requestBody,
            AppConstants.APPOINTMENT_STATUS_ARRIVED
        )
    }

    private fun initViews() {
        binding.dashboardNavigationView.itemIconTintList = null
        binding.dashboardNavigationView.setNavigationItemSelectedListener(this)
        progressPadding = binding.dashboardContent.tvDaily.paddingTop

        val dm = DisplayMetrics()
        val wm = mContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.defaultDisplay.getMetrics(dm)
        binding.dashboardContent.exSevenCalendar.apply {
            val dayWidth = dm.widthPixels / 6
            val dayHeight = (dayWidth * 1.35).toInt()
            daySize = Size(dayWidth, dayHeight)
        }

        class DayViewContainer(view: View) : ViewContainer(view) {
            val bind = Example7CalendarDayBinding.bind(view)
            lateinit var day: CalendarDay

            init {
                view.setOnClickListener {
                    val firstDay = binding.dashboardContent.exSevenCalendar.findFirstVisibleDay()
                    val lastDay = binding.dashboardContent.exSevenCalendar.findLastVisibleDay()
                    if (firstDay == day) {
                        // If the first date on screen was clicked, we scroll to the date to ensure
                        // it is fully visible if it was partially off the screen when clicked.
                        binding.dashboardContent.exSevenCalendar.smoothScrollToDate(day.date)
                    } else if (lastDay == day) {
                        // If the last date was clicked, we scroll to 4 days ago, this forces the
                        // clicked date to be fully visible if it was partially off the screen.
                        // We scroll to 4 days ago because we show max of five days on the screen
                        // so scrolling to 4 days ago brings the clicked date into full visibility
                        // at the end of the calendar view.
                        binding.dashboardContent.exSevenCalendar.smoothScrollToDate(
                            day.date.minusDays(
                                4
                            )
                        )
                    }

                    // Example: If you want the clicked date to always be centered on the screen,
                    // you would use: exSevenCalendar.smoothScrollToDate(day.date.minusDays(2))

                    if (selectedDate != day.date) {
                        val oldDate = selectedDate
                        selectedDate = day.date
                        binding.dashboardContent.exSevenCalendar.notifyDateChanged(day.date)
                        oldDate?.let { binding.dashboardContent.exSevenCalendar.notifyDateChanged(it) }

                        Log.e(tag, selectedDate.toString())
                        if (AppConstants.checkedAppointmentIds.size > 0) {
                            AppConstants.checkedAppointmentIds.clear()
                        }

                        stCurrentDate = selectedDate.toString()
                        fetchDataFromWeb(true)
                    }
                }
            }

            fun bind(day: CalendarDay) {
                this.day = day

                Log.e(tag, day.toString())

                bind.exSevenDateText.text = dateFormatter.format(day.date)
                bind.exSevenDayText.text = dayFormatter.format(day.date)
                bind.exSevenMonthText.text = monthFormatter.format(day.date)

                bind.exSevenDateText.setTextColor(view.context.getColor(if (day.date == selectedDate) R.color.yellow else R.color.color_dark_grey))
                bind.exSevenSelectedView.isVisible = day.date == selectedDate
            }
        }

        binding.dashboardContent.exSevenCalendar.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) = container.bind(day)
        }

        val currentMonth = YearMonth.now()
        // Value for firstDayOfWeek does not matter since inDates and outDates are not generated.
        binding.dashboardContent.exSevenCalendar.setup(
            currentMonth,
            currentMonth.plusMonths(2),
            DayOfWeek.values().random()
        )
        binding.dashboardContent.exSevenCalendar.scrollToDate(LocalDate.now())

        binding.dashboardContent.srlTop.setOnRefreshListener {
            fetchDataFromWeb(false)
            binding.dashboardContent.srlTop.isRefreshing = false;
        }

        setAppointmentListDataView()
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Snackbar.make(
            binding.dashboardNavigationView,
            getString(R.string.double_back_to_exit),
            Snackbar.LENGTH_SHORT
        ).show()

        Handler(Looper.getMainLooper()).postDelayed({
            doubleBackToExitPressedOnce = false
        }, 2000)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        CommonUtils.navigateNavigationDrawer(
            mContext,
            item,
            this,
            loginViewModel,
            dashboardViewModel
        )

        binding.dashboardDrawer.closeDrawer(GravityCompat.START)

        return true
    }

    private fun showClockIn() {
        val dialogBinding = DialogClockInBinding.inflate(LayoutInflater.from(mContext))
        val builder = AlertDialog.Builder(mContext, R.style.popupDialog)
        builder.setCancelable(false)
        builder.setView(dialogBinding.root)

        dialogBinding.clockInCancel.setOnClickListener {
            if (popupClockIn.isShowing) {
                popupClockIn.dismiss()
            }
        }

        dialogBinding.clockInProceed.setOnClickListener {
            if (locationFlag) {

                if (CommonUtils.isInternetAvailable(mContext)) {
                    val stCheckinTime = CommonUtils.getCurrentTimeHHmm()
                    val stCheckinTimeStamp = System.currentTimeMillis().toString()

                    submitCheckinRequest =
                        SubmitCheckinRequest(
                            stCheckinTime,
                            latitude.toString(),
                            longitude.toString(),
                            stCheckinTimeStamp
                        )

                    val requestString: String =
                        CommonUtils.getJsonFromObject(
                            submitCheckinRequest,
                            SubmitCheckinRequest::class.java
                        )

                    // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
                    val requestBody =
                        requestString.toRequestBody("application/json".toMediaTypeOrNull())

                    dashboardViewModel.getSubmitCheckinResponse(loginResponse.token, requestBody)

                } else {
                    Snackbar.make(
                        mContext,
                        binding.dashboardContent.parent,
                        getString(R.string.internet_connection),
                        LENGTH_SHORT
                    ).show()
                }
            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardContent.parent,
                    getString(R.string.fetching_lat_long),
                    LENGTH_SHORT
                ).show()
            }
        }
        popupClockIn = builder.create()
        popupClockIn.show()
    }

    private fun showClockOut() {

        val dialogBinding = DialogClockOutBinding.inflate(LayoutInflater.from(mContext))
        val builder = AlertDialog.Builder(this, R.style.popupDialog)
        builder.setCancelable(false)
        builder.setView(dialogBinding.root)

        val mHandler = Handler(Looper.getMainLooper())
        val mStatusChecker: Runnable = object : Runnable {
            override fun run() {
                try {
                    dialogBinding.tvTimeHour.text = formattedTime.substringBefore(":")
                    dialogBinding.tvTimeMinute.text = formattedTime.substringBeforeLast(":")
                        .substringAfter(":")
                    dialogBinding.tvTimeSeconds.text = formattedTime.substringAfterLast(":")

                } finally {
                    // 100% guarantee that this always happens, even if
                    // your update method throws an exception
                    mHandler.postDelayed(this, mInterval.toLong())
                }
            }
        }
        mStatusChecker.run()
        dialogBinding.tvCurrentProgress.text =
            progressDailyWeeklyData.allDayCompleteListdata.toString()

        dialogBinding.tvTotalAppointments.text =
            " of " + progressDailyWeeklyData.allDayListdata.toString()

        dialogBinding.tvIncompleteAppointments.text =
            progressDailyWeeklyData.allDayCompleteListdata?.let {
                progressDailyWeeklyData.allDayListdata?.minus(it)
                    .toString()
            }
        dialogBinding.pbAppointments.max = progressDailyWeeklyData.allDayListdata ?: 0
        dialogBinding.pbAppointments.progress = progressDailyWeeklyData?.allDayCompleteListdata ?: 0


        dialogBinding.clockOutCancel.setOnClickListener {
            if (popupClockOut.isShowing) {
                popupClockOut.dismiss()
            }
        }

        dialogBinding.clockOutProceed.setOnClickListener {

            if (locationFlag) {
                if ((progressDailyWeeklyData.allDayCompleteListdata?.let { it1 ->
                        progressDailyWeeklyData.allDayListdata?.minus(it1)
                    }) == 0) {
                    doCheckOut()
                    if (popupClockOut.isShowing) {
                        popupClockOut.dismiss()
                    }
                } else {
                    Snackbar.make(
                        mContext,
                        dialogBinding.root,
                        "Please either complete/cancel or reschedule all the appointments first",
                        LENGTH_SHORT
                    ).show()
                }
            } else {
                Snackbar.make(
                    mContext,
                    binding.dashboardContent.parent,
                    getString(R.string.fetching_lat_long),
                    LENGTH_SHORT
                ).show()
            }
        }
        popupClockOut = builder.create()
        popupClockOut.show()
    }

    private fun doCheckOut() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()

            val stCheckinTime = CommonUtils.getCurrentTimeHHmm()
            val submitCheckoutRequest =
                SubmitCheckoutRequest(stCheckinTime, latitude.toString(), longitude.toString())

            val requestString: String =
                CommonUtils.getJsonFromObject(
                    submitCheckoutRequest,
                    SubmitCheckoutRequest::class.java
                )

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody =
                requestString.toRequestBody("application/json".toMediaTypeOrNull())

            dashboardViewModel.getSubmitCheckOutResponse(loginResponse.token, requestBody)

        } else {
            Snackbar.make(
                mContext,
                binding.dashboardContent.parent,
                getString(R.string.internet_connection),
                LENGTH_SHORT
            ).show()
        }
    }

    private fun initClickListener() {

        binding.dashboardContent.includeTb.ivMenu.setOnClickListener {
            if (!binding.dashboardDrawer.isDrawerOpen(GravityCompat.START)) {
                binding.dashboardDrawer.openDrawer(GravityCompat.START)
            }
        }

        binding.dashboardContent.btClockText.setOnClickListener {
            when (binding.dashboardContent.btClockText.text) {
                getString(R.string.clock_in) -> {
                    showClockIn()
                }
                getString(R.string.clock_out) -> {
                    showClockOut()
                }
            }
        }

        binding.dashboardContent.rlReschedule.setOnClickListener {
            Log.e(tag, AppConstants.checkedAppointmentIds.size.toString())
            if (AppConstants.checkedAppointmentIds.size == 1) {
                val intent = Intent(mContext, RescheduleActivity::class.java)
                intent.putExtra(AppConstants.INTENT_APPOINTMENT_LIST, appointmentListResponse)
                startActivity(intent)
            } else {
                Snackbar.make(
                    binding.dashboardNavigationView,
                    "Please select only one appointment",
                    LENGTH_SHORT
                ).show()
            }
        }

        binding.dashboardContent.rlCancel.setOnClickListener {
            Log.e(tag, AppConstants.checkedAppointmentIds.size.toString())
            if (AppConstants.checkedAppointmentIds.size > 0) {
                val intent = Intent(mContext, CancellationActivity::class.java)
                intent.putExtra(AppConstants.INTENT_APPOINTMENT_LIST, appointmentListResponse)
                startActivity(intent)
            } else {
                Snackbar.make(
                    binding.dashboardNavigationView,
                    "Please select an appointment first",
                    LENGTH_SHORT
                ).show()
            }
        }

        binding.dashboardContent.inclueUpNext.tvNextGetDirection.setOnClickListener {
            if (binding.dashboardContent.inclueUpNext.tvTitle.text == resources.getString(R.string.upnext_title_upnext)) {
                val intent = Intent(mContext, RoutePlannerActivity::class.java)
                intent.putExtra(AppConstants.INTENT_UP_NEXT_APPOINTMENT, upNextAppointmentData)
                startActivity(intent)
            } else {
                Log.e("upnext", "order parts--" + Throwable().stackTrace[0].lineNumber)
                /*val intent = Intent(mContext, O::class.java)
                intent.putExtra(AppConstants.INTENT_UP_NEXT_APPOINTMENT, upNextAppointmentData)
                startActivity(intent)*/
            }
            //open map
        }
        binding.dashboardContent.inclueUpNext.tvNextMarkArrived.setOnClickListener {
            if (binding.dashboardContent.inclueUpNext.tvTitle.text == resources.getString(R.string.upnext_title_upnext)) {
                markAsArrived(upNextAppointmentData.id ?: "")

            } else {
                Log.e("upnext", "mark complete--" + Throwable().stackTrace[0].lineNumber)
                /*val intent = Intent(mContext, O::class.java)
                intent.putExtra(AppConstants.INTENT_UP_NEXT_APPOINTMENT, upNextAppointmentData)
                startActivity(intent)*/
            }


            //mark arrived
        }

        binding.dashboardContent.tvAddNewSchedule.setOnClickListener {
            //new schedule
        }

        binding.dashboardContent.cvSelectAll.setOnClickListener {
            //select all appointments
        }

        binding.dashboardContent.tvDaily.setOnClickListener {
            if (!progressDailyClick) {
                binding.dashboardContent.tvDaily.background =
                    AppCompatResources.getDrawable(mContext, R.drawable.style_half_left)
                binding.dashboardContent.tvDaily.setPadding(0, progressPadding, 0, progressPadding)

                binding.dashboardContent.tvWeekly.background =
                    AppCompatResources.getDrawable(mContext, R.drawable.style_half_right_deselect)
                binding.dashboardContent.tvWeekly.setPadding(0, progressPadding, 0, progressPadding)

                binding.dashboardContent.tvProgress.text = getString(
                    R.string.dashboard_progress_out_of,
                    progressDailyWeeklyData.allDayCompleteListdata,
                    progressDailyWeeklyData.allDayListdata
                )
                binding.dashboardContent.pbAppointments.max =
                    progressDailyWeeklyData.allDayListdata ?: 0
                binding.dashboardContent.pbAppointments.progress =
                    progressDailyWeeklyData.allDayCompleteListdata ?: 0

                progressDailyClick = true
            }
        }
        binding.dashboardContent.tvWeekly.setOnClickListener {
            if (progressDailyClick) {
                binding.dashboardContent.tvWeekly.background =
                    AppCompatResources.getDrawable(mContext, R.drawable.style_half_right)
                binding.dashboardContent.tvWeekly.setPadding(0, progressPadding, 0, progressPadding)

                binding.dashboardContent.tvDaily.background =
                    AppCompatResources.getDrawable(mContext, R.drawable.style_half_left_deselect)
                binding.dashboardContent.tvDaily.setPadding(0, progressPadding, 0, progressPadding)

                binding.dashboardContent.tvProgress.text = getString(
                    R.string.dashboard_progress_out_of,
                    progressDailyWeeklyData.allWeekCompleteListdata,
                    progressDailyWeeklyData.allWeekListdata
                )
                binding.dashboardContent.pbAppointments.max =
                    progressDailyWeeklyData.allWeekListdata ?: 0
                binding.dashboardContent.pbAppointments.progress =
                    progressDailyWeeklyData.allWeekCompleteListdata ?: 0

                progressDailyClick = false
            }
        }
        binding.dashboardContent.includeTb.ivNotification.setOnClickListener {
            val intent = Intent(mContext, NotificationsActivity::class.java)
            startActivity(intent)
        }

        binding.dashboardContent.includeTb.ivSearch.setOnClickListener {
            val intent = Intent(mContext, MainActivity::class.java)
            startActivity(intent)
        }

        binding.dashboardContent.btTasksViewAll.setOnClickListener {
            val intent = Intent(mContext, TaskListActivity::class.java)
            startActivity(intent)
        }

        binding.dashboardContent.btWorkOrdersViewAll.setOnClickListener {
            val intent = Intent(mContext, WorkOrdersActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.ll_work_orders_parent -> {
                val intent = Intent(mContext, WorkOrderDetailsActivity::class.java)
                intent.putExtra(INTENT_WORK_ORDER_ID, view.tag.toString())
                startActivity(intent)
            }
            R.id.cl_arrive_top -> {
                val intent = Intent(mContext, AppointmentDetailsArrivedActivity::class.java)
                intent.putExtra(
                    AppConstants.INTENT_APPOINTMENT_DETAILS,
                    view.tag as AppointmentListListdata
                )
                startActivity(intent)
            }
            R.id.cl_completed_top -> {
                val intent = Intent(mContext, AppointmentDetailsCompletedActivity::class.java)
                intent.putExtra(
                    AppConstants.INTENT_APPOINTMENT_DETAILS,
                    view.tag as AppointmentListListdata
                )
                startActivity(intent)
            }
            R.id.cl_rescheduled_top -> {
                val intent = Intent(mContext, AppointmentDetailsRescheduledActivity::class.java)
                intent.putExtra(
                    AppConstants.INTENT_APPOINTMENT_DETAILS,
                    view.tag as AppointmentListListdata
                )
                startActivity(intent)
            }
            R.id.clScheduledTop -> {
                val intent = Intent(mContext, AppointmentDetailsScheduledActivity::class.java)
                intent.putExtra(
                    AppConstants.INTENT_APPOINTMENT_DETAILS,
                    view.tag as AppointmentListListdata
                )
                startActivity(intent)
            }
        }
    }

    private fun locationTask() {

        // Have permission, do the thing!
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create().apply {
            // Sets the desired interval for
            // active location updates.
            // This interval is inexact.
            interval = TimeUnit.SECONDS.toMillis(3)

            // Sets the fastest rate for active location updates.
            // This interval is exact, and your application will never
            // receive updates more frequently than this value
            fastestInterval = TimeUnit.SECONDS.toMillis(5)

            // Sets the maximum time when batched location
            // updates are delivered. Updates may be
            // delivered sooner than this interval
            maxWaitTime = TimeUnit.SECONDS.toMillis(10)

            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                locationResult.lastLocation.let {
                    locationFlag = true
                    currentLocation = it
                    latitude = currentLocation.latitude
                    longitude = currentLocation.longitude
                }
            }
        }

        if (hasLocationPermissions()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.rationale_location),
                    RC_LOCATION_PERM,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                return
            }
            Looper.myLooper()?.let {
                fusedLocationProviderClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    it
                )
            }

            fusedLocationProviderClient.lastLocation.addOnSuccessListener { location: Location? ->
                // Got last known location. In some rare situations this can be null.

                locationFlag = true
                currentLocation = location!!
                latitude = currentLocation.latitude
                longitude = currentLocation.longitude
                /*Log.e(
                    "location",
                    "new processlastLocation" + location?.latitude + "---" + location?.longitude
                )*/
            }

        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_location),
                RC_LOCATION_PERM,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }

    }

    private fun hasLocationPermissions(): Boolean {
        return EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        Log.d(tag, "onPermissionsGranted:" + requestCode + ":" + perms.size)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        Looper.myLooper()?.let {
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                it
            )
        }
    }

    override fun onRationaleAccepted(requestCode: Int) {
        Log.d(tag, "onRationaleAccepted:$requestCode")
    }

    override fun onRationaleDenied(requestCode: Int) {
        Log.d(tag, "onRationaleDenied:$requestCode")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Log.d(tag, "onPermissionsDenied:" + requestCode + ":" + perms.size)

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            SettingsDialog.Builder(this).build().show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DEFAULT_SETTINGS_REQ_CODE) {
            val yes = getString(R.string.yes)
            val no = getString(R.string.no)
            // Do something after user returned from app settings screen, like showing a Toast.
            Toast.makeText(
                this,
                getString(
                    R.string.returned_from_app_settings_to_activity,
                    if (hasLocationPermissions()) yes else no,
                ),
                Toast.LENGTH_LONG
            )
                .show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (progressDialog.isShowing)
            progressDialog.dismiss()

        stopTimer()
        loginViewModel.getAllLogin().removeObservers(this)
        dashboardViewModel.observerSubmitCheckinResponse.removeObservers(this)
        dashboardViewModel.observerAppointmentListResponse.removeObservers(this)
        dashboardViewModel.observerErrorMessage.removeObservers(this)
        dashboardViewModel.observerProgressDailyWeeklyResponse.removeObservers(this)
        dashboardViewModel.observerTasksListResponse.removeObservers(this)
        dashboardViewModel.observerUpNextAppointment.removeObservers(this)
        dashboardViewModel.observerWorkOrderListResponse.removeObservers(this)

        val removeTask = fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        removeTask.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.d(tag, "Location Callback removed.")
            } else {
                Log.d(tag, "Failed to remove Location Callback.")
            }
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when (buttonView?.id) {
            R.id.cb_selection -> {
                if (isChecked) {
                    AppConstants.checkedAppointmentIds.add(buttonView.tag.toString())
                } else {
                    AppConstants.checkedAppointmentIds.remove(buttonView.tag.toString())
                }
            }
        }
    }
}