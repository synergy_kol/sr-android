package com.met.ahvara.ui.customer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.adapter.AdapterCustomerDetailsAddress
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.CustomerDetailsBinding
import com.met.ahvara.model.CustomerDetailsData
import com.met.ahvara.model.CustomerDetailsRequest
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.AppConstants.Companion.INTENT_CUSTOMER_ID
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.CustomerDetailsViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class CustomerDetailsActivity : AppCompatActivity() {
    private lateinit var binding: CustomerDetailsBinding
    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private val tag = "CustomerDetailsActivity"
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var customerDetailsViewModel: CustomerDetailsViewModel
    private lateinit var stCustomerID: String
    private lateinit var adapterCustomerDetailsAddress: AdapterCustomerDetailsAddress

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        binding = CustomerDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        stCustomerID = intent.getStringExtra(INTENT_CUSTOMER_ID) ?: "0"
        adapterCustomerDetailsAddress = AdapterCustomerDetailsAddress(mContext)

        initViewModel()
        initClickListener()
    }

    private fun initClickListener() {
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }

        binding.cvHistory.setOnClickListener {
            val intent = Intent(this, CustomerHistoryActivity::class.java)
            startActivity(intent)
        }

        binding.cvDocuments.setOnClickListener {
            val intent = Intent(this, CustomerDocumentsActivity::class.java)
            startActivity(intent)
        }

        binding.cvUnits.setOnClickListener {
            val intent = Intent(this, CustomerUnitsActivity::class.java)
            startActivity(intent)
        }

        binding.cvWork.setOnClickListener {
            val intent = Intent(this, CustomerWorkOrdersActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initViewModel() {
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this, {
            loginResponse = it
            //setDataFromLogin()
            fetchDataFromWeb()
        })

        customerDetailsViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[CustomerDetailsViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        customerDetailsViewModel.customerDetailsResponse.observe(this, {
            if (it.status?.errorCode ?: 1000 == 0) {
                setCustomerData(it.result!!.data)

            } else {
                Snackbar.make(
                    mContext,
                    binding.cvDocuments,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        })

        //invoked when a network exception occurred
        customerDetailsViewModel.errorMessage.observe(this, {
            Log.d(tag, "errorMessage: $it")
        })
    }

    private fun setCustomerData(customerDetailsData: CustomerDetailsData) {
        val layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        binding.rvAddress.layoutManager = layoutManager
        binding.rvAddress.adapter = adapterCustomerDetailsAddress

        if (customerDetailsData.contactDetails.size > 0)
            adapterCustomerDetailsAddress.putData(customerDetailsData)

        binding.tvName.text = customerDetailsData.customerName
        binding.tvContactNumber.text = customerDetailsData.contact
        binding.tvEmail.text = customerDetailsData.email
        binding.tvAddress.text = customerDetailsData.address
        binding.tvCategoriesValue.text = customerDetailsData.category
        binding.tvLeadDealStatusValue.text = customerDetailsData.category

    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            val customerDetailsRequest = CustomerDetailsRequest()
            customerDetailsRequest.customer_id = stCustomerID

            val requestString: String =
                CommonUtils.getJsonFromObject(
                    customerDetailsRequest,
                    CustomerDetailsRequest::class.java
                )

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody = requestString.toRequestBody("application/json".toMediaTypeOrNull())

            customerDetailsViewModel.getCustomerDetailsResponse(loginResponse.token, requestBody)

        } else {
            Snackbar.make(
                mContext,
                binding.cvDocuments,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

}