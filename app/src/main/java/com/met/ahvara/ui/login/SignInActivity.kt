package com.met.ahvara.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.database.AhvaraDatabase
import com.met.ahvara.databinding.SignInBinding
import com.met.ahvara.model.LoginRequest
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.AppConstants.Companion.TEXT_SOURCE
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.utils.subscribeOnBackground
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class SignInActivity : AppCompatActivity() {
    private lateinit var binding: SignInBinding

    private val tag = "SignInActivity"
    lateinit var viewModel: LoginViewModel
    private lateinit var retrofitService: RetrofitService
    private lateinit var mContext: Context
    private lateinit var database: AhvaraDatabase
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        binding = SignInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        progressDialog = CommonUtils.getProgressDialog(mContext)
        database = AhvaraDatabase.getInstance(mContext)
        retrofitService = ApiClient.getInstance()

        initClickListener()
        initViewModel()
    }

    private fun initViewModel() {
        //get viewmodel instance using ViewModelProvider.Factory
        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        viewModel.loginResponse.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()

            if (it.status.errorCode ?: 1 == 0) {

                subscribeOnBackground {
                    viewModel.deleteAllLogin()
                    viewModel.insert(it)
                }

                //  finish()
            } else {
                Snackbar.make(
                    mContext,
                    binding.cvSubmit,
                    it.status.message ?: getString(R.string.api_call_error),
                    LENGTH_SHORT
                ).show()
            }
        }

        viewModel.getAllLogin().observe(this) {
            if (it != null) {
                val intent = Intent(mContext, StartYourDayActivity::class.java)
                startActivity(intent)
            }
        }

        //invoked when a network exception occurred
        viewModel.errorMessage.observe(this) {
            Log.d(tag, "errorMessage: $it")
        }
    }

    private fun initClickListener() {
        binding.tvForgotPassword.setOnClickListener {
            val intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }

        binding.cvSubmit.setOnClickListener {
            validateFields()
        }
    }

    private fun validateFields() {
        if (CommonUtils.isValidEmail(binding.etEmail.text)) {
            if (binding.etPassword.text?.length ?: 0 > 7) {
                if (CommonUtils.isInternetAvailable(mContext)) {
                    progressDialog.show()
                    requestLogin()
                } else {
                    Snackbar.make(
                        mContext,
                        binding.cvSubmit,
                        getString(R.string.internet_connection),
                        LENGTH_SHORT
                    ).show()
                }
            } else {
                binding.etPassword.error = getString(R.string.enter_password)
            }
        } else {
            binding.etEmail.error = getString(R.string.enter_valid_email)
        }
    }

    private fun requestLogin() {
        val loginRequest = LoginRequest()
        loginRequest.deviceToken = "Test"
        loginRequest.deviceType = AppConstants.TEXT_DEVICE_TYPE
        loginRequest.email = binding.etEmail.text.toString()
        loginRequest.password = binding.etPassword.text.toString()
        loginRequest.source = TEXT_SOURCE

        val requestString: String =
            CommonUtils.getJsonFromObject(loginRequest, LoginRequest::class.java)

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = requestString.toRequestBody("application/json".toMediaTypeOrNull())

        viewModel.getLoginResponse(requestBody)
    }
}