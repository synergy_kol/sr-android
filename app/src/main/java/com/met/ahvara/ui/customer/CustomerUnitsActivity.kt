package com.met.ahvara.ui.customer

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.CustomerUnitsBinding

class CustomerUnitsActivity : AppCompatActivity() {

    private lateinit var binding: CustomerUnitsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = CustomerUnitsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.cv01.setOnClickListener {

            val intent = Intent(this, CustomerUnitDetailsActivity::class.java)
            startActivity(intent)
        }
    }
}