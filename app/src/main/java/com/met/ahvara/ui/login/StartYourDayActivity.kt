package com.met.ahvara.ui.login

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.*
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.StartYourDayBinding
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.model.SubmitCheckinRequest
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.dashboard.DashboardActivity
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.utils.subscribeOnBackground
import com.met.ahvara.viewmodel.DashboardViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.vmadalin.easypermissions.EasyPermissions
import com.vmadalin.easypermissions.dialogs.DEFAULT_SETTINGS_REQ_CODE
import com.vmadalin.easypermissions.dialogs.SettingsDialog
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

class StartYourDayActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks,
    EasyPermissions.RationaleCallbacks {
    private lateinit var submitCheckinRequest: SubmitCheckinRequest
    private var locationFlag: Boolean = false
    private lateinit var binding: StartYourDayBinding
    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var viewModel: DashboardViewModel
    private lateinit var progressDialog: AlertDialog

    //location
    private val RC_LOCATION_PERM = 124
    private var latitude by Delegates.notNull<Double>()
    private var longitude by Delegates.notNull<Double>()

    // FusedLocationProviderClient - Main class for receiving location updates.
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    // LocationRequest - Requirements for the location updates, i.e.,
    // how often you should receive updates, the priority, etc.
    private lateinit var locationRequest: LocationRequest

    // LocationCallback - Called when FusedLocationProviderClient
    // has a new Location
    private lateinit var locationCallback: LocationCallback

    // This will store current location info
    private lateinit var currentLocation: Location
    //location

    private val tag = StartYourDayActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)

        binding = StartYourDayBinding.inflate(layoutInflater)
        setContentView(binding.root)
        progressDialog = CommonUtils.getProgressDialog(mContext)

        initViewModel()
        locationTask()
        initClickListener()
    }

    private fun initClickListener() {
        binding.cvStartYourDay.setOnClickListener {
            if (locationFlag) {
                fetchDataFromWeb()
            } else {
                Snackbar.make(
                    mContext,
                    binding.cvStartYourDay,
                    getString(R.string.fetching_lat_long),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        binding.tvSkip.setOnClickListener {
            if (locationFlag) {
                fetchDataFromWeb()
            } else {
                Snackbar.make(
                    mContext,
                    binding.cvStartYourDay,
                    getString(R.string.fetching_lat_long),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }

        }
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
        }

        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[DashboardViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        viewModel.observerSubmitCheckinResponse.observe(this, Observer {

            Snackbar.make(
                mContext,
                binding.cvStartYourDay,
                it.status?.message ?: getString(R.string.api_call_error),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()

            if (it.status?.errorCode ?: 1000 == 0) {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()

                subscribeOnBackground {
                    viewModel.deleteAllCheckin()
                    viewModel.insertCheckin(submitCheckinRequest)
                }
            }
        })

        viewModel.observerSubmitCheckinResponse.observe(this) {
            if (it != null) {
                val intent = Intent(this, DashboardActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

        //invoked when a network exception occurred
        viewModel.observerErrorMessage.observe(this, Observer {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")
        })
    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()

            val stCheckinTime = CommonUtils.getCurrentTimeHHmm()
            val stCheckinTimeStamp = System.currentTimeMillis().toString()

            submitCheckinRequest =
                SubmitCheckinRequest(
                    stCheckinTime,
                    latitude.toString(),
                    longitude.toString(),
                    stCheckinTimeStamp
                )

            val requestString: String =
                CommonUtils.getJsonFromObject(
                    submitCheckinRequest,
                    SubmitCheckinRequest::class.java
                )

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody =
                requestString.toRequestBody("application/json".toMediaTypeOrNull())

            viewModel.getSubmitCheckinResponse(loginResponse.token, requestBody)

        } else {
            Snackbar.make(
                mContext,
                binding.cvStartYourDay,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    private fun locationTask() {

        // Have permission, do the thing!
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create().apply {
            // Sets the desired interval for
            // active location updates.
            // This interval is inexact.
            interval = TimeUnit.SECONDS.toMillis(3)

            // Sets the fastest rate for active location updates.
            // This interval is exact, and your application will never
            // receive updates more frequently than this value
            fastestInterval = TimeUnit.SECONDS.toMillis(5)

            // Sets the maximum time when batched location
            // updates are delivered. Updates may be
            // delivered sooner than this interval
            maxWaitTime = TimeUnit.SECONDS.toMillis(10)

            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                locationResult.lastLocation.let {

                    locationFlag = true
                    currentLocation = it
                    latitude = currentLocation.latitude
                    longitude = currentLocation.longitude
                    // use latitude and longitude as per your need
                }
            }
        }

        if (hasLocationPermissions()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.rationale_location),
                    RC_LOCATION_PERM,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                return
            }
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()!!
            )

            fusedLocationProviderClient.lastLocation.addOnSuccessListener { location: Location? ->
                // Got last known location. In some rare situations this can be null.

                locationFlag = true
                currentLocation = location!!
                latitude = currentLocation.latitude
                longitude = currentLocation.longitude
            }
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_location),
                RC_LOCATION_PERM,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    private fun hasLocationPermissions(): Boolean {
        return EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        Log.d(tag, "onPermissionsGranted:" + requestCode + ":" + perms.size)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest, locationCallback,
            Looper.myLooper()!!
        )

    }

    override fun onRationaleAccepted(requestCode: Int) {
        Log.d(tag, "onRationaleAccepted:$requestCode")
    }

    override fun onRationaleDenied(requestCode: Int) {
        Log.d(tag, "onRationaleDenied:$requestCode")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Log.d(tag, "onPermissionsDenied:" + requestCode + ":" + perms.size)

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            SettingsDialog.Builder(this).build().show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DEFAULT_SETTINGS_REQ_CODE) {
            val yes = getString(R.string.yes)
            val no = getString(R.string.no)
            // Do something after user returned from app settings screen, like showing a Toast.
            Toast.makeText(
                this,
                getString(
                    R.string.returned_from_app_settings_to_activity,
                    if (hasLocationPermissions()) yes else no,
                ),
                Toast.LENGTH_LONG
            )
                .show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        val removeTask = fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        removeTask.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.d(tag, "Location Callback removed.")
            } else {
                Log.d(tag, "Failed to remove Location Callback.")
            }
        }
    }

}