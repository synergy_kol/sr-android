package com.met.ahvara.ui.units

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.UnitDetailsBinding

class UnitDetailsActivity : AppCompatActivity() {

    private lateinit var binding: UnitDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = UnitDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}