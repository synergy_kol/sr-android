package com.met.ahvara.ui.customer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.CustomerDocumentsBinding

class CustomerDocumentsActivity : AppCompatActivity() {

    private lateinit var binding: CustomerDocumentsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = CustomerDocumentsBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}