package com.met.ahvara.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.PasswordUpdatedBinding

class PasswordUpdatedActivity : AppCompatActivity() {

    private lateinit var binding: PasswordUpdatedBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = PasswordUpdatedBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.cvLoginNow.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }
    }
}