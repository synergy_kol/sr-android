package com.met.ahvara.ui.task

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.CompoundButton
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.adapter.AdapterTaskList
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.ContentMenuHeaderBinding
import com.met.ahvara.databinding.TaskActivityBinding
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.model.TaskListSearchRequest
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.customer.CustomerDetailsActivity
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.DashboardViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.squareup.picasso.Picasso
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class TaskListActivity : AppCompatActivity(), View.OnClickListener,
    NavigationView.OnNavigationItemSelectedListener, CompoundButton.OnCheckedChangeListener {
    private lateinit var binding: TaskActivityBinding

    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private lateinit var adapterTaskList: AdapterTaskList
    private val tag = "TaskActivity"
    lateinit var viewModel: DashboardViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)

        binding = TaskActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        progressDialog = CommonUtils.getProgressDialog(mContext)

        initViewModel()
        initViews()
        initClickListener()
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            setDataFromLogin()
            fetchDataFromWeb()
        }

        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[DashboardViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        viewModel.observerTasksListResponse.observe(this) {
            if (it.status?.errorCode ?: 1000 == 0) {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                if (it.result!!.data.size > 0) {
                    adapterTaskList.putData(it.result!!.data)
                    binding.taskContent.tvNoData.visibility = View.GONE
                } else {
                    binding.taskContent.tvNoData.visibility = View.VISIBLE
                }
            } else {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                binding.taskContent.tvNoData.visibility = View.VISIBLE
                Snackbar.make(
                    mContext,
                    binding.taskDrawer,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        viewModel.observerErrorMessage.observe(this) {
            Log.d(tag, "errorMessage: $it")
            if (progressDialog.isShowing)
                progressDialog.dismiss()

            adapterTaskList.clearList()
            binding.taskContent.tvNoData.visibility = View.VISIBLE
        }
    }

    private fun initViews() {
        binding.taskNavigationView.itemIconTintList = null
        binding.taskNavigationView.setNavigationItemSelectedListener(this)

        binding.taskContent.svTasks.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.isNotEmpty()) {
                    searchTasks(query)
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                if (query!!.isEmpty()) {
                    fetchDataFromWeb()
                }
                return false
            }
        })

        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)

        binding.taskContent.rvTasks.addItemDecoration(dividerItemDecoration)
        binding.taskContent.rvTasks.layoutManager = linearLayoutManager
        adapterTaskList = AdapterTaskList(mContext, this, this)
        binding.taskContent.rvTasks.adapter = adapterTaskList
    }

    private fun searchTasks(query: String) {
        if (CommonUtils.isInternetAvailable(mContext)) {
            val searchRequest = TaskListSearchRequest()
            searchRequest.search_string = query
            val requestString: String =
                CommonUtils.getJsonFromObject(searchRequest, TaskListSearchRequest::class.java)

            viewModel.getTaskSearchListResponse(
                loginResponse.token,
                requestString.toRequestBody("application/json".toMediaTypeOrNull())
            )

        } else {
            Snackbar.make(
                mContext,
                binding.taskDrawer,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }


    private fun initClickListener() {
        if (binding.taskDrawer.isDrawerOpen(GravityCompat.START)) {
            binding.taskDrawer.closeDrawer(GravityCompat.START)
        }
        binding.taskContent.includeTb.ivMenu.setOnClickListener {
            if (!binding.taskDrawer.isDrawerOpen(GravityCompat.START)) {
                binding.taskDrawer.openDrawer(GravityCompat.START)
            }
        }
        binding.taskContent.taskAdd.setOnClickListener {
            val intent = Intent(this, TaskAddActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setDataFromLogin() {
        val loginResponseData = loginResponse.result.data
        // navView is NavigationView
        val viewHeader = binding.taskNavigationView.getHeaderView(0)
        // nav_header.xml is headerLayout
        val navViewHeaderBinding: ContentMenuHeaderBinding =
            ContentMenuHeaderBinding.bind(viewHeader)
        // title is Children of nav_header
        navViewHeaderBinding.profileName.text =
            getString(
                R.string.dashboard_nav_name,
                loginResponseData.firstName,
                loginResponseData.lastName
            )

        Picasso.get()
            .load(loginResponseData.profileImage)
            .placeholder(R.drawable.ic_baseline_person_24)
            .error(R.drawable.ic_baseline_person_24)
            .into(navViewHeaderBinding.menuDp)

        binding.taskContent.includeTb.tvTitle.text = getString(R.string.task)

        //for st sr
        val navigationMenu = binding.taskNavigationView.menu
        if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_ST) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = false
        } else if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_SR) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = true
        }
        //for st sr
    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()

            viewModel.getTaskListResponse(loginResponse.token)

        } else {
            Snackbar.make(
                mContext,
                binding.taskDrawer,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        CommonUtils.navigateNavigationDrawer(
            mContext, item, this,
            loginViewModel,
            null
        )
        return true
    }

    override fun onClick(view: View?) {
        if (view!!.id == R.id.ll_demo) {
            val intent = Intent(mContext, CustomerDetailsActivity::class.java)
            intent.putExtra(AppConstants.INTENT_CUSTOMER_ID, view.tag.toString())
            startActivity(intent)
        }
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        TODO("Not yet implemented")
    }

}