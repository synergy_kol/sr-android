package com.met.ahvara.ui.leads

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.DialogCancelLostBinding
import com.met.ahvara.databinding.LeadsDetailsBinding
import com.met.ahvara.model.LeadsDetailsData
import com.met.ahvara.model.LeadsDetailsRequest
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.notifications.NotificationsActivity
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.LeadsDetailsViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class LeadDetailsActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: LeadsDetailsBinding

    private lateinit var stLeadId: String
    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse
    private val tag = "LeadDetailsActivity"
    lateinit var viewModel: LeadsDetailsViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog
    private lateinit var leadDetailsData: LeadsDetailsData


    private lateinit var popup: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        progressDialog = CommonUtils.getProgressDialog(mContext)
        binding = LeadsDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        stLeadId = intent?.getStringExtra(AppConstants.INTENT_LEAD_ID).toString()

        initViewModel()
        initViews()
        initClickListener()

        binding.tvCancel.setOnClickListener {
            showCancel()
        }
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            setDataFromLogin()
            fetchDataFromWeb()
        }

        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LeadsDetailsViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        viewModel.leadsDetailsResponse.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()

            if (it?.status?.errorCode ?: 1000 == 0) {
                if (it.result?.data != null) {
                    leadDetailsData = it?.result!!.data
                    setData()
                }
            } else {
                Snackbar.make(
                    mContext,
                    binding.cvSubmit,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        viewModel.errorMessage.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")
        }
    }

    private fun setData() {

    }

    private fun initClickListener() {

        /*      binding.leadsContent.customerAdd.setOnClickListener {
                  val intent = Intent(mContext, CustomerAddActivity::class.java)
                  startActivity(intent)
              }*/
        binding.ivNotification.setOnClickListener {
            val intent = Intent(mContext, NotificationsActivity::class.java)
            mContext.startActivity(intent)
        }
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setDataFromLogin() {
        val loginResponseData = loginResponse.result.data
        // navView is NavigationView
        /*    val viewHeader = binding.leadsNavigationView.getHeaderView(0)
            // nav_header.xml is headerLayout
            val navViewHeaderBinding: ContentMenuHeaderBinding =
                ContentMenuHeaderBinding.bind(viewHeader)
            // title is Children of nav_header
            navViewHeaderBinding.profileName.text =
                getString(
                    R.string.dashboard_nav_name,
                    loginResponseData.firstName,
                    loginResponseData.lastName
                )

            Picasso.get()
                .load(loginResponseData.profileImage)
                .placeholder(R.drawable.ic_baseline_person_24)
                .error(R.drawable.ic_baseline_person_24)
                .into(navViewHeaderBinding.menuDp)

            binding.leadsContent.includeTb.tvTitle.text = getString(R.string.menu_leads)
    */
        //for st sr
        //  val navigationMenu = binding.leadsNavigationView.menu
        if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_ST) {
            //   navigationMenu.findItem(R.id.drawer_leads).isVisible = false
        } else if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_SR) {
            //      navigationMenu.findItem(R.id.drawer_leads).isVisible = true
        }
        //for st sr
    }

    private fun initViews() {

    }

    private fun showCancel() {
        val dialogBinding = DialogCancelLostBinding.inflate(layoutInflater)

        val builder = AlertDialog.Builder(this, R.style.popupDialog)

        builder.setView(dialogBinding.root)

        dialogBinding.etDescription.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                dialogBinding.tvCount.text = mContext.getString(R.string.out_of_500, s?.length)

            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        dialogBinding.noteCancel.setOnClickListener {
            if (popup.isShowing) {
                popup.dismiss()
            }
        }

        dialogBinding.noteProceed.setOnClickListener {
            if (popup.isShowing) {
                popup.dismiss()
            }
        }

        popup = builder.create()
        popup.window?.setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        ); builder.setCancelable(false)
        popup.show()
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.llTop -> {
                val intent = Intent(mContext, LeadDetailsActivity::class.java)
                intent.putExtra(AppConstants.INTENT_LEAD_ID, view.tag.toString())
                startActivity(intent)
            }
            R.id.btCancelLost -> {
                Log.e("btCancelLost", "tasks--" + Throwable().stackTrace[0].lineNumber)
            }
            R.id.btConvert -> {
                Log.e("btConvert", "tasks--" + Throwable().stackTrace[0].lineNumber)
            }
        }
    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            progressDialog.show()

            val request = LeadsDetailsRequest()
            request.lead_id = stLeadId

            val requestString: String =
                CommonUtils.getJsonFromObject(request, LeadsDetailsRequest::class.java)

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody = requestString.toRequestBody("application/json".toMediaTypeOrNull())

            viewModel.getLeadsDetailsResponse(loginResponse.token, requestBody)

        } else {
            Snackbar.make(
                mContext,
                binding.cvSubmit,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }
}