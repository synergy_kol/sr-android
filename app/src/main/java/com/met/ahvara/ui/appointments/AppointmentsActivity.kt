package com.met.ahvara.ui.appointments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.kizitonwose.calendarview.utils.Size
import com.met.ahvara.R
import com.met.ahvara.adapter.AdapterAppointments
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.databinding.AppointmentsActivityBinding
import com.met.ahvara.databinding.ContentMenuHeaderBinding
import com.met.ahvara.databinding.Example7CalendarDayBinding
import com.met.ahvara.model.AppointmentListListdata
import com.met.ahvara.model.AppointmentListRequest
import com.met.ahvara.model.LoginResponse
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.DashboardViewModel
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import com.squareup.picasso.Picasso
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter

class AppointmentsActivity : AppCompatActivity(), View.OnClickListener,
    NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: AppointmentsActivityBinding
    private lateinit var adapterAppointmentList: AdapterAppointments

    private lateinit var stCurrentDate: String

    private lateinit var popupStatus: AlertDialog
    private lateinit var popupNote: AlertDialog
    private lateinit var popupRate: AlertDialog

    private lateinit var mContext: Context
    lateinit var loginResponse: LoginResponse

    private val tag = "AppointmentsActivity"
    lateinit var dashboardViewModel: DashboardViewModel
    private val retrofitService: RetrofitService = ApiClient.getInstance()
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var progressDialog: AlertDialog

    //weekview
    private var selectedDate = LocalDate.now()
    private val dateFormatter = DateTimeFormatter.ofPattern("dd")
    private val dayFormatter = DateTimeFormatter.ofPattern("EEE")
    private val monthFormatter = DateTimeFormatter.ofPattern("MMM")
    //weekview

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)

        binding = AppointmentsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        progressDialog = CommonUtils.getProgressDialog(mContext)


        /*binding.ivBack.setOnClickListener {
            finish()
        }

        binding.fabMap.setOnClickListener {
            val intent = Intent(this, RoutePlannerActivity::class.java)
            startActivity(intent)
        }

        binding.cvStatus.setOnClickListener {
            showStatus()
        }

        binding.cvNote.setOnClickListener {
            showAddNote()
        }

        binding.cvRate.setOnClickListener {
            showRate()
        }

        val supportMapFragment =
            supportFragmentManager.findFragmentById(R.id.map)
                    as SupportMapFragment

        supportMapFragment.getMapAsync(this)*/

        initViewModel()
        initViews()
        initClickListener()
    }

    private fun initViewModel() {  //get viewmodel instance using ViewModelProvider.Factory
        loginViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[LoginViewModel::class.java]

        loginViewModel.getAllLogin().observe(this) {
            loginResponse = it
            setDataFromLogin()
            fetchDataFromWeb()
        }

        dashboardViewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[DashboardViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        dashboardViewModel.observerAppointmentListResponse.observe(this) {
            if (it.status?.errorCode ?: 1000 == 0) {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                if (it.result?.data!!.listdata.size > 0) {
                    adapterAppointmentList.putData(it.result?.data!!.listdata)
                    binding.appointmentsContent.tvNoData.visibility = View.GONE
                } else {
                    binding.appointmentsContent.tvNoData.visibility = View.VISIBLE
                }

            } else {
                binding.appointmentsContent.tvNoData.visibility = View.VISIBLE
                Snackbar.make(
                    mContext,
                    binding.appointmentsDrawer,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        dashboardViewModel.observerErrorMessage.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            Log.d(tag, "errorMessage: $it")
            adapterAppointmentList.clearList()
            binding.appointmentsContent.tvNoData.visibility = View.VISIBLE
        }
    }

    private fun initViews() {
        val date = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        stCurrentDate = date.format(formatter)

        binding.appointmentsContent.includeTb.tvTitle.text = getString(R.string.appointments)

        binding.appointmentsNavigationView.itemIconTintList = null
        binding.appointmentsNavigationView.setNavigationItemSelectedListener(this)

        binding.appointmentsContent.svAppointments.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.isNotEmpty()) {
                    searchAppointments(query)
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                if (query!!.isEmpty()) {
                    fetchDataFromWeb()
                }
                return false
            }
        })

        val linearLayoutManager = LinearLayoutManager(mContext)
        val dividerItemDecoration = DividerItemDecoration(mContext, linearLayoutManager.orientation)

        binding.appointmentsContent.rvAppointments.addItemDecoration(dividerItemDecoration)
        binding.appointmentsContent.rvAppointments.layoutManager = linearLayoutManager
        adapterAppointmentList = AdapterAppointments(mContext, this)
        binding.appointmentsContent.rvAppointments.adapter = adapterAppointmentList

        val dm = DisplayMetrics()
        val wm = mContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.defaultDisplay.getMetrics(dm)
        binding.appointmentsContent.exSevenCalendar.apply {
            val dayWidth = dm.widthPixels / 6
            val dayHeight = (dayWidth * 1.35).toInt()
            daySize = Size(dayWidth, dayHeight)
        }

        class DayViewContainer(view: View) : ViewContainer(view) {
            val bind = Example7CalendarDayBinding.bind(view)
            lateinit var day: CalendarDay

            init {
                view.setOnClickListener {
                    val firstDay = binding.appointmentsContent.exSevenCalendar.findFirstVisibleDay()
                    val lastDay = binding.appointmentsContent.exSevenCalendar.findLastVisibleDay()
                    if (firstDay == day) {
                        // If the first date on screen was clicked, we scroll to the date to ensure
                        // it is fully visible if it was partially off the screen when clicked.
                        binding.appointmentsContent.exSevenCalendar.smoothScrollToDate(day.date)
                    } else if (lastDay == day) {
                        // If the last date was clicked, we scroll to 4 days ago, this forces the
                        // clicked date to be fully visible if it was partially off the screen.
                        // We scroll to 4 days ago because we show max of five days on the screen
                        // so scrolling to 4 days ago brings the clicked date into full visibility
                        // at the end of the calendar view.
                        binding.appointmentsContent.exSevenCalendar.smoothScrollToDate(
                            day.date.minusDays(
                                4
                            )
                        )
                    }

                    // Example: If you want the clicked date to always be centered on the screen,
                    // you would use: exSevenCalendar.smoothScrollToDate(day.date.minusDays(2))

                    if (selectedDate != day.date) {
                        val oldDate = selectedDate
                        selectedDate = day.date
                        binding.appointmentsContent.exSevenCalendar.notifyDateChanged(day.date)
                        oldDate?.let {
                            binding.appointmentsContent.exSevenCalendar.notifyDateChanged(
                                it
                            )
                        }
                        stCurrentDate = selectedDate.toString()
                        fetchDataFromWeb()
                        Log.e(tag, selectedDate.toString())
                    }
                }
            }

            fun bind(day: CalendarDay) {
                this.day = day
                bind.exSevenDateText.text = dateFormatter.format(day.date)
                bind.exSevenDayText.text = dayFormatter.format(day.date)
                bind.exSevenMonthText.text = monthFormatter.format(day.date)

                bind.exSevenDateText.setTextColor(view.context.getColor(if (day.date == selectedDate) R.color.yellow else R.color.color_dark_grey))
                bind.exSevenSelectedView.isVisible = day.date == selectedDate
            }
        }

        binding.appointmentsContent.exSevenCalendar.dayBinder =
            object : DayBinder<DayViewContainer> {
                override fun create(view: View) = DayViewContainer(view)
                override fun bind(container: DayViewContainer, day: CalendarDay) =
                    container.bind(day)
            }

        val currentMonth = YearMonth.now()
        // Value for firstDayOfWeek does not matter since inDates and outDates are not generated.
        binding.appointmentsContent.exSevenCalendar.setup(
            currentMonth,
            currentMonth.plusMonths(2),
            DayOfWeek.values().random()
        )
        binding.appointmentsContent.exSevenCalendar.scrollToDate(LocalDate.now())
    }

    private fun searchAppointments(query: String) {
        /* if (CommonUtils.isInternetAvailable(mContext)) {
             val searchRequest = CustomerListSearchRequest()
             searchRequest.search_string = query
             val requestString: String =
                 CommonUtils.getJsonFromObject(searchRequest, CustomerListSearchRequest::class.java)

             dashboardViewModel.getCustomerSearchListResponse(
                 loginResponse.token,
                 requestString.toRequestBody("application/json".toMediaTypeOrNull())
             )

         } else {
             Snackbar.make(
                 mContext,
                 binding.appointmentsDrawer,
                 getString(R.string.internet_connection),
                 BaseTransientBottomBar.LENGTH_SHORT
             ).show()
         }*/
    }

    private fun initClickListener() {

        binding.appointmentsContent.includeTb.ivMenu.setOnClickListener {
            if (!binding.appointmentsDrawer.isDrawerOpen(GravityCompat.START)) {
                binding.appointmentsDrawer.openDrawer(GravityCompat.START)
            }
        }
        binding.appointmentsContent.fabAdd.setOnClickListener {
            /*   val intent = Intent(mContext, CustomerAddActivity::class.java)
               startActivity(intent)*/
        }
    }

    private fun showStatus() {

        val inflater = layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_appointment_status, null)

        val cancel = view.findViewById<RelativeLayout>(R.id.status_cancel)
        val proceed = view.findViewById<RelativeLayout>(R.id.status_proceed)

        val builder = AlertDialog.Builder(this, R.style.popupDialog)
        builder.setCancelable(false)
        builder.setView(view)

        cancel.setOnClickListener {
            if (popupStatus.isShowing) {
                popupStatus.dismiss()
            }
        }

        proceed.setOnClickListener {
            if (popupStatus.isShowing) {
                popupStatus.dismiss()
            }
        }

        popupStatus = builder.create()
        popupStatus.show()
    }

    private fun showAddNote() {

        val inflater = layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_add_note, null)

        val cancel = view.findViewById<RelativeLayout>(R.id.note_cancel)
        val proceed = view.findViewById<RelativeLayout>(R.id.note_proceed)

        val builder = AlertDialog.Builder(this, R.style.popupDialog)
        builder.setCancelable(false)
        builder.setView(view)

        cancel.setOnClickListener {
            if (popupNote.isShowing) {
                popupNote.dismiss()
            }
        }

        proceed.setOnClickListener {
            if (popupNote.isShowing) {
                popupNote.dismiss()
            }
        }

        popupNote = builder.create()
        popupNote.show()
    }

    private fun showRate() {

        val inflater = layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_rate, null)

        val cancel = view.findViewById<RelativeLayout>(R.id.rate_cancel)
        val proceed = view.findViewById<RelativeLayout>(R.id.rate_proceed)

        val builder = AlertDialog.Builder(this, R.style.popupDialog)
        builder.setCancelable(false)
        builder.setView(view)

        cancel.setOnClickListener {
            if (popupRate.isShowing) {
                popupRate.dismiss()
            }
        }

        proceed.setOnClickListener {
            if (popupRate.isShowing) {
                popupRate.dismiss()
            }
        }

        popupRate = builder.create()
        popupRate.show()
    }

    private fun setDataFromLogin() {
        val loginResponseData = loginResponse.result.data
        // navView is NavigationView
        val viewHeader = binding.appointmentsNavigationView.getHeaderView(0)
        // nav_header.xml is headerLayout
        val navViewHeaderBinding: ContentMenuHeaderBinding =
            ContentMenuHeaderBinding.bind(viewHeader)
        // title is Children of nav_header
        navViewHeaderBinding.profileName.text =
            getString(
                R.string.dashboard_nav_name,
                loginResponseData.firstName,
                loginResponseData.lastName
            )

        Picasso.get()
            .load(loginResponseData.profileImage)
            .placeholder(R.drawable.ic_baseline_person_24)
            .error(R.drawable.ic_baseline_person_24)
            .into(navViewHeaderBinding.menuDp)


        //for st sr
        val navigationMenu = binding.appointmentsNavigationView.menu
        if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_ST) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = false
        } else if (loginResponseData.roleId!! == AppConstants.LOGIN_TYPE_SR) {
            navigationMenu.findItem(R.id.drawer_leads).isVisible = true

        }
        //for st sr

    }

    private fun fetchDataFromWeb() {
        if (CommonUtils.isInternetAvailable(mContext)) {
            if (!progressDialog.isShowing)
                progressDialog.show()
            val request = AppointmentListRequest()

            request.date = stCurrentDate
            request.filter_by = AppConstants.APPOINTMENT_FILTER_BY_DATE

            val requestString: String =
                CommonUtils.getJsonFromObject(request, AppointmentListRequest::class.java)

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody =
                requestString.toRequestBody("application/json".toMediaTypeOrNull())

            dashboardViewModel.getAppointmentListResponse(loginResponse.token, requestBody)

        } else {
            Snackbar.make(
                mContext,
                binding.appointmentsDrawer,
                getString(R.string.internet_connection),
                BaseTransientBottomBar.LENGTH_SHORT
            ).show()
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.cl_arrive_top -> {
                val intent = Intent(mContext, AppointmentDetailsArrivedActivity::class.java)
                intent.putExtra(AppConstants.INTENT_APPOINTMENT_DETAILS, view.tag as AppointmentListListdata)
                startActivity(intent)
            }
            R.id.cl_completed_top -> {
                val intent = Intent(mContext, AppointmentDetailsCompletedActivity::class.java)
                intent.putExtra(AppConstants.INTENT_APPOINTMENT_DETAILS, view.tag as AppointmentListListdata)
                startActivity(intent)
            }
            R.id.cl_rescheduled_top -> {
                val intent = Intent(mContext, AppointmentDetailsRescheduledActivity::class.java)
                intent.putExtra(AppConstants.INTENT_APPOINTMENT_DETAILS, view.tag as AppointmentListListdata)
                startActivity(intent)
            }
            R.id.clScheduledTop -> {
                val intent = Intent(mContext, AppointmentDetailsScheduledActivity::class.java)
                intent.putExtra(AppConstants.INTENT_APPOINTMENT_DETAILS, view.tag as AppointmentListListdata)
                startActivity(intent)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        CommonUtils.navigateNavigationDrawer(
            mContext, item, this,
            loginViewModel,
            dashboardViewModel
        )

        binding.appointmentsDrawer.closeDrawer(GravityCompat.START)

        return false
    }
}