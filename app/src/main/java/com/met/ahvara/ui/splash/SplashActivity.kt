package com.met.ahvara.ui.splash

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowInsets
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.ViewModelProvider
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.database.AhvaraDatabase
import com.met.ahvara.databinding.SplashBinding
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.ui.dashboard.DashboardActivity
import com.met.ahvara.ui.landing.LandingActivity
import com.met.ahvara.viewmodel.LoginViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: SplashBinding

    private lateinit var mContext: Context
    private lateinit var database: AhvaraDatabase
    lateinit var retrofitService: RetrofitService
    lateinit var myIntent: Intent


    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        val splashScreen = installSplashScreen()

        super.onCreate(savedInstanceState)
        database = AhvaraDatabase.getInstance(mContext)
        retrofitService = ApiClient.getInstance()

        binding = SplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

   /*     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }*/


        val loginViewModel: LoginViewModel = ViewModelProvider(
            this, MyViewModelFactory(
                MainRepository(retrofitService, application as AhvaraApplication),
                application as AhvaraApplication
            )
        )[LoginViewModel::class.java]


        // Set up an OnPreDrawListener to the root view.
        val content: View = findViewById(android.R.id.content)
        content.viewTreeObserver.addOnPreDrawListener(
            object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    var status = false
                    loginViewModel.getAllLogin().observe(mContext as SplashActivity) {
                        if (it != null) {
                            myIntent = Intent(mContext, DashboardActivity::class.java)
                            finishAffinity()
                        } else {
                            myIntent = Intent(mContext, LandingActivity::class.java)
                        }
                        content.viewTreeObserver.removeOnPreDrawListener(this)
                        status = true
                        startActivity(myIntent)
                    }
                    return status

                    /*   // Check if the initial data is ready.
                       return if (viewModel.isReady) {
                           // The content is ready; start drawing.
                           content.viewTreeObserver.removeOnPreDrawListener(this)
                           true
                       } else {
                           // The content is not ready; suspend.
                           false
                       }*/
                }
            }
        )


    }
}