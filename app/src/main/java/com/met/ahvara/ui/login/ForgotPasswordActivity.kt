package com.met.ahvara.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.met.ahvara.R
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.database.AhvaraDatabase
import com.met.ahvara.databinding.ForgotPasswordBinding
import com.met.ahvara.model.ForgotPasswordRequest
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils
import com.met.ahvara.viewmodel.ForgotPasswordViewModel
import com.met.ahvara.viewmodelfactory.MyViewModelFactory
import com.met.ahvara.webservice.ApiClient
import com.met.ahvara.webservice.RetrofitService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

class ForgotPasswordActivity : AppCompatActivity() {
    private lateinit var binding: ForgotPasswordBinding

    private val tag = "ForgotPasswordActivity"
    lateinit var viewModel: ForgotPasswordViewModel
    lateinit var retrofitService: RetrofitService
    private lateinit var mContext: Context
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        mContext = this
        super.onCreate(savedInstanceState)
        retrofitService = ApiClient.getInstance()
        binding = ForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        progressDialog = CommonUtils.getProgressDialog(mContext)

        initClickListener()
        initViewModel()
    }

    private fun initViewModel() {
        //get viewmodel instance using ViewModelProvider.Factory
        viewModel =
            ViewModelProvider(
                this, MyViewModelFactory(
                    MainRepository(retrofitService, application as AhvaraApplication),
                    application as AhvaraApplication
                )
            )[ForgotPasswordViewModel::class.java]

        //the observer will only receive events if the owner(activity) is in active state
        //invoked when List data changes
        viewModel.forgotPasswordResponse.observe(this) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            if (it.status?.errorCode ?: 1 == 0) {

                Snackbar.make(
                    mContext,
                    binding.cvSubmit,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()

                val intent = Intent(mContext, ResetPasswordActivity::class.java)
                intent.putExtra(AppConstants.INTENT_EMAIL_ID, binding.etEmail.text.toString())
                startActivity(intent)

            } else {

                Snackbar.make(
                    mContext,
                    binding.cvSubmit,
                    it.status?.message ?: getString(R.string.api_call_error),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        }

        //invoked when a network exception occurred
        viewModel.errorMessage.observe(this) {
            Log.d(tag, "errorMessage: $it")
        }
    }

    private fun initClickListener() {
        binding.cvSubmit.setOnClickListener {
            validateFields()
        }
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun validateFields() {
        if (CommonUtils.isValidEmail(binding.etEmail.text)) {
            if (CommonUtils.isInternetAvailable(mContext)) {
                progressDialog.show()
                requestLogin()
            } else {
                Snackbar.make(
                    mContext,
                    binding.cvSubmit,
                    getString(R.string.internet_connection),
                    BaseTransientBottomBar.LENGTH_SHORT
                ).show()
            }
        } else {
            binding.editEmail.isErrorEnabled = true
            binding.editEmail.error = getString(R.string.enter_valid_email)
            binding.etEmail.error = getString(R.string.enter_valid_email)
        }
    }

    private fun requestLogin() {
        val forgotPasswordRequest = ForgotPasswordRequest()
        forgotPasswordRequest.prefix = getString(R.string.request_prefix)
        forgotPasswordRequest.email = binding.etEmail.text.toString()

        val requestString: String =
            CommonUtils.getJsonFromObject(forgotPasswordRequest, ForgotPasswordRequest::class.java)

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody =
            requestString.toRequestBody("application/json".toMediaTypeOrNull())

        viewModel.getForgotPasswordResponse(requestBody)
    }
}