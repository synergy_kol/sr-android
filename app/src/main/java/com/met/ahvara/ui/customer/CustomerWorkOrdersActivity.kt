package com.met.ahvara.ui.customer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.met.ahvara.databinding.CustomerWorkOrdersBinding

class CustomerWorkOrdersActivity : AppCompatActivity() {

    private lateinit var binding: CustomerWorkOrdersBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = CustomerWorkOrdersBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}