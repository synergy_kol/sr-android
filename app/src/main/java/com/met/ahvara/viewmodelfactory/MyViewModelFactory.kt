package com.met.ahvara.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.met.ahvara.application.AhvaraApplication
import com.met.ahvara.repository.MainRepository
import com.met.ahvara.viewmodel.*

class MyViewModelFactory constructor(
    private val repository: MainRepository,
    private val ahvaraApplication: AhvaraApplication
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> {
                LoginViewModel(this.repository, ahvaraApplication) as T
            }
            modelClass.isAssignableFrom(ForgotPasswordViewModel::class.java) -> {
                ForgotPasswordViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(ResetPasswordViewModel::class.java) -> {
                ResetPasswordViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(DashboardViewModel::class.java) -> {
                DashboardViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(CustomerListingViewModel::class.java) -> {
                CustomerListingViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(CustomerDetailsViewModel::class.java) -> {
                CustomerDetailsViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(AddCustomerViewModel::class.java) -> {
                AddCustomerViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(WorkOrdersViewModel::class.java) -> {
                WorkOrdersViewModel(this.repository, ahvaraApplication) as T
            }
            modelClass.isAssignableFrom(AppointmentReasonsViewModel::class.java) -> {
                AppointmentReasonsViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(AppointmentCancellationViewModel::class.java) -> {
                AppointmentCancellationViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(AppointmentRescheduleViewModel::class.java) -> {
                AppointmentRescheduleViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(LeadsListingViewModel::class.java) -> {
                LeadsListingViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(LeadsDetailsViewModel::class.java) -> {
                LeadsDetailsViewModel(this.repository) as T
            }
            else -> throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}