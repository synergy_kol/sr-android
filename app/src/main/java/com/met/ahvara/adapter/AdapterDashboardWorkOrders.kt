package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.R
import com.met.ahvara.databinding.ListDashboardWorkOrdersBinding
import com.met.ahvara.model.WorkOrderListData
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils

class AdapterDashboardWorkOrders(
    private val mContext: Context, listOfWorkOrders: List<WorkOrderListData>?,
    listener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val workOrders: MutableList<WorkOrderListData>
    private val listener: View.OnClickListener

    init {
        workOrders = ArrayList()
        workOrders.addAll(listOfWorkOrders!!)
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListDashboardWorkOrdersBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DashboardWorkOrdersViewholder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val workOrdersListdata = workOrders[position]
        val workOrdersViewHolder = holder as DashboardWorkOrdersViewholder

        workOrdersViewHolder.binding.tvId.text = mContext.getString(
            R.string.hash_tag, workOrdersListdata.workId
        )
        workOrdersViewHolder.binding.tvCompanyName.text = workOrdersListdata.companyName
        workOrdersViewHolder.binding.tvMachineUnit.text = workOrdersListdata.productName

        workOrdersViewHolder.binding.tvDate.text =
            CommonUtils.getMonthDayYearFromYYYYMMDD(workOrdersListdata.expiryDate)

        when (workOrdersListdata.priority) {
            AppConstants.PRIORITY_HIGH -> {
                workOrdersViewHolder.binding.tvPriority.text =
                    mContext.getString(R.string.priority_high)
                workOrdersViewHolder.binding.tvPriority.setTextColor(mContext.getColor(R.color.priority_high))
            }
            AppConstants.PRIORITY_NORMAL -> {
                workOrdersViewHolder.binding.tvPriority.text =
                    mContext.getString(R.string.priority_normal)
                workOrdersViewHolder.binding.tvPriority.setTextColor(mContext.getColor(R.color.priority_medium))
            }
            AppConstants.PRIORITY_LOW -> {
                workOrdersViewHolder.binding.tvPriority.text =
                    mContext.getString(R.string.priority_low)
                workOrdersViewHolder.binding.tvPriority.setTextColor(mContext.getColor(R.color.priority_low))
            }
        }

        workOrdersViewHolder.binding.tvDaysLeft.text = CommonUtils.getDateDifference(
            mContext,
            CommonUtils.getYYYYMMDDHHMMSSFromYYYYMMDDHHMMA(
                mContext.getString(
                    R.string.add_space,
                    workOrdersListdata.expiryDate,
                    workOrdersListdata.expiryTime
                )
            )
        )
        workOrdersViewHolder.binding.llWorkOrdersParent.tag = workOrdersListdata.id
        workOrdersViewHolder.binding.llWorkOrdersParent.setOnClickListener(listener)
    }

    override fun getItemCount(): Int {
        return if (workOrders.size > 4)
            4
        else
            workOrders.size
     }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class DashboardWorkOrdersViewholder(val binding: ListDashboardWorkOrdersBinding) :
        RecyclerView.ViewHolder(binding.root)

}