package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.databinding.ListNotificationsBinding
import com.met.ahvara.model.CustomerListData

class AdapterNotificationsList(
    private val mContext: Context,
    listener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val listData: MutableList<CustomerListData>
    private val listener: View.OnClickListener

    init {
        listData = ArrayList()
        this.listener = listener
    }

    fun putData(listCustomers: List<CustomerListData>) {
        if (listData.isNotEmpty()) {
            listData.clear()
        }
        listData.addAll(listCustomers)
        notifyDataSetChanged()
    }

    fun clearList() {
        if (listData.isNotEmpty()) {
            listData.clear()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListNotificationsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return CustomViewholder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        /*  val customersListdata = listData[position]
          val customersViewHolder = holder as CustomViewholder

          customersViewHolder.binding.tvTitle.text = customersListdata.companyName
         *//* customersViewHolder.binding.tvTime.text = CommonUtils.getDateDifference(
            mContext,
            CommonUtils.getYYYYMMDDHHMMSSFromYYYYMMDDHHMMA(
                mContext.getString(
                    R.string.add_space,
                    customersListdata.Date,
                    customersListdata.Time
                )
            )
        )*//*

        *//*    Picasso.get()
                         .load(appointmentListListdata.)
                         .placeholder(R.drawable.ic_baseline_person_24)
                         .error(R.drawable.ic_baseline_person_24)
                         .into( appoRescheduledViewHolder.binding.ivWalk);*//*
*/


    }

    override fun getItemCount(): Int {
        return 5
//        return customers.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class CustomViewholder(val binding: ListNotificationsBinding) :
        RecyclerView.ViewHolder(binding.root)
}