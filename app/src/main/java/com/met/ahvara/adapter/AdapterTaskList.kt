package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.databinding.ListTasksBinding
import com.met.ahvara.model.TaskListData

class AdapterTaskList(
    private val mContext: Context,
    clickListener: View.OnClickListener,
    checkListener: CompoundButton.OnCheckedChangeListener

    ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val tasks: MutableList<TaskListData>
    private val clickListener: View.OnClickListener
    private val checkListener: CompoundButton.OnCheckedChangeListener

    init {
        tasks = ArrayList()
        this.clickListener = clickListener
        this.checkListener = checkListener
    }

    fun putData(listCustomers: List<TaskListData>) {
        if (tasks.isNotEmpty()) {
            tasks.clear()
        }
        tasks.addAll(listCustomers)
        notifyDataSetChanged()
    }

    fun clearList() {
        if (tasks.isNotEmpty()) {
            tasks.clear()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListTasksBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return CustomViewholder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val listdata = tasks[position]
        val viewHolder = holder as CustomViewholder

        viewHolder.binding.tvTitle.text = listdata.companyName
        viewHolder.binding.tvDueDate.text = listdata.expiryDate
        viewHolder.binding.tvDescription.text = listdata.description
        viewHolder.binding.tvStatus.text = listdata.status

        viewHolder.binding.llTasks.tag = listdata.id
        viewHolder.binding.llTasks.setOnClickListener(clickListener)

        viewHolder.binding.cbCheck.tag = listdata.id
        viewHolder.binding.cbCheck.setOnCheckedChangeListener(checkListener)


        /* customersViewHolder.binding.tvDays.text = CommonUtils.getDateDifference(
             mContext,
             CommonUtils.getYYYYMMDDHHMMSSFromYYYYMMDDHHMMA(
                 mContext.getString(
                     R.string.add_space,
                     customersListdata.Date,
                     customersListdata.Time
                 )
             )
         )*/
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class CustomViewholder(val binding: ListTasksBinding) :
        RecyclerView.ViewHolder(binding.root)
}