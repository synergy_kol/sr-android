package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.R
import com.met.ahvara.databinding.ListWorkOrderListBinding
import com.met.ahvara.model.WorkOrderListData
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils

class AdapterWorkOrderList(
    private val mContext: Context,
    listener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val customers: MutableList<WorkOrderListData>
    private val listener: View.OnClickListener

    init {
        customers = ArrayList()
        this.listener = listener
    }

    fun putData(list: List<WorkOrderListData>) {
        if (customers.isNotEmpty()) {
            customers.clear()
        }
        customers.addAll(list)
        notifyDataSetChanged()
    }

    fun clearList() {
        if (customers.isNotEmpty()) {
            customers.clear()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListWorkOrderListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return CustomViewholder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val listdata = customers[position]
        val viewHolder = holder as CustomViewholder

        viewHolder.binding.tvWorkId.text =
            mContext.getString(R.string.hash_tag, listdata.workId)
        viewHolder.binding.tvCompanyName.text = listdata.companyName
        viewHolder.binding.tvUnit.text = listdata.productName
        viewHolder.binding.tvDueDate.text =
            CommonUtils.getMonthDayYearFromYYYYMMDD(listdata.expiryDate)

        viewHolder.binding.tvTitle.text = listdata.customerName
        if (listdata.attachement?.isNotEmpty() == true) {
            viewHolder.binding.tvHasAttachment.text = "Has Attachment"
            viewHolder.binding.tvHasAttachment.visibility = View.VISIBLE

        } else {
            viewHolder.binding.tvHasAttachment.visibility = View.GONE
        }
        viewHolder.binding.tvValue.text = ""

        when {
            listdata.status.equals(
                AppConstants.APPOINTMENT_STATUS_UNASSIGNED,
                ignoreCase = true
            ) -> {
                viewHolder.binding.tvStatus.background =
                    AppCompatResources.getDrawable(
                        mContext,
                        R.drawable.style_work_order_status_unassigned
                    )
                viewHolder.binding.tvStatus.text = mContext.getString(R.string.status_unassigned)
            }
            listdata.status.equals(
                AppConstants.APPOINTMENT_STATUS_PENDING,
                ignoreCase = true
            ) -> {
                viewHolder.binding.tvStatus.background =
                    AppCompatResources.getDrawable(
                        mContext,
                        R.drawable.style_work_order_status_pending
                    )
                viewHolder.binding.tvStatus.text = mContext.getString(R.string.status_pending)
            }
            listdata.status.equals(AppConstants.APPOINTMENT_STATUS_REVIEW, ignoreCase = true) -> {
                viewHolder.binding.tvStatus.setBackgroundResource(
                    R.drawable.style_work_order_status_in_review
                )
                viewHolder.binding.tvStatus.text = mContext.getString(R.string.status_in_review)
            }
            listdata.status.equals(
                AppConstants.APPOINTMENT_STATUS_COMPLETED,
                ignoreCase = true
            ) -> {
                viewHolder.binding.tvStatus.background =
                    AppCompatResources.getDrawable(
                        mContext,
                        R.drawable.style_work_order_status_resolved
                    )
                viewHolder.binding.tvStatus.text = mContext.getString(R.string.status_completed)
                viewHolder.binding.tvValue.text =
                    mContext.getString(R.string.amount, listdata.total_amount?.toInt())
            }
        }
        when {
            listdata.priority.equals(AppConstants.PRIORITY_HIGH, ignoreCase = true) -> {
                viewHolder.binding.tvPriority.text =
                    mContext.getString(R.string.priority_high)
                viewHolder.binding.tvPriority.setTextColor(mContext.getColor(R.color.priority_high))
            }
            listdata.priority.equals(AppConstants.PRIORITY_LOW, ignoreCase = true) -> {
                viewHolder.binding.tvPriority.setTextColor(mContext.getColor(R.color.priority_medium))
                viewHolder.binding.tvPriority.text =
                    mContext.getString(R.string.priority_normal)
            }
            listdata.priority.equals(AppConstants.PRIORITY_NORMAL, ignoreCase = true) -> {
                viewHolder.binding.tvPriority.setTextColor(mContext.getColor(R.color.priority_low))
                viewHolder.binding.tvPriority.text =
                    mContext.getString(R.string.priority_normal)
            }
        }

        viewHolder.binding.tvAssignedTo.text = listdata.customerName

        viewHolder.binding.llWorkOrdersParent.tag = listdata.id
        viewHolder.binding.llWorkOrdersParent.setOnClickListener(listener)

        /* viewHolder.binding.tvDays.text = CommonUtils.getDateDifference(
             mContext,
             CommonUtils.getYYYYMMDDHHMMSSFromYYYYMMDDHHMMA(
                 mContext.getString(
                     R.string.add_space,
                     customersListdata.Date,
                     customersListdata.Time
                 )
             )
         )*/

        if (position == (customers.size - 1)) {
            viewHolder.binding.llWorkOrdersParent.setPadding(
                0, 0, 0,
                CommonUtils.convertDpToPixel(
                    45f, mContext
                ).toInt()
            )
        }
    }

    override fun getItemCount(): Int {
        return customers.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class CustomViewholder(val binding: ListWorkOrderListBinding) :
        RecyclerView.ViewHolder(binding.root)

}