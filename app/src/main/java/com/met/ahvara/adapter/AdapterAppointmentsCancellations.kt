package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.databinding.ListAppointmentCancellationsBinding
import com.met.ahvara.model.AppointmentListListdata
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils

class AdapterAppointmentsCancellations(
    private val mContext: Context,
    listener: View.OnClickListener, checkListener: CompoundButton.OnCheckedChangeListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val listOfAppointments: MutableList<AppointmentListListdata>
    private val listener: View.OnClickListener
    private val checkListener: CompoundButton.OnCheckedChangeListener

    companion object {
        private const val VISIBLE_TYPE = 0
        private const val INVISIBLE_TYPE = 1
    }

    init {
        listOfAppointments = ArrayList()
        this.listener = listener
        this.checkListener = checkListener
    }

    fun putData(listCustomers: List<AppointmentListListdata>) {
        if (listOfAppointments.isNotEmpty()) {
            listOfAppointments.clear()
        }
        listOfAppointments.addAll(listCustomers)
        notifyDataSetChanged()
    }

    fun clearList() {
        if (listOfAppointments.isNotEmpty()) {
            listOfAppointments.clear()
        }
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VISIBLE_TYPE -> {
                val binding = ListAppointmentCancellationsBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                AppointmentsViewholder(binding)
            }
            INVISIBLE_TYPE -> {
                val binding = ListAppointmentCancellationsBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                AppointmentsViewholder(binding)
            }

            else -> {
                val binding = ListAppointmentCancellationsBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                AppointmentsViewholder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val appointmentListListdata = listOfAppointments[position]
        when (holder.itemViewType) {
            VISIBLE_TYPE -> {
                val appoCompletedViewHolder = holder as AppointmentsViewholder

                holder.itemView.visibility = View.VISIBLE
                holder.itemView.layoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )

                /*    Picasso.get()
                        .load(appointmentListListdata.)
                        .placeholder(R.drawable.ic_baseline_person_24)
                        .error(R.drawable.ic_baseline_person_24)
                        .into( appoCompletedViewHolder.binding.ivWalk);*/

                appoCompletedViewHolder.binding.tvName.text = appointmentListListdata.companyName
                appoCompletedViewHolder.binding.tvAddress.text = appointmentListListdata.siteAddress
                appoCompletedViewHolder.binding.tvTime.text =
                    CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.startDateTime)

                appoCompletedViewHolder.binding.cbSelection.tag = appointmentListListdata.id
                appoCompletedViewHolder.binding.cbSelection.setOnCheckedChangeListener(
                    checkListener
                )

                if (position == (listOfAppointments.size - 1)) {
                    appoCompletedViewHolder.binding.tvAddress.setPadding(
                        0, 0, 0,
                        CommonUtils.convertDpToPixel(
                            50f, mContext
                        ).toInt()
                    )
                }
            }
            INVISIBLE_TYPE -> {
                val appoArrivedViewHolder = holder as AppointmentsViewholder
                holder.itemView.visibility = View.GONE
                holder.itemView.layoutParams = LinearLayout.LayoutParams(0, 0)
            }
        }
    }

    override fun getItemCount(): Int {
        return listOfAppointments.size
    }

    override fun getItemViewType(position: Int): Int {
        for (i in AppConstants.checkedAppointmentIds) {
            return if (i == listOfAppointments[position].id) {
                VISIBLE_TYPE
            } else {
                INVISIBLE_TYPE
            }
        }
        return INVISIBLE_TYPE
        /*return when {
            listOfAppointments[position].id.equals(
                mContext.resources.getString(R.string.appo_scheduled), ignoreCase = true
            ) -> {
                VISIBLE_TYPE
            }
            listOfAppointments[position].status.equals(
                mContext.resources.getString(R.string.appo_arrived),
                ignoreCase = true
            ) -> {
                INVISIBLE_TYPE
            }

            else -> {
                INVISIBLE_TYPE
            }
        }*/
    }

    inner class AppointmentsViewholder(val binding: ListAppointmentCancellationsBinding) :
        RecyclerView.ViewHolder(binding.root)
}