package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.R
import com.met.ahvara.databinding.ListDashboardAppointmentsArrivedBinding
import com.met.ahvara.databinding.ListDashboardAppointmentsCompletedBinding
import com.met.ahvara.databinding.ListDashboardAppointmentsRescheduledBinding
import com.met.ahvara.databinding.ListDashboardAppointmentsScheduledBinding
import com.met.ahvara.model.AppointmentListListdata
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils

class AdapterAppointments(
    private val mContext: Context,
    clicklistener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val listOfAppointments: MutableList<AppointmentListListdata>
    private val clicklistener: View.OnClickListener

    init {
        listOfAppointments = ArrayList()
        this.clicklistener = clicklistener
    }

    fun putData(listCustomers: List<AppointmentListListdata>) {
        if (listOfAppointments.isNotEmpty()) {
            listOfAppointments.clear()
        }
        listOfAppointments.addAll(listCustomers)
        notifyDataSetChanged()
    }

    fun clearList() {
        if (listOfAppointments.isNotEmpty()) {
            listOfAppointments.clear()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            AppConstants.APPOINTMENT_STATUS_COMPLETED.toInt() -> {
                val binding = ListDashboardAppointmentsCompletedBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsCompletedViewholder(binding)
            }
            AppConstants.APPOINTMENT_STATUS_SCHEDULED.toInt() -> {
                val binding = ListDashboardAppointmentsScheduledBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsScheduledViewholder(binding)
            }

            AppConstants.APPOINTMENT_STATUS_ARRIVED.toInt() -> {
                val binding = ListDashboardAppointmentsArrivedBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsArrivedViewholder(binding)
            }
            AppConstants.APPOINTMENT_STATUS_RESCHEDULED.toInt() -> {
                val binding = ListDashboardAppointmentsRescheduledBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsRescheduledViewholder(binding)
            }
            else -> {
                val binding = ListDashboardAppointmentsScheduledBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsScheduledViewholder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val appointmentListListdata = listOfAppointments[position]
        when (holder.itemViewType) {
            AppConstants.APPOINTMENT_STATUS_COMPLETED.toInt() -> {
                val appoViewHolder =
                    holder as DashboardAppointmentsCompletedViewholder

                /*    Picasso.get()
                        .load(appointmentListListdata.)
                        .placeholder(R.drawable.ic_baseline_person_24)
                        .error(R.drawable.ic_baseline_person_24)
                        .into( appoCompletedViewHolder.binding.ivWalk);*/

                appoViewHolder.binding.tvName.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvAddress.text = appointmentListListdata.siteAddress
                //appoCompletedViewHolder.binding.tvStarVip.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvTimeRange.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_time_range,
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.startDateTime),
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.endDateTime)
                    )
                appoViewHolder.binding.tvTotalTime.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_total_time, appointmentListListdata.duration
                    )
                /* appoCompletedViewHolder.binding.tvCompleted.text =
                     appointmentListListdata.companyName*/

                appoViewHolder.binding.clCompletedTop.tag = appointmentListListdata.id
                appoViewHolder.binding.clCompletedTop.setOnClickListener(clicklistener)

            }
            AppConstants.APPOINTMENT_STATUS_ARRIVED.toInt() -> {
                val appoViewHolder =
                    holder as DashboardAppointmentsArrivedViewholder

                /*    Picasso.get()
                         .load(appointmentListListdata.)
                         .placeholder(R.drawable.ic_baseline_person_24)
                         .error(R.drawable.ic_baseline_person_24)
                         .into( appoArrivedViewHolder.binding.ivWalk);*/

                appoViewHolder.binding.tvName.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvAddress.text = appointmentListListdata.siteAddress
                //appoCompletedViewHolder.binding.tvStarVip.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvTimeRange.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_time_range,
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.startDateTime),
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.endDateTime)
                    )
                appoViewHolder.binding.tvTotalTime.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_total_time, appointmentListListdata.duration
                    )
                /* appoArrivedViewHolder.binding.tvCompleted.text =
                     appointmentListListdata.companyName*/

                appoViewHolder.binding.clArriveTop.tag = appointmentListListdata.id
                appoViewHolder.binding.clArriveTop.setOnClickListener(clicklistener)

                appoViewHolder.binding.cbSelection.tag = appointmentListListdata.id
                appoViewHolder.binding.cbSelection.visibility = View.GONE


            }
            AppConstants.APPOINTMENT_STATUS_SCHEDULED.toInt() -> {
                val appoViewHolder =
                    holder as DashboardAppointmentsScheduledViewholder

                /*    Picasso.get()
                        .load(appointmentListListdata.)
                        .placeholder(R.drawable.ic_baseline_person_24)
                        .error(R.drawable.ic_baseline_person_24)
                        .into( appoRescheduledViewHolder.binding.ivWalk);*/

                appoViewHolder.binding.tvName.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvAddress.text =
                    appointmentListListdata.siteAddress
                //appoCompletedViewHolder.binding.tvStarVip.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvTimeRange.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_time_range,
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.startDateTime),
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.endDateTime)
                    )
                appoViewHolder.binding.tvTotalTime.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_total_time, appointmentListListdata.duration
                    )

                appoViewHolder.binding.clScheduledTop.tag = appointmentListListdata.id
                appoViewHolder.binding.clScheduledTop.setOnClickListener(clicklistener)

                appoViewHolder.binding.cbSelection.tag = appointmentListListdata.id
                appoViewHolder.binding.cbSelection.visibility = View.GONE
            }

            AppConstants.APPOINTMENT_STATUS_RESCHEDULED.toInt() -> {
                val appoViewHolder =
                    holder as DashboardAppointmentsRescheduledViewholder

                /*    Picasso.get()
                        .load(appointmentListListdata.)
                        .placeholder(R.drawable.ic_baseline_person_24)
                        .error(R.drawable.ic_baseline_person_24)
                        .into( appoRescheduledViewHolder.binding.ivWalk);*/

                appoViewHolder.binding.tvName.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvAddress.text =
                    appointmentListListdata.siteAddress
                //appoCompletedViewHolder.binding.tvStarVip.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvTimeRange.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_time_range,
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.startDateTime),
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.endDateTime)
                    )
                appoViewHolder.binding.tvTotalTime.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_total_time, appointmentListListdata.duration
                    )

                appoViewHolder.binding.tvRescheduledDate.text =
                    CommonUtils.getMonthDayYearFromYYYYMMDDHHmmss(appointmentListListdata.startDateTime)
                /* appoCompletedViewHolder.binding.tvCompleted.text =
                     appoRescheduledViewHolder.companyName*/

                appoViewHolder.binding.clRescheduledTop.tag = appointmentListListdata.id
                appoViewHolder.binding.clRescheduledTop.setOnClickListener(clicklistener)

                appoViewHolder.binding.cbSelection.tag = appointmentListListdata.id
                appoViewHolder.binding.cbSelection.visibility = View.GONE

            }
        }
    }

    override fun getItemCount(): Int {
        return listOfAppointments.size
    }

    override fun getItemViewType(position: Int): Int {
        return listOfAppointments[position].status?.toInt() ?: 0
/*
        return when {
            listOfAppointments[position].status.equals(
                mContext.resources.getString(R.string.appo_scheduled), ignoreCase = true
            ) -> {
                COMPLETED_TYPE
            }
            listOfAppointments[position].status.equals(
                mContext.resources.getString(R.string.appo_arrived),
                ignoreCase = true
            ) -> {
                ARRIVED_TYPE
            }
            listOfAppointments[position].status.equals(
                mContext.resources.getString(R.string.appo_rescheduled),
                ignoreCase = true
            ) -> {
                RESCHEDULE_TYPE
            }
            else -> {
                COMPLETED_TYPE
            }
        }*/
    }

    inner class DashboardAppointmentsScheduledViewholder(val binding: ListDashboardAppointmentsScheduledBinding) :
        RecyclerView.ViewHolder(binding.root)

    inner class DashboardAppointmentsCompletedViewholder(val binding: ListDashboardAppointmentsCompletedBinding) :
        RecyclerView.ViewHolder(binding.root)

    inner class DashboardAppointmentsArrivedViewholder(val binding: ListDashboardAppointmentsArrivedBinding) :
        RecyclerView.ViewHolder(binding.root)

    inner class DashboardAppointmentsRescheduledViewholder(val binding: ListDashboardAppointmentsRescheduledBinding) :
        RecyclerView.ViewHolder(binding.root)
}