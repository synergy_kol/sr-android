package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.R
import com.met.ahvara.databinding.ListCustomerDetailsAddressBinding
import com.met.ahvara.model.CustomerContactDetails
import com.met.ahvara.model.CustomerDetailsData

class AdapterCustomerDetailsAddress(
    private val mContext: Context/*,
    listener: View.OnClickListener*/
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val customerAddress: MutableList<CustomerContactDetails>
    private var customerDetails: CustomerDetailsData
    //  private val listener: View.OnClickListener

    init {
        customerAddress = ArrayList()
        //       this.listener = listener
        customerDetails = CustomerDetailsData()
    }

    fun putData(customerDetails: CustomerDetailsData) {

        if (customerAddress.isNotEmpty()) {
            customerAddress.clear()
        }
        this.customerDetails = customerDetails
        customerAddress.addAll(customerDetails.contactDetails)
        notifyDataSetChanged()
    }

    fun clearList() {
        if (customerAddress.isNotEmpty()) {
            customerAddress.clear()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListCustomerDetailsAddressBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return CustomViewholder(binding)
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val customersListdata = customerAddress[position]
        val customersViewHolder = holder as CustomViewholder

        customersViewHolder.binding.tvSiteNumber.text =
            mContext.getString(R.string.customer_details_sites, (position + 1))
//        customersViewHolder.binding.tvSiteNumber.text = "Sites: " + (position + 1).toString()

        customersViewHolder.binding.tvNameEmailPhone.text =
            mContext.getString(
                R.string.add_double_space,
                customerDetails.customerName,
                customerDetails.email,
                customerDetails.contact
            )
        customersViewHolder.binding.tvAddress.text = customersListdata.address

    }

    override fun getItemCount(): Int {
        return customerAddress.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class CustomViewholder(val binding: ListCustomerDetailsAddressBinding) :
        RecyclerView.ViewHolder(binding.root)
}