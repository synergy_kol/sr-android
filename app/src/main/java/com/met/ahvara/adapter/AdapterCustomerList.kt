package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.databinding.ListCustomerListBinding
import com.met.ahvara.model.CustomerListData
import com.met.ahvara.utils.CommonUtils

class AdapterCustomerList(
    private val mContext: Context,
    listener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val customers: MutableList<CustomerListData>
    private val listener: View.OnClickListener

    init {
        customers = ArrayList()
        this.listener = listener
    }

    fun putData(listCustomers: List<CustomerListData>) {
        if (customers.isNotEmpty()) {
            customers.clear()
        }
        customers.addAll(listCustomers)
        notifyDataSetChanged()
    }

    fun clearList() {
        if (customers.isNotEmpty()) {
            customers.clear()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListCustomerListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return CustomViewholder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val customersListdata = customers[position]
        val customersViewHolder = holder as CustomViewholder

        customersViewHolder.binding.tvTitle.text = customersListdata.companyName
        customersViewHolder.binding.tvContactNo.text = customersListdata.contact
        customersViewHolder.binding.tvEmail.text = customersListdata.email
        customersViewHolder.binding.tvAddress.text = customersListdata.address
        customersViewHolder.binding.llDemo.tag = customersListdata.customerId
        customersViewHolder.binding.llDemo.setOnClickListener(listener)

        if (position == (customers.size - 1)) {
            customersViewHolder.binding.tvAddress.setPadding(
                0, 0, 0,
                CommonUtils.convertDpToPixel(
                    50f, mContext
                ).toInt()
            )
        }

        /* customersViewHolder.binding.tvDays.text = CommonUtils.getDateDifference(
             mContext,
             CommonUtils.getYYYYMMDDHHMMSSFromYYYYMMDDHHMMA(
                 mContext.getString(
                     R.string.add_space,
                     customersListdata.Date,
                     customersListdata.Time
                 )
             )
         )*/
    }

    override fun getItemCount(): Int {
        return customers.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class CustomViewholder(val binding: ListCustomerListBinding) :
        RecyclerView.ViewHolder(binding.root)
}