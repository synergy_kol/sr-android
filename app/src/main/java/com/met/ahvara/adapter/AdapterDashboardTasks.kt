package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.R
import com.met.ahvara.databinding.ListDashboardTasksBinding
import com.met.ahvara.model.TaskListData

class AdapterDashboardTasks(
    private val mContext: Context, listOfWorkOrders: List<TaskListData>?,
    listener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val workOrders: MutableList<TaskListData>
    private val listener: View.OnClickListener

    init {
        workOrders = ArrayList()
        workOrders.addAll(listOfWorkOrders!!)
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListDashboardTasksBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DashboardTasksViewholder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val workOrdersListdata = workOrders[position]
        val workOrdersViewHolder = holder as DashboardTasksViewholder

        workOrdersViewHolder.binding.tvTitle.text = mContext.getString(
            R.string.hash_tag, workOrdersListdata.workId
        )
        workOrdersViewHolder.binding.tvDueDate.text = workOrdersListdata.companyName
        workOrdersViewHolder.binding.tvDescription.text = ""
    }

    override fun getItemCount(): Int {
        return workOrders.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class DashboardTasksViewholder(val binding: ListDashboardTasksBinding) :
        RecyclerView.ViewHolder(binding.root)
}