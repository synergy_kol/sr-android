package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.databinding.ListLeadsBinding
import com.met.ahvara.model.LeadsListData
import com.met.ahvara.utils.CommonUtils

class AdapterLeadsList(
    private val mContext: Context,
    listener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val leads: MutableList<LeadsListData>
    private val listener: View.OnClickListener

    init {
        leads = ArrayList()
        this.listener = listener
    }

    fun putData(listLeads: List<LeadsListData>) {
        if (leads.isNotEmpty()) {
            leads.clear()
        }
        leads.addAll(listLeads)
        notifyDataSetChanged()
    }

    fun clearList() {
        if (leads.isNotEmpty()) {
            leads.clear()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListLeadsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return CustomViewholder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val leadsListdata = leads[position]
        val viewHolder = holder as CustomViewholder

        viewHolder.binding.tvCompanyName.text = leadsListdata.companyName

        //Nov 06, 2021"
        viewHolder.binding.tvEstDate.text = leadsListdata.estDateClose?.let {
            CommonUtils.getDateUsingPattern(
                it,
                "yyyy-MM-dd",
                "MMM dd, yyyy"
            )
        }

        viewHolder.binding.tvUnit.text = leadsListdata.unit

        //Nov 06, 2021"
        viewHolder.binding.tvFollowUpDate.text = leadsListdata.estDateClose?.let {
            CommonUtils.getDateUsingPattern(
                it,
                "yyyy-MM-dd",
                "MMM dd, yyyy"
            )
        }

        viewHolder.binding.tvDaysToGo.text =
            leadsListdata.estDateClose?.let {
                CommonUtils.getDateDifferenceDays(
                    mContext,
                    it
                )
            }

        viewHolder.binding.btCancelLost.tag = leadsListdata.leadId
        viewHolder.binding.btCancelLost.setOnClickListener(listener)

        viewHolder.binding.btConvert.tag = leadsListdata.leadId
        viewHolder.binding.btConvert.setOnClickListener(listener)

        viewHolder.binding.llTop.tag = leadsListdata.leadId
        viewHolder.binding.llTop.setOnClickListener(listener)

        /*  if (position == (leads.size - 1)) {
              viewHolder.binding.llTop.setPadding(
                  0, 0, 0,
                  CommonUtils.convertDpToPixel(
                      50f, mContext
                  ).toInt()
              )
          }*/

        /* customersViewHolder.binding.tvDays.text = CommonUtils.getDateDifference(
             mContext,
             CommonUtils.getYYYYMMDDHHMMSSFromYYYYMMDDHHMMA(
                 mContext.getString(
                     R.string.add_space,
                     customersListdata.Date,
                     customersListdata.Time
                 )
             )
         )*/
    }

    override fun getItemCount(): Int {
        return leads.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class CustomViewholder(val binding: ListLeadsBinding) :
        RecyclerView.ViewHolder(binding.root)
}