package com.met.ahvara.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import com.met.ahvara.R
import com.met.ahvara.databinding.ListDashboardAppointmentsArrivedBinding
import com.met.ahvara.databinding.ListDashboardAppointmentsCompletedBinding
import com.met.ahvara.databinding.ListDashboardAppointmentsRescheduledBinding
import com.met.ahvara.databinding.ListDashboardAppointmentsScheduledBinding
import com.met.ahvara.model.AppointmentListListdata
import com.met.ahvara.utils.AppConstants
import com.met.ahvara.utils.CommonUtils

class AdapterDashboardAppointments(
    private val mContext: Context,
    clickListener: View.OnClickListener,
    checkListener: CompoundButton.OnCheckedChangeListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val listOfAppointments: MutableList<AppointmentListListdata>
    private val clickListener: View.OnClickListener
    private val checkListener: CompoundButton.OnCheckedChangeListener

    fun putData(listCustomers: List<AppointmentListListdata>) {
        if (listOfAppointments.isNotEmpty()) {
            listOfAppointments.clear()
        }
        listOfAppointments.addAll(listCustomers)
        notifyDataSetChanged()
    }

    fun clearList() {
        if (listOfAppointments.isNotEmpty()) {
            listOfAppointments.clear()
        }
        notifyDataSetChanged()
    }

    init {
        listOfAppointments = ArrayList()
        this.clickListener = clickListener
        this.checkListener = checkListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            AppConstants.APPOINTMENT_STATUS_COMPLETED.toInt() -> {
                val binding = ListDashboardAppointmentsCompletedBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsCompletedViewholder(binding)
            }
            AppConstants.APPOINTMENT_STATUS_SCHEDULED.toInt() -> {
                val binding = ListDashboardAppointmentsScheduledBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsScheduledViewholder(binding)
            }

            AppConstants.APPOINTMENT_STATUS_ARRIVED.toInt() -> {
                val binding = ListDashboardAppointmentsArrivedBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsArrivedViewholder(binding)
            }
            AppConstants.APPOINTMENT_STATUS_RESCHEDULED.toInt() -> {
                val binding = ListDashboardAppointmentsRescheduledBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsRescheduledViewholder(binding)
            }
            else -> {
                val binding = ListDashboardAppointmentsScheduledBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                DashboardAppointmentsScheduledViewholder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val appointmentListListdata = listOfAppointments[position]
        when (holder.itemViewType) {
            AppConstants.APPOINTMENT_STATUS_COMPLETED.toInt() -> {
                val appoCompletedViewHolder = holder as DashboardAppointmentsCompletedViewholder

                /*    Picasso.get()
                        .load(appointmentListListdata.)
                        .placeholder(R.drawable.ic_baseline_person_24)
                        .error(R.drawable.ic_baseline_person_24)
                        .into( appoCompletedViewHolder.binding.ivWalk);*/

                appoCompletedViewHolder.binding.tvName.text = appointmentListListdata.companyName
                appoCompletedViewHolder.binding.tvAddress.text = appointmentListListdata.siteAddress
                //appoCompletedViewHolder.binding.tvStarVip.text = appointmentListListdata.companyName
                appoCompletedViewHolder.binding.tvTimeRange.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_time_range,
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.startDateTime),
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.endDateTime)
                    )
                appoCompletedViewHolder.binding.tvTotalTime.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_total_time, appointmentListListdata.duration
                    )
                /* appoCompletedViewHolder.binding.tvCompleted.text =
                     appointmentListListdata.companyName*/

                appoCompletedViewHolder.binding.clCompletedTop.tag = appointmentListListdata
                appoCompletedViewHolder.binding.clCompletedTop.setOnClickListener(clickListener)

            }
            AppConstants.APPOINTMENT_STATUS_ARRIVED.toInt() -> {
                val appoViewHolder = holder as DashboardAppointmentsArrivedViewholder

                /*    Picasso.get()
                         .load(appointmentListListdata.)
                         .placeholder(R.drawable.ic_baseline_person_24)
                         .error(R.drawable.ic_baseline_person_24)
                         .into( appoArrivedViewHolder.binding.ivWalk);*/

                appoViewHolder.binding.tvName.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvAddress.text = appointmentListListdata.siteAddress
                //appoCompletedViewHolder.binding.tvStarVip.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvTimeRange.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_time_range,
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.startDateTime),
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.endDateTime)
                    )
                appoViewHolder.binding.tvTotalTime.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_total_time, appointmentListListdata.duration
                    )
                /* appoArrivedViewHolder.binding.tvCompleted.text =
                     appointmentListListdata.companyName*/

                appoViewHolder.binding.clArriveTop.tag = appointmentListListdata
                appoViewHolder.binding.clArriveTop.setOnClickListener(clickListener)

                appoViewHolder.binding.cbSelection.tag = appointmentListListdata.id
                appoViewHolder.binding.cbSelection.setOnCheckedChangeListener(checkListener)

            }
            AppConstants.APPOINTMENT_STATUS_SCHEDULED.toInt() -> {
                val appoViewHolder = holder as DashboardAppointmentsScheduledViewholder

                /*    Picasso.get()
                        .load(appointmentListListdata.)
                        .placeholder(R.drawable.ic_baseline_person_24)
                        .error(R.drawable.ic_baseline_person_24)
                        .into( appoRescheduledViewHolder.binding.ivWalk);*/

                appoViewHolder.binding.tvName.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvAddress.text =
                    appointmentListListdata.siteAddress
                //appoCompletedViewHolder.binding.tvStarVip.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvTimeRange.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_time_range,
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.startDateTime),
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.endDateTime)
                    )
                appoViewHolder.binding.tvTotalTime.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_total_time, appointmentListListdata.duration
                    )

                appoViewHolder.binding.clScheduledTop.tag = appointmentListListdata
                appoViewHolder.binding.clScheduledTop.setOnClickListener(clickListener)

                appoViewHolder.binding.cbSelection.tag = appointmentListListdata.id
                appoViewHolder.binding.cbSelection.setOnCheckedChangeListener(
                    checkListener
                )
            }

            AppConstants.APPOINTMENT_STATUS_RESCHEDULED.toInt() -> {
                val appoViewHolder = holder as DashboardAppointmentsRescheduledViewholder

                /*    Picasso.get()
                        .load(appointmentListListdata.)
                        .placeholder(R.drawable.ic_baseline_person_24)
                        .error(R.drawable.ic_baseline_person_24)
                        .into( appoRescheduledViewHolder.binding.ivWalk);*/

                appoViewHolder.binding.tvName.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvAddress.text =
                    appointmentListListdata.siteAddress
                //appoCompletedViewHolder.binding.tvStarVip.text = appointmentListListdata.companyName
                appoViewHolder.binding.tvTimeRange.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_time_range,
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.startDateTime),
                        CommonUtils.getTimeFromYYYYMMDD(appointmentListListdata.endDateTime)
                    )
                appoViewHolder.binding.tvTotalTime.text =
                    mContext.getString(
                        R.string.dashboard_appo_list_total_time, appointmentListListdata.duration
                    )

                appoViewHolder.binding.tvRescheduledDate.text =
                    CommonUtils.getMonthDayYearFromYYYYMMDDHHmmss(appointmentListListdata.startDateTime)
                /* appoCompletedViewHolder.binding.tvCompleted.text =
                     appoRescheduledViewHolder.companyName*/

                appoViewHolder.binding.clRescheduledTop.tag = appointmentListListdata
                appoViewHolder.binding.clRescheduledTop.setOnClickListener(clickListener)

                appoViewHolder.binding.cbSelection.tag = appointmentListListdata.id
                appoViewHolder.binding.cbSelection.setOnCheckedChangeListener(
                    checkListener
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return listOfAppointments.size
    }

    override fun getItemViewType(position: Int): Int {
        return listOfAppointments[position].status?.toInt() ?: 0
        /*
            return when (listOfAppointments[position].status) {
                AppConstants.APPOINTMENT_STATUS_COMPLETED
                -> {
                    COMPLETED_TYPE
                }
                AppConstants.APPOINTMENT_STATUS_ARRIVED
                -> {
                    ARRIVED_TYPE
                }
                AppConstants.APPOINTMENT_STATUS_RESCHEDULED
                -> {
                    RESCHEDULE_TYPE
                }
                else -> {
                    COMPLETED_TYPE
                }*/
    }

    inner class DashboardAppointmentsScheduledViewholder(val binding: ListDashboardAppointmentsScheduledBinding) :
        RecyclerView.ViewHolder(binding.root)

    inner class DashboardAppointmentsCompletedViewholder(val binding: ListDashboardAppointmentsCompletedBinding) :
        RecyclerView.ViewHolder(binding.root)

    inner class DashboardAppointmentsArrivedViewholder(val binding: ListDashboardAppointmentsArrivedBinding) :
        RecyclerView.ViewHolder(binding.root)

    inner class DashboardAppointmentsRescheduledViewholder(val binding: ListDashboardAppointmentsRescheduledBinding) :
        RecyclerView.ViewHolder(binding.root)
}