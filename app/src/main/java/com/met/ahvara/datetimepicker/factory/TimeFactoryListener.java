package com.met.ahvara.datetimepicker.factory;

public interface TimeFactoryListener {

    void onHourChanged(int hour);

    void onMinuteChanged(int minute);
}
