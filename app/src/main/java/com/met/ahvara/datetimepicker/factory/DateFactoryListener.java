package com.met.ahvara.datetimepicker.factory;

public interface DateFactoryListener {
    void onYearChanged();

    void onMonthChanged();

    void onDayChanged();

    void onConfigsChanged();
}
