package com.met.ahvara.datetimepicker.view.popup;

import android.view.View;

public interface IPopupInterface {

    void addView(View view);
}
